<?php
$routes = array(
	    'about' 		=> 'page/view/id/8',
	    'accreditation'  	=> 'page/view/id/1',
	    'founders'		=> 'page/view/id/2',
	    'structure'		=> 'page/view/id/3',
	    'legal-clinic'	=> 'page/view/id/12',
	    'faculty/continuing-education' => 'page/view/id/22',
	    'faculty/training'	=> 'page/view/id/15',
	    'faculty/higher-education' => 'page/view/id/21',
	    'faculty'		=> 'page/view/id/39',
	    'applicant'		=> 'page/view/id/7',
	    'page/([0-9]+)'     => 'page/view/id/$1',
	    'news/([0-9]+)'	=> 'news/view/id/$1',
            'admin'             => 'Backend',
            'admin/(.+?)'       => 'Backend/$1');
