<div class="pagecontent">
    <div class="title"><span>Ошибка 404</span></div>
    <div class="pagenewstext">
        <h3 style="color: #444;">Приносим извинения, страница которую Вы запросили не найдена</h3><br>
        <p style="font-size: 13px;">Возможные причины ошибки:</p><br>
           <p style="padding-bottom: 15px;"> — ошибка при наборе адреса страницы (URL),<br>
            — переход по неработающей или  неправильной  ссылке,<br>
            — отсутствие запрашиваемой страницы на сайте.<br>
</p>
        <a style="font-size: 14px; padding-left: 15px; text-decoration: underline;" href="/">На Главную</a>
    </div>
</div>