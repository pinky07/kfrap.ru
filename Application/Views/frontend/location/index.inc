<div class="pagecontent2">
    <div class="title"><span>Контакты/Схема проезда</span></div>
    <div class="news_all">
        <div id="leftlocation">
            <h3 style="margin-bottom: 15px;">Добраться до академии:</h3>
            <h4 style="padding: 3px 0;">Автобус:</h4>
            <p style="padding: 3px 0;"><i><a href="#" onclick="showStation('megastroy'); return false;">Остановка "Мегастрой":</a> </i>  45, 46, 52, 62, 100</p>
            <p style="padding: 3px 0;"><i><a href="#" onclick="showStation('pmk'); return false;">Остановка "ПМК":</a></i> 45, 46, 52, 62, 100</p>
        </div>
        <div id="rightcontacts">
            <h3>Российская академия правосудия, Казанский филиал</h3>
            <p style="color: #909090; margin-bottom: 15px;">2-я Азинская ул., 7а, г. Казань, Республика Татарстан, 420100</p>
            <div id="academy-location" style="height: 400px; width: 500px;"></div>
            <!--<img src="/images/location.png" alt="Академия правосудия" style="width: 500px;height: 300px;">-->
        </div>
    </div>
</div>
<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
<script type="text/javascript">
    //$(function(){
        //initialize();
    //});
    //function initialize() {
        var map_latlng = new google.maps.LatLng(55.8031897, 49.208601);
        var academy_latlng = new google.maps.LatLng(55.8024897, 49.208101);
        var myOptions = {
          zoom: 16,
          center: map_latlng,
          mapTypeId: google.maps.MapTypeId.HYBRID
        };
        var map = new google.maps.Map(document.getElementById("academy-location"),
            myOptions);
        /*var marker = new google.maps.Marker({
          position: latlng,
          map: map,
          animation: google.maps.Animation.BOUNCE,
          title:"Российская академия правосудия!"
        });*/

        var contentString = '<div style="width:200px;"><img style="width:110px; padding-right:10px" align="left" src="/images/location_window_image.jpg"><b>Казанский филиал академии правосудия</b></div>';
            var infowindow = new google.maps.InfoWindow({
                content: contentString
            });
            var marker = new google.maps.Marker({
                position: academy_latlng,
                map: map,
                title: 'Российская академия правосудия'
            });

            infowindow.open(map,marker);

            google.maps.event.addListener(marker, 'click', function() {
              infowindow.open(map,marker);
            });
    //}

    function showStation(station)
    {
        switch(station)
        {
            case('pmk'):
                var pmkStation = new google.maps.InfoWindow({
                                content: '<div>автобусы:<br/>45, 46, 52, 62, 100</div>'
                            }).open(map,new google.maps.Marker({
                                                            position: new google.maps.LatLng(55.8006897, 49.210801),
                                                            map: map,
                                                            title: 'Остановка ПМК'
                                                        })); break;
            case('megastroy'):
                var megastroyStation = new google.maps.InfoWindow({
                                        content: '<div>автобусы</div>'
                                    }).open(map,new google.maps.Marker({
                                                                    position: new google.maps.LatLng(55.8051897, 49.205601),
                                                                    map: map,
                                                                    title: 'Остановка Мегострой'
                                                                })); break;
        }
    }

</script>
