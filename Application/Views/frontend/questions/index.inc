<div class="pagecontent2">
    <div class="title"><span>Вопрос-ответ</span></div>
    <div class="news_all">
        <div style="float:left; width: 690px">
	<?foreach($questions as $question):?>
        <div style="line-height: 20px; font-size: 12px"><h4>Вопрос №<?=$question->id?> от <?=sprintf('%s %s', $question->last_name, $question->first_name)?>. </h4> Отвечает <?=isset($question->answer->answerer)?$question->answer->answerer:''?></div>
        <div class="pagenewstext clearfix" style="border: 1px solid #ddd; width:630px; margin-bottom: 20px;">

        <div class="one_division" style="width: 630px; margin: 1px;">
                <div class="left2" style="width: 240px;">
                    <div class="one_division_more2" style="width: 220px;">
                        <img src="/images/officer3.png" />
                        <p><?=$question->text?></p>
                    </div>
                </div>
                <div class="center2" style="text-align: center; ">
                    <i>Вопрос</i></div>
                <div class="right2">
                    &nbsp;</div>
            </div>
            <div class="one_division" style="width: 690px; margin: 1px;">
                <div class="left2" style="width: 240px;">
                    &nbsp;</div>
                <div class="center2" style="text-align: center; ">
                    <i>Ответ</i></div>
                <div class="right2" style="width: 240px;">
                    <div class="one_division_more" style="width: 220px;">
                        <img src="/images/officer2.png" />
			<p><?=isset($question->answer->answer_text)?$question->answer->answer_text:''?></p>
                    </div>
                </div>
            </div>
         </div>
	<?endforeach?>
            </div>
    </div>
    </div>
