<div class="pagenews"><div class="title"><span><?=$newsObject->title?></span></div>
    <div class="pagenewstext">
        <div class="prewie_top_new">
            <p class="data">Дата публикации:
                <i><?=!empty($newsObject->actual_date)?$newsObject->actual_date->format('d.m.Y'):''?></i>
            </p>
            <p class="cat">Категория:
                <a href="/news/category/id/<?=$newsObject->category_id?>"><?=$newsObject->category->title?></a>
            </p>
        </div>
        <p>
            <?=$newsObject->content?>
        </p>
    </div>
</div>