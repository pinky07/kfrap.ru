<div class="pagecontent2">
    <div class="title"><span><?=$categoryTitle?></span></div>
    <div class="news_all">
    <div class="allnews_list">
        <div id="loader"></div>
        <?foreach($news as $article):?>
        <div class="one_news">
            <?$image = ($article->getImages(true))?$article->getImages(true):(($article->slider_picture)?"/upload/images/{$article->slider_picture}":'')?>
            <a href="/news/<?=$article->id?>"><img class="one_news_image" height="60px" src="<?=(!empty($image)?$image:'/images/defaultimage.jpg')?>"></a>
            <div class="allnews_list_margin">
                <div class="one_news_title"><a href="/news/<?=$article->id?>"><?=$article->title?></a></div>
                <div class="one_news_date"><?=!empty($article->actual_date)?$article->actual_date->format('d.m.Y'):''?></div>
                <div class="one_news_text"><?=$article->preview?></div>
                <div class="one_news_more"><a href="/news/<?=$article->id?>">Читать полностью</a></div>
            </div>
        </div>
        <?endforeach?>
    </div>
        <div class="news_right_colomn">
        <div class="news-categories-section" style="margin-bottom: 20px">
            <span>Категории</span>
            <ul>
                <li class="news-category">
                    <a href="/news/category" <?=($categoryId=='')?'class="selected-category"':''?>> <span>Все</span></a>
                </li>
                <?foreach($categories as $category):?>
                <li class="news-category">
                    <a href="/news/category/id/<?=$category->id?>" <?=($categoryId==$category->id)?'class="selected-category"':''?>> <span><?=$category->title?></span></a>
                </li>
                <?endforeach?>
            </ul>
        </div>
        <div id ="datepicker" style="margin-top:10px; position: relative;"></div>
    </div>
    </div>

</div>
    <script type="text/javascript">
        $(function() {
            $.datepicker.setDefaults(
                $.extend($.datepicker.regional["ru"])
            );

            $( "#datepicker" ).datepicker({
                dateFormat: "yy-mm-dd",
                onSelect: function(dateText, inst)
                {
                    var category = '/news/category';

                    if($('.selected-category').length)
                    {
                        category = $('.selected-category').attr('href');
                    }

                    showNews(category, dateText);
                }
            });

            $('.news-category a').click(function()
            {
                var category    = $(this).attr('href');
                var date        = 'all';

                if($('.selected-category').length)
                {
                    $('.selected-category').removeClass('selected-category');
                }

                $(this).addClass('selected-category');

                if($('.ui-state-active').length && !$('.ui-state-active').hasClass('ui-state-highlight'))
                {
                    date = $.datepicker.formatDate('yy-mm-dd', $( "#datepicker" ).datepicker( "getDate" ));
                }
                showNews(category, date);

                return false;
            });
        });

        function showNews(category, date)
        {
            $('#loader').show();
            getNews(category, date);
        }

        function getNews(url, date)
        {
            url += (date == 'all')?'':'?date='+date;
            //alert(url);
                //alert(url)
            $.ajax({
                url: url,
                dataType : 'json',
                success: function (data, textStatus)
                {
                    innerContent(data);

                    $('#loader').hide();
                }
            });


        }

        function innerContent(data)
        {
            var content = '<div id="loader"></div>';


            for(var i = 0; i < data.news.length; i++)
            {
                content += '<div class="one_news"><a href="/news/'
                    + data.news[i].id
                    + '"><img class="one_news_image" height="60px" src="'
                    + data.news[i].image
                    +'"></a><div class="allnews_list_margin"><div class="one_news_title"><a href="/news/'
                    + data.news[i].id
                    +'">'
                    + data.news[i].title
                    +'</a></div><div class="one_news_date">'
                    + data.news[i].actual_date
                    +'</div><div class="one_news_text">'
                    + data.news[i].preview
                    +'</div><div class="one_news_more"><a href="/news/'
                    + data.news[i].id
                    +'">Читать полностью</a></div></div></div>';

            }

            var date = '';

            if(data.date != '')
            {
                date = '<span class="date-for">Дата: ' + data.date + '(<a href="" onclick="removeDate(); return false;">×</a>)</span> ';
                data.categoryTitle += '.';
            }
            var title = '<span>' +data.categoryTitle+ '</span>&nbsp;&nbsp;&nbsp;' + date;


            $('.title').html(title);
            $('.allnews_list').html(content);
        }

        function removeDate()
        {
            var category = '/news/category';

            if($('.selected-category').length)
            {
                category = $('.selected-category').attr('href');
            }

            showNews(category, 'all');

        }
    </script>
    <style type="text/css">
        .selected-category span
        {
            color: #3399FF;
        }
        .date-for
        {
            position: relative;
            left:0;
        }
    </style>