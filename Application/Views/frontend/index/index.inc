<div class="leftcoloumn">
    <div class="twit_pic">
        <h2 id="tw-r">Твиттер</h2>
        <a href="https://twitter.com/@kazanraj" class="amblame">@Kazanraj</a>
    </div>
    <div id="twiter-ticker" style="display: block;">
        <div class="jScrollPaneContainer" style="height: auto; max-height: 120px; width: 282px;">
            <div id="tweet-container">

            </div>
            <div class="jScrollPaneTrack">
                <div class="jScrollPaneDrag">
                    <div class="jScrollPaneTop"></div>
                    <div class="jScrollPaneBottom"></div>
                </div>
            </div>
        </div>
        <div id="scroll"></div>

    </div>
    <!--<ul class="pager" style="font-size: 11px;">
        <li>
            <a href="#">Предыдущий</a>
        </li>
        <li>
            <a href="#">Следующий</a>
        </li>
    </ul>-->

    <div class="ad">
            <p>Важное<p>
            <ul>
                <li>
                    <a href="#" id="adwert" data-reveal-id="myModal">Информация о новом сайте</a>
                </li>
                <li>
                    <a href="/page/view/id/9">Олимпиада по праву</a>
                </li>
            </ul>
        <div id="myModal" class="reveal-modal" style="display: none;" title="Важное">

            <p style="font-style: italic;font-size: 14px;">Добро пожаловать на новый сайт, в данный момент он запущен в тестовом режиме, команда сайта переносит всю необходимую информацию со <a href="http://old.kfrap.ru">старого сайта</a>.</p><br>

        </div>
    </div>
    <div class="library">
        <div class="library-img"></div>
        <a href="/library">Электронная библиотека</a>
        <p style="color: #777; font-size: 11px;">УМК, программы вступительных испытаний</p>
   </div>
    <div class="library">
        <div class="iprbooks-background" ></div>
        <a href="http://www.iprbookshop.ru/">IPRbooks</a>
        <p style="color: #777; font-size: 11px;">Элетронная библиотечная система</p>
    </div>
    <div class="library">
        <div class="femida-background" ></div>
        <a href="http://femida.raj.ru/">Фемида</a>
        <p style="color: #777; font-size: 11px;">Элетронная система обучения</p>
    </div>
</div>
<div class="content">
    <div id="content">
            <div class="photos margin_bottom_xx" onmouseover="showSlider();" onmouseout="hideSlider();">
                <?foreach($forSlider as $key => $value):?>
                <div id="slide_photo_<?=$key?>" data-title="<?=$value->title?>" class="photos_img" style="background-image: url('<?=SITE_URL?>/upload/images/<?=$value->slider_picture?>'); <?=($key==0)?'z-index:3;':''?>">
                    <div class="photos_link_button">
                        <div class="photos_link_button_content">
                            <div class="photos_link_button_label" onclick="document.location.href = '/news/<?=$value->id?>';">ПОДРОБНЕЕ >></div>
                        </div>
                    </div>
                    <div class="photos_line_bottom"></div>
                </div>
                <?endforeach?>


                <div id="slide_photo_slider" class="photos_slider" style="display: none; " onmouseover="imgOut = false;" onmouseout="imgOut = false;">
                <table class="grid">
                    <tbody>
                    <tr>
                        <td class="photos_slider_title"><?=(isset($forSlider[0]))?$forSlider[0]->title:''?></td>
                        <td class="photos_slider_buttons">
                            <?foreach($forSlider as $k => $v):?>
                                <div class="photos_button_wrapper">
                                    <div id="slide_photo_<?=$k?>_link" class="photos_button photos_button_off" style="display: block; " onclick="changeImage('<?=$k?>');"></div>
                                    <div id="slide_photo_<?=$k?>_link_dis" class="photos_button photos_button_on" style="display: <?=($k==0)?'block':'none'?>; "></div>
                                </div>
                            <?endforeach?>
                        </td>
                    </tr>
                    </tbody>
                </table>
                </div>
            </div>

			<div>
				<div id="tabs">
                <ul id="tab" class="nav nav-tabs">
                    <li class="active"><a href="#all" data-toggle="tab">Все</a></li>
                    <?if(isset($categories)):?>
                        <?for($i=0; $i< count($categories); $i++):?>
                        <li><a href="#<?=$categories[$i]->id?>"><?=$categories[$i]->title?></a></li>
                            <?if($i == 1) break;?>
                        <?endfor?>

                        <?if(count($categories)>1):?>
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Ещё<b class="caret"></b></a>
                                <ul class="dropdown-menu">
                            <?for($i=2; $i< count($categories); $i++):?>
                            <li><a href="#<?=$categories[$i]->id?>" data-toggle="tab"><?=$categories[$i]->title?></a></li>
                            <?endfor?>
                                </ul>
                            </li>
                        <?endif?>

                    <?endif?>
                </ul>
					<div id="news_tabs">
                        <div id='loader'></div>
						<?if(isset($articles)):?>
							<ul id="news">
							<?foreach($articles as $article):?>
                                <?$image = ($article->getImages(true))?$article->getImages(true):(($article->slider_picture)?"/upload/images/{$article->slider_picture}":'')?>
								<li><div id="newsimage"><?=(!empty($image)?'<img height="45px"  src="'.$image.'">':'')?></div>
                                    <a href="<?=SITE_URL.'/news/'.$article->id?>"><?=$article->title?></a>
								<p><?=$article->preview?></p>
								<div class="more"><a href="<?=SITE_URL.'/news/'.$article->id?>"></a></div>
								</li>
							<?endforeach?>
							</ul>
						<?endif?>
					</div>
                    <div class="allnews">
                        <div class="allnews-left"></div>
                        <div class="allnews-right"></div>
                        <p><a href="<?=SITE_URL?>/news">Все новости</a></p>
                    </div>

				</div>

        </div>
    </div>
</div>
<style type="text/css">

</style>


<script type="text/javascript">
    /**********************
     Элементы ли кликабельны, курсор поинтер
     ***********************/
//$(document).ready(function(){

$(".library, #news li, .allnews").click(function(){
    window.location=$(this).find("a").attr("href"); //return false;
});

//});
    //нахрена тут return false и событие ready для объекта документ?
    //вообще самое правильное это 1 строчка onClick="location.href=this.href"; гораздо меньше кода и красивее
</script>


<script type="text/javascript">
    /**********************
     Слайдер
    ***********************/
    var slide_photo_imgIndex = '0',
        slide_photo_showLinks = false, // показывать ли ссылки для изменения изображения
        imgOut = false; // выведен ли курсор мыши за пределы изображения

    function changeImage(index)
    {
        var imgIndex = slide_photo_imgIndex;
        $('#slide_photo_' + imgIndex + '_link_dis').hide();
        $('#slide_photo_' + imgIndex + '_link').show();
        $('#slide_photo_' + index + '_link').hide();
        $('#slide_photo_' + index + '_link_dis').show();
        //
        $('.photos_img').css('zIndex', 1);
        $('#slide_photo_' + imgIndex).css('zIndex', 2);
        $('#slide_photo_' + index).css('zIndex', 3);
        //
        $('#slide_photo_' + index).show('puff', {percent: 100}, 800);
        $('.photos_slider_title').text($('#slide_photo_' + index).attr('data-title'));
        //
        slide_photo_imgIndex = index;
    }

    function showSlider()
    {
        imgOut = false;
        if (!slide_photo_showLinks) {
            $('#slide_photo_slider').show('slide', {direction: 'down'}, 500);
            slide_photo_showLinks = true;
        }
    }

    function hideSlider()
    {
        imgOut = true;
        setTimeout(function() {
            if (imgOut) {
                $('#slide_photo_slider').hide('slide', {direction: 'down'}, 500);
                imgOut = false;
                slide_photo_showLinks = false;
            }
        }, 0);
    }
    /**********************
            Слайдер
    ***********************/

    /*******************************
    Переключение новостей по разделам
    ********************************/
    $(function()
    {
        $('.nav-tabs li:not(.dropdown) a').bind('click', function()
        {
            $('.nav-tabs li:not(.dropdown)').removeClass('active');

            $('li.dropdown').removeClass('open');

            $(this).parent().addClass('active');

            var cat_id = $(this).attr('href').substring(1);

            $('#loader').show();
            $.ajax({
                url: '/index/getNewsByCategory',
                type:'POST',
                dataType : 'json',
                data: {cat_id:cat_id},
                success: function (data, textStatus) {
                    $('#loader').hide();
                    var news_content = '';

                    if(data.length)
                    {
                        for(var i = 0; i < data.length; i ++)
                        {
                            var image = '';
                            if(data[i].image.length > 0)
                            {
                                image = '<img height="45px" src="' +data[i].image+ '"/>'
                            }

                            news_content    += '<li><div id="newsimage">' + image
                                            + '</div><a href="/news/' + data[i].id + '">'
                                            + data[i].title
                                            + '</a><p>'
                                            + data[i].preview
                                            + '</p><div class="more"><a href="/news/' + data[i].id + '"></a></div></li>';


                        }
                    }
                    else
                    {
                        news_content = '<div style="padding: 10px">В данном разделе новости отсутствуют.</div>'
                    }




                    $('#news').html(news_content);
                }
            });

            return false;
        })
    });

    $(function(){
        $( "#myModal" ).dialog({
        			autoOpen: false,
        			show: "blind",
        			hide: "blind",
                    modal: true
        		});
        updateTwitter()
    });

    setInterval(updateTwitter, 30000);

    $('#adwert').click(function(){
        $( "#myModal" ).dialog( "open" );
            return false;
    });

    function updateTwitter()
    {
        $.ajax ({
            url: '/api/twitter',
            dataType: "json",
            success: function(data)
            {
                var content = '';
                for(var i = 0; i < data.length; i++)
                {
                    content += '<div class="tweet"><img class="profile_image_url" src="'+data[i].user_pic+'" width="" height="21">'
                            + '<div class="tweet_info"><div class="user">'
                            +'<a href="http://twitter.com/kazanraj" target="_blank">kazanraj</a></div>'
                            +'<div class="txt">'+data[i].text +'</div><div class="time">'+ data[i].date +'</div></div><div class="clear"></div></div>';
                }

                $('#tweet-container').html(content);
            }
        });
    }
</script>

