<div class="pagenewstext">
    <div style="width: 570px; margin: 0 auto;">
    <form class="form-well" method="POST">
       <h2 style="margin-bottom: 15px;">Активация учетной записи</h2>
        <label >Введите Ваш актуальный e-mail:</label>
        <input type="text" name="user_activate_form[email]" class="span3" placeholder="">
        <p class="help-block" style="margin-bottom: 15px;">Пример: example@yandex.ru<br>  На ваш e-mail будет выслан временный пароль с дальнейшими инструкциями</p>

        <button type="submit" class="btn btn-primary" style="color: #fff;">Получить пароль</button>
    </form>
    </div>
</div>