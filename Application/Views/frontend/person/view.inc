<div class="pageperson">
    <div class="title"><span>Персоналии</span></div>
    <div class="pagenewstext clearfix" >

        <div class="person_left">
            <div class="person_foto">
                <img style="width: 133px; height: 200px;" src="<?=((empty($personObject->photo_link))?"/images/default-person.jpg":"/upload/people/{$personObject->photo_link}")?>">
            </div>
            <?if(!empty($personObject->phone) || !empty($personObject->email)):?>
            <div class="person_contact">
                <table style="width: 133px;">
                    <?if(!empty($personObject->phone)):?>
                    <tr>
                        <td class="b">тел</td>
                    </tr>
                    <tr>
                        <td class="c"><?=$personObject->phone?></td>
                    </tr>
                    <?endif?>
                    <?if(!empty($personObject->email)):?>
                    <tr>
                        <td class="b">E-mail</td>
                    </tr>
                    <tr>
                        <td class="c"><?=$personObject->email?></td>
                    </tr>
                    <?endif?>
                </table>
            </div>
            <?endif?>
        </div>
        <div class="person_right">
            <p><?=$personObject->last_name.' '.$personObject->first_name.' '.$personObject->middle_name ?></p>
            <table class="person_info">
                <?if(!empty($personObject->division_id)):?>
                <tr>
                    <td class="b">Подразделение</td>
                </tr>
                <tr>
                    <td class="c"><?=$personObject->division->name?></td>
                </tr>
                <?endif?>
                <?if(!empty($personObject->appointment)):?>
                <tr>
                    <td class="b">Должность/Уч.ст, уч.звание<td>
                </tr>
                <tr>
                    <td class="c"><?=$personObject->appointment?></td>
                </tr>
                <?endif?>
                <?if(!empty($personObject->activity)):?>
                <tr>
                    <td class="b">Направление деятельности</td>
                </tr>
                <tr>
                    <td class="c">
                    <?php echo $personObject->activity?>
                    </td>
                </tr>
                <?endif?>
            </table>
            <div class="person_about">
                <span>Дополнительная информация</span>
                <div><?php echo $personObject->info?></div>
            </div>
        </div>
    </div>
</div>
