<div class="pagecontent">
    <div class="title"><span>Персоналии</span></div>
    <div class="pagenewstext">
        <div class="seach_person">
            <div class="ui-widget">
                <label for="persons-search">Введите имя, фамилию или отчество: </label>
                <input id="persons-search" style="width:228px; height:18px;;"/>
                <button type="submit" class="search-people-icon"></button>
            </div>
        </div>
        <p>Поиск по алфавиту: </p>
        <div class="alfavit" style="width: 650px;">
            <?for($i=0; $i<count($letters); $i++):?>
            <a href="#"><span><?=$letters[$i]?></span></a>
            <?endfor?>
        </div>
        <div class="person_and_search">
        <div class="allperson">
            <div id='loader'></div>
            <?foreach($peopleArray as $personObject):?>
            <div class="one_person">
                <img class="photo_person" src="<?=((empty($personObject->photo_link))?"/images/default-person.jpg":"/upload/people/{$personObject->photo_link}")?>">
                <div class="one_person_name"><a href="/person/view/id/<?=$personObject->id?>"><?=$personObject->last_name.' '.$personObject->first_name.' '.$personObject->middle_name ?></a></div>
                <div class="one_person_podraz"><?=isset($personObject->division->name)?$personObject->division->name:''?></div>
            </div>
            <?endforeach?>
            <?=$pages?>
        </div>
         <div class="right-search">

             <div class="search_divisions">
                 <span>Иные подразделения:</span>
                 <ul>
                     <?foreach($divisions as $division):?>
                     <li>
                         - <span><a href="#<?=$division->id?>"><?=$division->name?></a></span>
                     </li>
                     <?endforeach?>
                 </ul>
             </div>
        <div class="search_divisions" style="margin-right: 30px;">
            <span>Кафедры:</span>
                    <ul>
                        <?foreach($departments as $department):?>
                        <li>
                            - <span><a href="#<?=$department->id?>"><?=$department->name?></a></span>
                        </li>
                        <?endforeach?>
                    </ul>
                </div>
            </div>
    </div>
        </div>
</div>
<script type="text/javascript">
    function bindPaginate()
    {
        $('.pagination ul li').bind('click', function()
        {
            $.ajax({
                url: $(this).children('a').attr('href'),
                dataType : 'json',
                success: function (data, textStatus)
                {
                    $('#loader').hide();
                    showPeople(data);
                }
            });

            return false;
        });
    }

    $(function(){
        bindPaginate();

        $('.alfavit a').bind('click', function(){
            var letter = $(this).children('span').text();
            $('#loader').show();
            $.ajax({
                url: '/person/search?find_by=letter',
                dataType : 'json',
                data: {letter:letter},
                success: function (data, textStatus)
                {
                    $('#loader').hide();
                    showPeople(data);
                }
            });
            return false;
        });

        $('.search_divisions ul li span a').bind('click', function(){

            var division_id = $(this).attr('href').substring(1);
            $('#loader').show();
            $.ajax({
                url: '/person/search?find_by=division',
                dataType : 'json',
                data: {division:division_id},
                success: function (data, textStatus)
                {
                    $('#loader').hide();
                    showPeople(data);
                }
            });

            return false;
        });
    });


    $(function() {
        function log( message ) {
            $( "<div/>" ).text( message ).prependTo( "#log" );
            $( "#log" ).scrollTop( 0 );
        }

        $( "#persons-search" ).autocomplete({
            source: function( request, response ) {
                $('#loader').show();
                $.ajax({
                    url: "/person/search?find_by=query",
                    dataType: "json",
                    data: {
                        maxRows: 12,
                        query: request.term
                    },
                    success: function( data ) {
                        $('#loader').hide();
                        showPeople(data);
                        response( $.map( data.persons, function( item ) {
                            return {
                                //label: item.name + (item.adminName1 ? ", " + item.adminName1 : "") + ", " + item.countryName,
                                value: item.last_name + ' ' + item.first_name + ' ' + item.middle_name
                            }
                        }));
                    }
                });
            },
            minLength: 0,
            select: function( event, ui ) {
                log( ui.item ?
                    "Selected: " + ui.item.label :
                    "Nothing selected, input was " + this.value);
            },
            open: function() {
                $( this ).removeClass( "ui-corner-all" ).addClass( "ui-corner-top" );
            },
            close: function() {
                $( this ).removeClass( "ui-corner-top" ).addClass( "ui-corner-all" );
            }
        });
    });

    function showPeople(data)
    {
        var people_content = '';

        for(var i = 0; i < data.persons.length; i ++)
        {
            people_content  += '<div id="loader"></div><div class="one_person"><img class="photo_person" src="'
                            + data.persons[i].photo
                            + '"><div class="one_person_name"><a href="/person/view/id/'
                            + data.persons[i].id
                            + '">'
                            + data.persons[i].last_name +' '+ data.persons[i].first_name +' '+ data.persons[i].middle_name
                            + '</a></div><div class="one_person_podraz">'
                            + data.persons[i].division + '</div></div>';
        }



        if(people_content == '')
            people_content = 'По данному запросу никого не найдено!';

        $('.allperson').html(people_content);
        $(".allperson").append(data.pages);

        bindPaginate();

        return false;
    }

    $(".one_person").click(function(){
        window.location=$(this).find("a").attr("href");
    });
</script>
