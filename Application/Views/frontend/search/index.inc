<script type="text/javascript">
    $(document).ready(function(){
        $(".btn-slide").click(function()
        {
            $(".more-search").slideToggle("slow", function ()
            {
                $(".btn-slide").text((($(this).is(':hidden'))?'Развернуть':'Свернуть'));
            });

            return false;
        });
    });
</script>

<div class="pagecontent2">
    <div class="title"><span>Поиск по сайту</span></div>
    <div class="news_all">
        <form class="form-horizontal" action="">
            <fieldset>
                <div class="control-group">
                    <label class="control-label" for="input01">Я ищу:</label>
                    <div class="controls">
                        <input type="text" name="q" class="input-xlarge" id="input01" value="<?=isset($query)?$query:''?>">
                        <p class="help-block">Если вы ищите персоналий воспользуйтесь следующей <a href="<?=SITE_URL?>/person/all">формой</a></p>
                    </div>
                </div>
                   <div class="more-search <?=(isset($results))?'none':'block'?>" style="display:<?=(isset($results))?'none':'block'?>;">
                    <div class="control-group">
                        <label class="control-label">Слова:</label>
                        <div style="float: left; width: 200px; height: 70px; margin-left: 20px;" >
                            <p>расположены:</p>
                            <label class="radio">
                                <input type="radio" name="entry" id="optionsRadios2" value="title">
                                В заголовке
                            </label>
                            <label class="radio">
                                <input type="radio" name="entry" id="optionsRadios1" value="content">
                                В тексте
                            </label>
                        </div>
                        <div class="control-group" style="float: left;">
                            <p>употреблены в тексте:</p>
                            <label class="radio">
                                <input type="radio" id="optionsRadios3">
                                В любой форме
                            </label>
                            <label class="radio">
                                <input type="radio" id="optionsRadios4">
                                Обязательно встречаются все слова
                            </label>
                            <label class="radio">
                                <input type="radio" id="optionsRadios5">
                                Точно так, как в запросе
                            </label>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label">Область поиска:</label>
                        <div class="search-by" style="float: left;  margin-left: 20px;margin-bottom: 20px;padding-top: 5px;position: relative;" >
                            <label  style="float: left;margin-right: 114px;">
                                <input type="radio" name="type" id="search-by-pages" value="page">
                                Материалы
                            </label>
                            <label style="float: right;">
                                <input type="radio" name="type" id="search-by-news" value="news">
                                Новости
                                <ul style="float: left;  display: none;" id="news-categories">
                                    <label class="checkbox"></label>
                                    <input type="checkbox" id="all-category">
                                    Все
                                    <?foreach($newsCategories as $category):?>
                                    <label class="checkbox">
                                        <input type="checkbox" name="categories[]" class="news-category" value="<?=$category->id?>">
                                        <?=$category->title?>
                                    </label>
                                    <?endforeach?>
                                </ul>
                            </label>

                    </div>
                    <!--<div class="control-group">
                        <label class="control-label" for="select1" style="clear: both;">Размер страницы:</label>
                        <div style="float: left; width: 250px; margin-left: 20px; height: 70px;" >
                            <p>показывать по:</p>
                            <select id="select1">
                                <option>10</option>
                                <option>20</option>
                                <option>30</option>
                            </select>
                        </div>
                    </div>-->
                   </div>
                       </div>
                <div class="form-actions">
                    <button type="submit" class="btn btn-primary">Найти</button>
                    <a class="btn-slide" style="text-decoration: underline; float: right; cursor: pointer;"><?=(isset($results))?'Развернуть':'Свернуть'?></a>
                </div>
            </fieldset>
        </form>
        <?if(isset($results)):?>
        <div class="result-search" style="margin-left: 10px; margin-right: 100px;">
            <?if(empty($results)):?>
            <div class="title"><span>По данному запросу ничего не найдено.</span></div>
            <?else:?>
            <table >
                <thead>
                    <div class="title"><span>Всего найдено <?=count($results)?> результатов.</span></div>
                    <?$cnt=0?>
                    <?foreach($results as $result):?>
                    <tr >
                        <td style="padding: 10px; font-size: 14px;"><?=++$cnt?></td>
                        <td style="padding: 10px; font-size: 14px;">
                            <p><strong><a href="<?=$result['type']?>/view/id/<?=$result['id']?>"><?=$result['title']?></a></strong></p>
                            <p style="padding: 6px 0 0 10px; font-size: 13px;">...<?=$result['context']?></p>
                        </td>
                    </tr>
                    <?endforeach?>
                </thead>
            </table>
            <?endif?>
        </div>
        <?endif?>
    </div>

    </div>

    <script type="text/javascript">
        $(function(){
            $("#all-category").bind('click',function(){
                if($(this).attr("checked") == "checked")
                {
                    $(".news-category").attr("checked", "checked");
                }
                else
                {
                    $(".news-category").removeAttr("checked");
                }
            });

            $(".search-by > label > input").bind('click',function(){
                if($(this).val() == 'news')
                {
                    $("#news-categories").show();
                }
                else
                {
                    $("#news-categories").hide();
                }

            });
        });
    </script>