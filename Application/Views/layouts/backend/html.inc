<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml: lang="ru" lang="ru">
<head>
    <link rel="stylesheet" type="text/css" href="<?=SITE_URL?>/css/layouts/frontend/jquery-ui-1.8.19.custom.css">
    <link rel="StyleSheet" type="text/css" href="<?=SITE_URL?>/css/layouts/backend/docs.css"/>
    <link rel="stylesheet" type="text/css" href="http://twitter.github.com/bootstrap/assets/css/bootstrap.css">
    <link rel="StyleSheet" type="text/css" href="<?=SITE_URL?>/css/layouts/backend/style.css"/>
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js" type="text/javascript"></script>
    <script src="<?=SITE_URL?>/js/jquery-ui-1.8.19.custom.min.js" type="text/javascript"></script>
    <script src="<?=SITE_URL?>/js/i18n/jquery-ui-i18n.js" type="text/javascript"></script>
    <script type="text/javascript" src="<?=SITE_URL?>/js/bootstrap-tooltip.js"></script>
    <script type="text/javascript" src="<?=SITE_URL?>/js/jquery.ba-url.js"></script>
    <script type="text/javascript" src="<?=SITE_URL?>/js/bootstrap-tab.js"></script>
    <script type="text/javascript" src="<?=SITE_URL?>/js/bootstrap-dropdown.js"></script>
    <script type="text/javascript" src="<?=SITE_URL?>/js/bootstrap-scrollspy.js"></script>
    <script type="text/javascript" src="<?=SITE_URL?>/js/editors/ckeditor/ckeditor.js"></script>
    <script type="text/javascript">
        $(function(){
            $('.tooltip').bind('mouseover', function(){
                var title = $(this).attr('title');
                var tooltipTitle = '<div>' +title+ '</div>';
                $(this).parent().append(tooltipTitle);
            }).bind('mouseout', function(){
                $('#tooltip-title').remove();
            });



            $('.ajax').click(function(){
                var type;
                if($(this).hasClass('post'))
                    type = 'POST';
                else
                    type = 'GET';

                ajax_load($(this).attr('rel'), $(this).attr('href'), $(this).attr('params'), type);
            
                return false;
            });
        });

        function ajax_load (container, url, params, type) {
            if (!$.isArray(params)) {
                params = $.queryString(params);
            }
            $.ajax({
                type: type,
                url: url,
                data: params,
                success: function (data) {
                    $(container).html(data);
                }
            });
        }
    </script>
    <style type="text/css">
      body {
        padding-top: 60px;
        padding-bottom: 40px;
      }
      .sidebar-nav {
        padding: 9px 0;
      }
    </style>
</head>
<body>
    <div class="navbar navbar-fixed-top">
        <div class="navbar-inner">
            <div class="container-fluid">
            <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </a>
            <a class="brand" href="<?=SITE_URL.'/'.ADMIN_PATH.'/'?>">КФ РАП</a>
            <div class="nav-collapse">
                <?if($this->adminMenu):?>
                    <ul class="nav">
                        <?foreach($this->adminMenu as $value):?>
                        <li <?//=($this_page == $value['link'])?'class="active"':''?>>
                            <a href="<?=SITE_URL.'/'.ADMIN_PATH.'/'.$value['link']?>"><?=$value['name']?></a>
                        </li>
                        <?endforeach?>
                    </ul>
                    <ul class="nav pull-right">
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><?php echo \Application\Models\User::getLogin()?> <b class="caret"></b></a>
                            <ul class="dropdown-menu">
                                <li><a href="/user/account">Мой аккаунт</a></li>
                                <li><a href="/user/personal">Мои данные</a></li>
                                <li class="divider"></li>
                                <li><a href="/backend/logout">Выход</a></li>
                            </ul>
                        </li>
                    </ul>
                <?endif?>
            </div><!--/.nav-collapse -->
        </div>
      </div>
    </div>

    <div class="container-fluid">
      <div class="row-fluid">
        <div class="span3" style="width:200px">
          <div class="well sidebar-nav">
            <ul class="nav nav-list">
              <li class="nav-header">Быстрая навигация</li>
              <li class="active"><a href="#">Добавить новость</a></li>
              <li><a href="#">Создать страницу</a></li>
              <li><a href="#" class="tooltip" rel="tooltip" title="first tooltip">Link</a></li>
              <li><a href="#">Link</a></li>
              <li class="nav-header">Sidebar</li>
              <li><a href="#">Link</a></li>
              <li><a href="#">Link</a></li>
              <li><a href="#">Link</a></li>
              <li><a href="#">Link</a></li>
              <li><a href="#">Link</a></li>
              <li><a href="#">Link</a></li>
              <li class="nav-header">Sidebar</li>
              <li><a href="#">Выйти</a></li>
            </ul>
          </div><!--/.well -->
        </div><!--/span-->
        <div class="span9">
            <?foreach($this->notices as $notice):?>
                <?=$notice->show()?>
            <?endforeach?>
          <?=$content?>
            </div><!--/span-->
          </div><!--/row-->
        </div><!--/span-->
      </div><!--/row-->

      <hr>

      <footer style="text-align: center">
        <p>© kfrap <?=date('Y')?></p>
      </footer>

    </div><!--/.fluid-container-->
</body>