<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml: lang="ru" lang="ru">
<head>
    <link rel="stylesheet" type="text/css" href="<?=SITE_URL?>/css/layouts/frontend/jquery-ui-1.8.19.custom.css"/>
    <link rel="stylesheet" type="text/css" href="<?=SITE_URL?>/css/layouts/frontend/style.css"/>
    <link rel="icon" href="<?=SITE_URL?>/images/icon_home.png" type="image/png" />
    <meta name='yandex-verification' content='5697fbb8c44202d9' />
</head>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js" type="text/javascript"></script>
<script src="<?=SITE_URL?>/js/jquery-ui-1.8.19.custom.min.js" type="text/javascript"></script>
<script src="<?=SITE_URL?>/js/i18n/jquery-ui-i18n.js" type="text/javascript"></script>
<script type="text/javascript" src="<?=SITE_URL?>/js/jquery.ba-url.js"></script>
<script type="text/javascript" src="<?=SITE_URL?>/js/bootstrap-tab.js"></script>
<script type="text/javascript" src="<?=SITE_URL?>/js/bootstrap-dropdown.js"></script>
<script src="<?=SITE_URL?>/js/main-menu.js" type="text/javascript"></script>
<!-- Yandex.Metrika counter -->
<script type="text/javascript">
    (function (d, w, c) {
        (w[c] = w[c] || []).push(function() {
            try {
                w.yaCounter15261580 = new Ya.Metrika({id:15261580, enableAll: true});
            } catch(e) {}
        });

        var n = d.getElementsByTagName("script")[0],
                s = d.createElement("script"),
                f = function () { n.parentNode.insertBefore(s, n); };
        s.type = "text/javascript";
        s.async = true;
        s.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//mc.yandex.ru/metrika/watch.js";

        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f);
        } else { f(); }
    })(document, window, "yandex_metrika_callbacks");
</script>
<noscript><div><img src="//mc.yandex.ru/watch/15261580" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->
<script type="text/javascript">

    var _gaq = _gaq || [];
    _gaq.push(['_setAccount', 'UA-32629048-1']);
    _gaq.push(['_trackPageview']);

    (function() {
        var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
        ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
    })();

</script>
<script type="text/javascript">

    var show = false; //true показывать приветствие всегда false показать только 1 раз

    $(function(){
        $('.ajax').click(function(){
            var type;
            if($(this).hasClass('post'))
                type = 'POST';
            else
                type = 'GET';

            ajax_load($(this).attr('rel'), $(this).attr('href'), $(this).attr('params'), type);

            return false;
        });
    });

    function ajax_load (container, url, params, type) {
        if (!$.isArray(params)) {
            params = $.queryString(params);
        }
        $.ajax({
            type: type,
            url: url,
            data: params,
            success: function (data) {
                $(container).html(data);
            }
        });
    }
</script>
<body>
<div class="top"></div>
<div id="page-wrapper">
    <div id="featured">
        <div class="container clearfix">
            <div class="first">
                <img src="<?=SITE_URL?>/images/emblem.png" alt=""><h4>Учредители:</h4>
                <p><a href="http://www.vsrf.ru/">Верховный Суд РФ</a></p>
                <p><a href="http://www.arbitr.ru">Высший Арбитражный Суд РФ</a></p>
            </div>
            <div>
                <img src="<?=SITE_URL?>/images/phone.png" alt=""><h4>Приемная комиссия:</h4>
                <p style="font-size: 14px; font-weight: bold; color: #999;">(843) 272 47 16</p>
                <span ><a href="<?=SITE_URL?>/page/view/id/6">Контакты</a></span> | <span><a href="<?=SITE_URL?>/location">Схема проезда</a></span>
            </div>
            <div>
                <img src="<?=SITE_URL?>/images/system-help.png" alt=""><h4>Вопрос-ответ:</h4>
                <p>Задавайте интересующие Вас вопросы:</p>
                <span ><a href="<?=SITE_URL?>/questions">Архив ответов</a></span> | <span><a id ='question' href="">Задайте вопрос</a></span>
            </div>
        </div>
    </div>
    <div id="header">
        <h1>
            <a href="/">
                "Российская академия правосудия"
                <span></span>
            </a>
        </h1>
        <div id="search_block">
            <label>
                <a href="<?=SITE_URL?>/search"><span style="text-shadow: #0f4774 0 -1px 0;">Расширенный поиск</span></a>
                <span style="color: white;text-shadow: #0f4774 0 -1px 0;">|</span>
                <a href="<?=SITE_URL?>/person/all"><span style="text-shadow: #0f4774 0 -1px 0;">Персоналии</span></a>
            </label>
            <div>
                <? $formHelper->openForm('/search', 'GET', array('class' => 'well form-search')) ?>
                <? $formHelper->text(array('name' => 'q', 'size' => '15', 'id' => 'search-query', 'class' => 'input-medium search-query')) ?>
                <? $formHelper->submit(array('value' => '', 'id' => 'button', 'onClick' => 'if(document.getElementById("search-query").value.length > 2) return true; else alert("Запрос должен содержать не менее 3х символов!"); return false;')) ?>
                <? $formHelper->closeForm() ?>
            </div>
        </div>
    </div>
    <div id="main-menu">
        <div class="menu">
            <div class="menu-left"></div>
            <div class="menu-right"></div>
            <div id="primary">
                <?if(isset($this->mainMenu) and !empty($this->mainMenu)):?>
                <ul>
                    <?foreach($this->mainMenu as $value):?>
                    <li><a href="<?=$value['url']?>"><?=$value['name']?></a></li>
                    <?endforeach?>
                </ul>
                <?endif?>
            </div>
            <ul class="socseti">
                <li><a href="http://vk.com/club356953" rel="external" class="vk"></a><li>
                <li><a href="https://twitter.com/@kazanraj" rel="external" class="tw"></a><li>
                <!--<li><a href="#" class="fb"></a><li>-->
            </ul>
        </div>
        <?if(!empty($this->mainMenu)):?>
        <?foreach($this->mainMenu as $keyParent => $val):?>
        <?if(!isset($val['parents']) or empty($val['parents'])):?>
        <?continue?>
        <?endif?>
        <div class="navbar-item-sub nav-<?=$keyParent?>">
            <div class="navbar-item-sub-inner">
                <div class="mark"></div>
                <?
					//делим меню на 2 колонки;)
					$cnt = count($val['parents']);
					//print_r($val['parents']); exit;
					$primCnt = $cnt/2;
					if(isset($primCnt)) {
						$colFirst = ceil($primCnt);
						$colSecond = floor($primCnt);
					}
					?>
                <ul>
                    <?for($i=0; $i < $cnt; $i++):?>
                    <?if($i == $colFirst):?>
                </ul>
                <ul class="last">
                    <?endif?>
                    <li><a href="<?=$val['parents'][$i]['url']?>"><?print($val['parents'][$i]['name'])?></a></li>
                    <?endfor?>
                </ul>
            </div>
        </div>
        <?endforeach?>
        <?endif?>
    </div>
    <div class="page2">
        <?if($this->showBreadcrumb):?>
        <div id="breadcrumb">
            <div class="map">
                <a class="breadcrumbHome" href="/index/"><img src="/images/icon_home.png"> Главная</a>
                <?foreach($this->breadcrumbs as $breadcrumb => $url):?>
                <span>→</span>
                <a href="<?=$url?>"><?=$breadcrumb?></a>
                <?endforeach?>
            </div>
        </div>
        <?endif?>

        <?if($this->showPageMenu):?>
        <!-- begin right menu block -->
        <?$this->pageMenu = (!is_array($this->pageMenu))?array($this->pageMenu):$this->pageMenu?>
        <?foreach($this->pageMenu as $pmenu):?>
        <div class="subnav">

            <ul class="nav nav-pills">
                <?foreach($pmenu->menu_items as $pMenuItem):?>
                <?if($pMenuItem->active == 0) continue?>
                <li>
                    <a href="<?=$pMenuItem->url?>"><?=$pMenuItem->name?></a>
                </li>
                <?endforeach?>
            </ul>

        </div>
        <?endforeach?>
        <!-- end right menu block -->
        <?endif?>

        <?if($this->showBlockPeople):?>
        <!-- begin right people block -->
        <div class="person_block">
            <img src="/images/officer.png">
            <p>Персоналии</p>
            <ul>
                <?foreach($this->blockPeople as $person):?>
                <li>
                    <a href="/person/view/id/<?=$person->id?>"><?=$person->last_name?> <?=$person->first_name?> </a>
                </li>
                <?endforeach?>
            </ul>
        </div>
        <!-- end people block -->
        <?endif?>

        <!--begin content-->
        <?echo $content?>
        <!-- -->

    </div>








    <div class="footer">
        <div class="head"></div>
        <div id="footer1">
            <div id="footer2">
                <div class="fmenu" id="f1">
                    <div>Учредители</div>
                    <ul>

                        <li><a href="http://www.vsrf.ru/">Верховный Суд РФ</a></li>

                        <li><a href="http://www.arbitr.ru">Высший Арбитражный Суд РФ</a></li>

                    </ul>
                </div>
                <div class="fmenu" id="f2">
                    <div>Полезное</div>
                    <ul>

                        <li><a href="http://www.kazan2013.ru/">Универсиада 2013</a></li>

                        <li><a href="http://http://www.ligastudentov.ru//">Лига студентов</a></li>

                        <li><a href="http://www.rajbook.ru/">Интернет магазин "Книги Академии"</a></li>
                    </ul>
                </div>
                <div class="fmenu" id="f3">
                    <div>Дополнительная информация</div>
                    <ul>
                        <li id="mpsite"><a href="#">Карта сайта</a></li>
                        <li id="cont"><a href="<?=SITE_URL?>/page/view/id/6">Контакты</a></li>
                        <li id="nizseache"> <a href="<?=SITE_URL?>/search">Расширенный поиск</a></li>
                    </ul>
                </div>
            </div>
            <div style="position: absolute; bottom: -40px; left: 20px; height:30px; width:948px;">
                <p style="float: left; color:#777;">© 2012 Казанский филиал Российской академии правосудия </p>
                <a style="float:right; text-decoration: underline;" href="http://old.kfrap.ru/"><span>Старая версия сайта</span></a>
            </div>
        </div>
    </div>
</div>

<div id="dialog-form" title="Новый вопрос">
    <div id='answer-loader'></div>
    <p class="validateTips" style="padding: 4px; margin: 6px;"></p>
    <form class="form-horizontal">
        <fieldset>
            <div class="control-group">
                <label class="control-label" for="last_name">Фамилия *</label>
                <div class="controls">
                    <input type="text" id="last_name" placeholder="Иванов">
                </div>
            </div>
            <div class="control-group">
                <label class="control-label" for="first_name">Имя *</label>
                <div class="controls">
                    <input type="text" id="first_name" placeholder="Иван">
                </div>
            </div>
            <div class="control-group">
                <label class="control-label" for="middle_name">Отчество</label>
                <div class="controls">
                    <input type="text" id="middle_name" placeholder="Иванович">
                </div>
            </div>
            <div class="control-group">
                <label class="control-label" for="enable_email">Email</label>
                <div class="controls">
                    <label class="checkbox">
                        <input type="checkbox" id="enable_email" value="option1">
                        Получить ответ по Email
                    </label>
                </div>
            </div>
            <div class="control-group">
                <div class="controls">
                    <input type="text" id="email" disabled="disabled">
                    <p class="help-block">Укажите Ваш электронный ящик, для того, что бы получить на него ответ.</p>
                </div>
            </div>

            <div class="control-group">
                <label class="control-label" for="division">Подразделение *</label>
                <div class="controls">
                    <select id="division">
                        <option>something</option>
                        <option>2</option>
                        <option>3</option>
                        <option>4</option>
                        <option>5</option>
                    </select>
                    <p class="help-block">Укажите подразделение, которому адресован Ваш вопрос.</p>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label" for="subject">Тема обращения *</label>
                <div class="controls">
                    <input type="text" id="subject">
                </div>
            </div>
            <div class="control-group">
                <label class="control-label" for="text">Текст обращения *</label>
                <div class="controls">
                    <textarea class="input-xlarge" id="text" rows="8"></textarea>
                </div>
            </div>
            <div class="control-group" style="padding-left: 20px">
                * Поля обязательные для заполнения
            </div>
        </fieldset>
    </form>
</div>
<div id="question-success" style="display: none">
    <p>
        <span class="ui-icon ui-icon-circle-check" style="float:left; margin:0 7px 50px 0;"></span>
        Ваше сообщение успешно отправлено!
    </p>
    <p>
        После прохождение модерации ответ будет размещен на <b>странице вопросов</b>.
    </p>
</div>

<div id="boxes">
    <div id="salutation-dialog" class="window">
        <div id="hello-header"> <img style="width: 65px; height:72px;;" src="<?=SITE_URL?>/images/logotipe.jpg" alt="">
            <div class="l"><p>Верховный Суд Российской Федерации</p></div>

            <div class="r"><p>Высший Арбитражный Суд Российской Федерации</p></div>
        </div>
        <div class="clear"></div>
        <div style="border-bottom:1px solid #ccc;">
            <h3 style="text-align: center; margin-top: 50px;">Казанский филиал</h3>
            <h4 style="text-align: center;">Федерального государственного бюджетного образовательного учреждения высшего профессионального образования</h4>
            <h1 style="text-align: center; padding: 0 10px;">"Российская академия правосудия"</h1>
        </div>
        <div style="margin: 20px 0; padding-left: 10px; padding-right: 10px; text-align: center;">
            <p style="font-size: 14px; color: #222; padding-bottom: 15px; font-weight: bolder;">Государственный вуз – Российская академия правосудия</p>
            <p style="font-size: 14px; color: #333; font-style: italic; padding-bottom: 15px;">Если у вас возникли вопросы: «куда поступить после 11 классов?», «куда пойти учиться по программе второго высшего образования?» – сделайте правильный выбор. </p>
            <p style="font-size: 13px; color: #333;">Российская академия правосудия – юридический вуз, реализующий целый ряд образовательных программ среднего профессионального, высшего образования и программ довузовской подготовки. РАП является государственным вузом, где преподают практикующие судьи и юристы, выдающиеся ученые в области права. Именно здесь можно получить достойное образование по различным специальностям и направлениям подготовки.</p>
        </div>
        <div align="center">

        </div><div align="right" style="position: absolute; right:16px; top:6px; font-size: 20px;"><a href="#" class="close">×</a></div>
        <div style="height: 360px; margin-left: 125px">
            <p style="font-size: 16px; color: #999;"><span style="display:block;">Видеознакомство с академией</span></p>
            <iframe style="border: none;" preventhide="1" type="text/html" width="520" height="290" src="http://www.youtube.com/embed/5AyjkquxYVs?autoplay=0&amp;autohide=1&amp;wmode=opaque&amp;showinfo=0" frameborder="0"></iframe></div>
        <div align="right" style="padding-right: 16px"></div>
        <!--<a href="#"class="close"/>Закрыть</a>-->
    </div>
</div>
<div id="mask"></div>
</body>
<script>
$(function() {
    $( "#dialog:ui-dialog" ).dialog( "destroy" );

    var last_name   = $( "#last_name" ),
            first_name  = $( "#first_name" ),
            middle_name = $( "#middle_name" ),
            email       = $( "#email" ),
            division    = $( "#division" ),
            subject     = $( "#subject" ),
            text        = $( "#text" ),
            tips = $( ".validateTips" );

    allFields = $( [] ).add( last_name ).add( first_name ).add( middle_name ).add( division ).add( email ).add( subject ).add( text );

    function updateTips( t ) {
        tips
                .text( t )
                .addClass( "ui-state-highlight" );
        setTimeout(function() {
            tips.removeClass( "ui-state-highlight", 1500 );
        }, 500 );
    }

    function checkLength( o, n, min, max ) {
        if ( o.val().length > max || o.val().length < min ) {
            o.addClass( "ui-state-error" );
            if(o.val().length < min)
                updateTips( "Поле \"" + n + "\" обязательна для заполнения!");

            if(o.val().length > max)
                updateTips( "Поле \"" + n + "\" не может содержать больше "+ max +" символов!");
            return false;
        } else {
            return true;
        }
    }

    function checkRegexp( o, regexp, n ) {
        if ( !( regexp.test( o.val() ) ) ) {
            o.addClass( "ui-state-error" );
            updateTips( n );
            return false;
        } else {
            return true;
        }
    }

    $( "#dialog-form" ).dialog({
        autoOpen: false,
        height: 720,
        width: 560,
        show: "blind",
        hide: "blind",
        modal: true,
        buttons: {
            "Отправить вопрос": function() {
                var bValid = true;
                allFields.removeClass( "ui-state-error" );

                bValid = bValid && checkLength( last_name, "Фамилия", 1, 30 );
                bValid = bValid && checkLength( first_name, "Имя", 1, 30 );
                bValid = bValid && checkLength( subject, "Тема обращения", 1, 40 );
                bValid = bValid && checkLength( subject, "Текст обращения", 1, 800 );
                //bValid = bValid && checkLength( password, "password", 5, 16 );

                bValid = bValid && checkRegexp( last_name, /^[а-я]([а-я_])+$/i, "Поле \"Фамилия\" должна содержать только буквы русского алфавита." );
                bValid = bValid && checkRegexp( first_name, /^[а-я]([а-я_])+$/i, "Поле \"Имя\" должна содержать только буквы русского алфавита." );

                if(middle_name.val() != '')
                    bValid = bValid && checkRegexp( middle_name, /^[а-я]([а-я_])+$/i, "Поле \"Отчество\" должна содержать только буквы русского алфавита." );

                if($( "#enable_email" ).get(0).checked)
                    bValid = bValid && checkRegexp( email, /^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i, "Неправильный формат Email, например mail@kfrap.ru" );

                if ( bValid )
                {
                    $('#answer-loader').show();

                    var q_params = {
                        first_name:first_name.val(),
                        last_name:last_name.val(),
                        middle_name:middle_name.val(),
                        division_id:division.val(),
                        email:email.val(),
                        mail_notice:($( "#enable_email" ).get(0).checked)?1:0,
                        subject:subject.val(),
                        text:text.val()
                    };

                    $.ajax({
                        url: "/index/add_question",
                        type: "POST",
                        data: {question_params:q_params},
                        dataType: "json",
                        success: function( data )
                        {
                            if(data.status == 'success')
                            {
                                $('#answer-loader').hide();
                                $( "#dialog-form" ).dialog('close');

                                $( "#question-success" ).dialog({
                                    modal: true,
                                    buttons: {
                                        'ок': function() {
                                            $( this ).dialog( "close" );
                                        }
                                    }
                                });
                            }

                            //todo вывод ошибки валидации
                        }
                    });
                }
            },
            'Отмена': function() {
                $( this ).dialog( "close" );
                $('#answer-loader').hide();
            }
        },
        close: function() {
            allFields.val( "" ).removeClass( "ui-state-error" );
        }
    });

    $( "#question" ).click(function()
    {
        $( "#dialog-form" ).dialog( "open" );

        $.ajax({
            url: "/index/get_divisions",
            type: "POST",
            dataType: "json",
            success: function( data )
            {
                $('#division').html('');
                for(var i = 0; i < data.length; i++)
                {
                    $('#division').append('<option value="'+data[i].id+'">' +data[i].name+ '</option>');
                }
            }
        });

        return false;
    });

    $( "#enable_email" ).click(function()
    {
        if (this.checked)
        {
            $('#email').removeAttr('disabled');
        }
        else
        {
            $('#email').attr('disabled', 'disabled');
        }
    });
});

$(document).ready(function() {

    if(getCookie('showedSalutation') == null)
    {
        setCookie('showedSalutation', true);
        show = true;
    }

    if(show)
    {
        var id = "#salutation-dialog";

        var maskHeight = $(document).height();
        var maskWidth = $(window).width();

        $('#mask').css({'width':maskWidth,'height':maskHeight});

        $('#mask').fadeIn(1000);
        $('#mask').fadeTo("slow",0.8);

        var winH = $(window).height();
        var winW = $(window).width();

        $(id).css('top',  winH/2-$(id).height()/2);
        $(id).css('left', winW/2-$(id).width()/2);

        $(id).fadeIn(2000);

        $('.window .close').click(function (e) {
            e.preventDefault();

            $('#mask, .window').hide();
        });

        $('#mask').click(function () {
            $(this).hide();
            $('.window').hide();
        });
    }
    else
    {

    }
});
</script>


</body>
</html>