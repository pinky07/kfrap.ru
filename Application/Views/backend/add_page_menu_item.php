<? include('elements/head.php') ?>
<script language="javascript" type="text/javascript">
	function getLinkType() {
		var link = document.getElementById('LinkType').value;
		
		//var url = '<?=SITE_URL?>/admin/menu/ajax';
		
		/*$.post('url', 
			{ 
			req:'getLinkType',
			link:link },
			function(data){
			document.getElementById("links").innerHTML = data;
		  });*/
		
		var url = '<?=SITE_URL?>/admin/menu/ajax';
				ajax({
				url:url,
				method:"POST",
				data:{	req:'getLinkType',
						link:link},
				load:function(){document.getElementById("links").innerHTML = 'Загрузка';},
				success:function(data){document.getElementById("links").innerHTML = data}
			})
	}
</script>
<table border="0" width="860px" align="center">
<tr>
	<td height="80px">
	</td>
</tr>
<tr>
	<td>
		<?include_once('elements/admin_menu.php')?>
	</td>
</tr>
<tr>
	<td height="400px" valign="top" align="center">
		<a href="<?=SITE_URL?>/<?=ADMIN_PATH?>/page/menuItem/menuId/<?=$pageMenu['menu_id']?>" style="float:left; padding:2px 4px 6px 0px;">Назад</a>
		<?if(isset($menuItem)):?>
		<form action="<?=SITE_URL.'/'.ADMIN_PATH.'/page/editMenuItem/menuId/'.$pageMenu['menu_id'].'/itemId/'.$menuItem['id']?>" method="POST">
		<?else:?>
		<form action="<?=SITE_URL.'/'.ADMIN_PATH.'/page/addMenuItem/menuId/'.$pageMenu['menu_id']?>" method="POST">
		<?endif?>
		<table width="100%" border="0">
		<tr bgcolor="#eee">
			<th colspan="3">
			Добавить пункт к меню <?=$pageMenu['menu_name']?>
			</th>
		</tr>
		<tr bgcolor="#fafafa">
			<td width="20%">Название:</td>
			<td colspan="2">
				<input type="text" name="name" value="<?=(isset($menuItem['name'])?$menuItem['name']:'')?>"/>
			</td>
		</tr>
		<tr bgcolor="#fafafa">
			<td width="20%">URL:</td>
			<td width="20%">
				<select id="LinkType" name="LinkType" onChange="getLinkType(); return false;">
					<option disabled="disabled">--Выбрать--</option>
					<option value="page">Страница</option>
					<option value="news">Новость</option>
					<option value="url">Внешняя ссылка</option>
				</select>
			</td>
			<td><div id="links">
				<input type="text" name="url" size="30" value="<?=(isset($menuItem['url'])?$menuItem['url']:'')?>"/>
				</div>
			</td>
		</tr>
		<tr bgcolor="#fafafa">
			<td width="20%">Сортировка:</td>
			<td colspan="2">
				<input type="text" name="sort" value="<?=(isset($menuItem['sort'])?$menuItem['sort']:'')?>"/>
			</td>
		</tr>
		<tr>
			<td>Активность:</td>
			<td colspan="2"><input name="active" type="checkbox" <?=(isset($menuItem['active']))?($menuItem['active'] == 1)?'checked':'':''?>/></td>
		</tr>
		<tr bgcolor="#fafafa">
			<td>&nbsp;</td>
			<td valign="top" colspan="2">
				<?if(isset($menuItem)):?>
					<input type="submit" name="edit" value="изменить">
				<?else:?>
					<input type="submit" name="add" value="добавить">
				<?endif?>
			</td>
		</tr>
		</table>
		</form>
	</td>
</tr>
<tr>
	<td height="40px">
	</td>
</tr>
</table>