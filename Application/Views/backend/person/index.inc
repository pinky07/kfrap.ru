<div class="container">
    <div class="page-header">
        <h1>Персоналии <small>список персон</small></h1>
    </div>
    <div class="btn-toolbar">
        <div class="btn-group">
          <a class="btn btn-primary btn-small" href="/admin/person/add">Новая персона</a>
        </div>
    </div>
    <table class="table">
        <thead>
            <tr>
                <th>#</th>
                <th>ФИО</th>
                <th>Подразделение</th>
                <th>Email</th>
                <th></th>
            </tr>
        </thead>
        <tbody>
        <?foreach($elements as $person):?>
            <tr>
                <td><?=$person->id?></td>
                <td><?=$person->last_name?> <?=$person->first_name?> <?=$person->middle_name?></td>
                <td><?=(!empty($person->division))?$person->division->name:''?></td>
                <td><?=$person->email?></td>
                <td><a href="/admin/person/edit/id/<?=$person->id?>">Редактировать</a></td>
            </tr>
        <?endforeach?>
        </tbody>
      </table>
    <?= $pages?>
</div>