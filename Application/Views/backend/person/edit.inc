<div class="container">
    <div class="page-header">
        <h1>Персоналии <small>редактирование персоны "<?=$person->last_name?> <?=$person->first_name?>"</small></h1>
    </div>
    <div class="row">
    <div class="span8">
        <?$formHelper->openForm('/admin/person/edit/id/'.$person->id, 'POST', array('class'=> 'form-horizontal', 'enctype' => 'multipart/form-data'))?>
        <fieldset>
            <div class="control-group">
                <?$formHelper->label(array('for' => 'photo_link', 'class' => 'control-label'), 'Фотография:')?>
                <div class="controls">
                    <label>
                        <?if(!empty($person->photo_link)):?>
                        <img style="width:210px; padding-bottom: 10px" alt="" src="/upload/people/<?=$person->photo_link?>"/>
                        <br/>
                        <?endif?>
                        <?$formHelper->file(array('name'=>'photo_link', 'id' => 'photo_link'))?>
                    </label>
                </div>
            </div>
            <div class="control-group">
                <?$formHelper->label(array('class' => 'control-label'), 'ФИО:')?>
                <div class="controls docs-input-sizes">
                    <?$formHelper->text(array('name'=>'person_form[last_name]', 'id'=>'last_name', 'value' => $person->last_name, 'placeholder' =>'Фамилия'))?>
                    <?$formHelper->text(array('name'=>'person_form[first_name]', 'id'=>'first_name', 'value' => $person->first_name, 'placeholder' =>'Имя'))?>
                    <?$formHelper->text(array('name'=>'person_form[middle_name]', 'id'=>'middle_name', 'value' => $person->middle_name, 'placeholder' =>'Отчество'))?>
                </div>
            </div>
            <div class="control-group">
                <?$formHelper->label(array('for' => 'division', 'class' => 'control-label'), 'Подразделение:')?>
                <div class="controls">
                    <?$formHelper->select(array('name'=>'person_form[division_id]', 'id'=>'division'), $divisions, $person->division_id)?>
                </div>
            </div>
            <div class="control-group">
                <?$formHelper->label(array('for' => 'appointment', 'class' => 'control-label'), 'Должность:')?>
                <div class="controls">
                    <?$formHelper->textarea(array('name'=>'person_form[appointment]', 'rows' => 2), $person->appointment)?>
                </div>
            </div>
	        <div class="control-group">
                <?$formHelper->label(array('for' => 'activity', 'class' => 'control-label'),
        'Направление деятельности')?>
                <div class="controls">
                <?$formHelper->textarea(array('name'=> 'person_form[activity]', 'rows' => 2), $person->activity)?>
                </div>
            </div>
            <div class="control-group">
                <?$formHelper->label(array('class' => 'control-label'), 'Контакты:')?>
                <div class="controls docs-input-sizes">
                    <?$formHelper->text(array('name'=>'person_form[email]', 'id'=>'email', 'value' => $person->email, 'placeholder' =>'Email'))?>
                    <?$formHelper->text(array('name'=>'person_form[phone]', 'id'=>'phone', 'value' => $person->phone, 'placeholder' =>'Номер телефона'))?>
                </div>
            </div>
            <div class="control-group">
                <?$formHelper->label(array('for' => 'tags', 'class' => 'control-label'), 'Теги:')?>
                <div class="controls">
                    <?$formHelper->textarea(array('name'=>'person_form[tags]', 'rows' => 2), $person->tags)?>
                </div>
            </div>
                <div class="control-group">
                <?$formHelper->label(array('for' => 'info', 'class' => 'control-label'), 'Общая информация:')?>
                <div class="controls">
                    <?$formHelper->textarea(array('name'=>'person_form[info]', 'rows' => 6), $person->info)?>
                </div>
            </div>
            <div class="control-group">
                <?$formHelper->label(array('for' => 'status', 'class' => 'control-label'), 'Опубликовать:')?>
                <div class="controls">
                    <label class="checkbox">
                    <?$formHelper->checkbox(array('name'=>'person_form[publish]'), $person->publish)?>
                    Отображать или скрывать на сайте
                    </label>
                </div>
            </div>
            <div class="form-actions">
                <?$formHelper->submit(array('class' => 'btn btn-primary', 'value' => 'Сохранить'))?>
                <button class="btn" onClick="window.location = '/admin/person'; return false;">Отмена</button>
            </div>
        </fieldset>
      <?$formHelper->closeForm()?>
    </div>
    <div class="span4">
      <!--<h3>What's included</h3>
      <p>Shown on the left are all the default form controls we support. Here's the bulleted list:</p>
      <ul>
        <li>text inputs (text, password, email, etc)</li>
        <li>checkbox</li>
        <li>radio</li>
        <li>select</li>
        <li>multiple select</li>
        <li>file input</li>
        <li>textarea</li>
      </ul>
      <hr>
      <h3>New defaults with v2.0</h3>
      <p>Up to v1.4, Bootstrap's default form styles used the horizontal layout. With Bootstrap 2, we removed that constraint to have smarter, more scalable defaults for any form.</p>
    </div>-->
  </div>
</div>
