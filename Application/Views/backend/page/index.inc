<div class="container">
    <div class="page-header">
        <h1>Страницы <small><?=(isset($responsible)?'список ответственных мне страниц':'список всех страниц сайта')?></small></h1>
    </div>
    <div class="btn-toolbar">
        <div class="btn-group">
          <a class="btn btn-primary btn-small" href="/admin/page/add">Создать страницу</a>
        </div>
        <div class="btn-group">
            <?if(isset($responsible)):?>
                <a class="btn btn-primary btn-small" href="/admin/page">Все страницы</a>
            <?else:?>
                <a class="btn btn-primary btn-small" href="/admin/page/responsible">Мои страницы</a>
            <?endif?>
        </div>
    </div>
    <table class="table">
        <thead>
            <tr>
                <th>#</th>
                <th>Заголовок</th>
                <th>Ответственный</th>
                <th>Статус</th>
                <th></th>
            </tr>
        </thead>
        <tbody>
        <?foreach($elements as $page):?>
            <tr>
                <td><?=$page->id?></td>
                <td><?=$page->title?></td>
                <td><?=($page->responsible_user != null)?$page->responsible_user->surname.' '.$page->responsible_user->name:'Не назначен'?></td>
                <td><?=($page->status)?'Опубликован':'Отключен'?></td>
                <td><a href="/admin/page/edit/id/<?=$page->id?>">редактировать</a></td>
            </tr>
        <?endforeach?>
        </tbody>
    </table>
    <?=$pages?>
</div>