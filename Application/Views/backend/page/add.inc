<div class="container">
    <div class="page-header">
        <h1>Страницы <small>новая страница</small></h1>
    </div>
    <div class="row">
        <div class="span11 columns">
            <?$formHelper->openForm('/admin/page/add', 'POST', array('class'=> 'form-horizontal'))?>
            <fieldset>
                <div class="tabbable" style="margin-bottom: 9px;">
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#settings" data-toggle="tab">Настройки</a></li>
                        <li><a href="#source" data-toggle="tab">Контент</a></li>
                        <li><a href="#view" data-toggle="tab" onclick="view(); return false;">Просмотр</a></li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="settings">
                            <div class="control-group">
                        <?$formHelper->label(array('for' => 'title', 'class' => 'control-label'), 'Заголовок страницы:')?>
                        <div class="controls">
                            <?$formHelper->text(array('name'=>'page_form[title]', 'id'=>'title'))?>
                        </div>
                    </div>
                    <div class="control-group">
                        <?$formHelper->label(array('for' => 'menu', 'class' => 'control-label'), 'Меню страницы:')?>
                        <div class="controls">
                            <?$formHelper->select(array('name' => 'page_form[menu_id]'), $menus)?>
                        </div>
                    </div>
                    <div class="control-group">
                        <?$formHelper->label(array('for' => 'responsible', 'class' => 'control-label'), 'Ответственный:')?>
                        <div class="controls">
                            <?$formHelper->select(array('name' => 'page_form[responsible]'), $users)?>
                        </div>
                    </div>
                    <div class="control-group">
                        <?$formHelper->label(array('for' => 'parent_page', 'class' => 'control-label'), 'Родительская страница:')?>
                        <div class="controls">
                            <?$formHelper->select(array('name' => 'page_form[parent_page_id]'), $pages)?>
                        </div>
                    </div>
                    <div class="control-group">
                        <?$formHelper->label(array('for' => 'layout', 'class' => 'control-label'), 'Макет страницы:')?>
                        <div class="controls">
                            <?$formHelper->select(array('name' => 'page_form[layout]'), array('frontend/html'=> 'С правой колонкой', 'frontend/html2'=> 'Во всю ширину'))?>
                        </div>
                    </div>
                    <div class="control-group">
                        <?$formHelper->label(array('for' => 'status', 'class' => 'control-label'), 'Активность:')?>
                        <div class="controls">
                            <label class="checkbox">
                            <?$formHelper->checkbox(array('name'=>'page_form[status]'))?>
                                Отображение для пользователей
                            </label>
                        </div>
                    </div>
                    <div class="control-group">
                        <?$formHelper->label(array('for' => 'tags', 'class' => 'control-label'), 'Теги:')?>
                        <div class="controls">
                        <?$formHelper->textarea(array('name'=>'page_form[tags]', 'rows' => 4))?>
                            <span class="help-inline">Теги необходимы для связи<br/>с другими объектами например<br/>документы, персоналии, материалы итд.<br/>Каждое слово или словосочетание<br/>необходимо разделять запятой</span>
                        </div>
                    </div>
                    <!--<div class="control-group">
                        <?$formHelper->label(array('for' => 'sef_url', 'class' => 'control-label'), 'ЧПУ:')?>
                        <div class="controls">
                        <?$formHelper->text(array('name'=>'page_form[sef_url]'))?>
                        </div>
                    </div>-->
                    <div class="control-group">
                        <?$formHelper->label(array('for' => 'slider', 'class' => 'control-label'), 'Документы:')?>
                        <div class="controls">
                            <div id="add-doc" class="btn btn-small">Добавить документы</div>
                            <div id="dialog" title="Добавить документы">
                                <select class="multiselect" multiple="multiple" name="page_form[documents][]" style="height: 250px;">
                                    <?foreach($documents as $documentId => $document):?>
                                    <option value="<?=$documentId?>" <?=(is_array(unserialize($page->documents)) && in_array($documentId,unserialize($page->documents)))?'selected':''?>><?=$document?></option>
                                    <?endforeach?>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>

                        <div class="tab-pane" id="source" align="left">

                                <div><?$formHelper->CKEditor(array('name' => 'page_form[content]', 'id' => 'content'))?></div>

                        </div>
                        <div class="tab-pane" id="view" align="left">
                            <div></div>
                        </div>
                    </div>
                </div>
            <div class="form-actions">
                <?$formHelper->submit(array('class' => 'btn btn-primary', 'value' => 'сохранить'))?>
                <button class="btn" onClick="window.location = '/admin/page'; return false;">Отмена</button>
            </div>
        </fieldset>
        <?$formHelper->closeForm()?>
        </div>
    </div>
</div>

<script type="text/javascript">
    function view() {
        var content = document.getElementsByTagName("iframe")[0].contentDocument.getElementsByTagName("body")[0].innerHTML;
        $('#view div').html(content);
    }
</script>
<script type="text/javascript" src="<?=SITE_URL?>/js/ui.multiselect.js"></script>
<script type="text/javascript">
    $(function(){
        $(".multiselect").multiselect({searchable: false});

        $("#dialog").dialog({
            autoOpen: false,
            height:400,
            width:700
        });
        $("#add-doc").click(function(){
            $("#dialog").dialog("open");
        });


    });
</script>