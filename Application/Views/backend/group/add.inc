<section id="buttonGroups">
    <div class="page-header">
        <h1>Управление правами <small>Добавление новой группы</small></h1>
    </div>
         <?$formHelper->openForm();?>
        <table class="table table-condensed">
            <tr>
                <td colspan="3">
                    <label class="control-label" for="name">Название группы</label><?$formHelper->text(array('id' => 'name', 'name' => 'name'));?>
                </td>
            </tr>
            <tr>
                <td>Раздел</td>
                <td>Страницы</td>
                <td>Права</td>
            </tr>
             <?foreach($controllers as $controller => $actions):?>
              <tr>
                  <td rowspan="<?=count($actions)?>" style="vertical-align: middle;"><?=$controller?></td>
                    <?$cnt=1?>
                    <?foreach($actions as $action):?>
                    <?if($cnt==1):?>
                        <td><?=$action?></td>
                        <td><label class="checkbox inline">
                                        <?$formHelper->checkbox(array('name' => 'access['.$controller.']['.$action.'][read]'));?> Просмотр
                                    </label>
                                    <label class="checkbox inline">
                                        <?$formHelper->checkbox(array('name' => 'access['.$controller.']['.$action.'][write]'));?> Редактирование
                                    </label>
                        </td>
                        </tr>
                     <?else:?>
                        <tr>
                        <td>
                                <?=$action?>
                          </td>
                          <td>
                          <label class="checkbox inline">
                                        <?$formHelper->checkbox();?> Просмотр
                                    </label>
                                    <label class="checkbox inline">
                                        <?$formHelper->checkbox();?> Редактирование
                                    </label>
                          </td>
                        </tr>
                     <?endif?>
                    <?endforeach?>
             <?endforeach?>
                    <tr>
                        <td colspan="3" style="text-align: center">
                            <p></p>
                            <?$formHelper->submit(array('class' => 'btn btn-success', 'value' => 'Сохранить'))?>
                            <?$formHelper->reset(array('class' => 'btn', 'value'=>'Отмена'))?>
                        </td>
                    </tr>
            </table>
            <?$formHelper->closeForm()?>
</section>