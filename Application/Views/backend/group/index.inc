<div class="container">
    <div class="page-header">
        <h1>Группы <small>список всех групп</small></h1>
    </div>
    <div class="btn-toolbar">
        <div class="btn-group">
          <a class="btn btn-primary btn-small" href="<?=SITE_URL.'/'.ADMIN_PATH?>/group/add">Новая группа</a>
        </div>
    </div>
		<table class="table table-bordered" style="margin-top: 10px;">
		<tr bgcolor="#f3f3f3">
			<th>ID</th>
			<th width="70%">Название группы</th>
			<th>Действия</th>
		</tr>
		<?if(!empty($roleObjects)):?>
		<?foreach($roleObjects as $key => $role):?>
		<tr>
			<td><?=$role->id?></td>
			<td><a href="<?=SITE_URL.'/'.ADMIN_PATH.'/group/edit/id/'.$role->id?>"><?=$role->name?></a></td>
			<td class="options-width"><a class="btn btn-primary " href="<?=SITE_URL.'/'.ADMIN_PATH.'/group/edit/id/'.$role->id?>">Редактировать</a>
			  <a class="btn btn-danger" href="#<?=SITE_URL.'/'.ADMIN_PATH.'/group/edit/id/'.$role->id?>">Удалить</a>
			</td>
		</tr>
		<?endforeach?>
        <?else:?>
                <tr>
                    <td colspan="3">Нет доступных групп</td>
                </tr>
		<?endif?>
		</table>
</div>