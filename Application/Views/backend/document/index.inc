<div class="container">
    <div class="page-header">
        <h1>Документы</h1>
    </div>
    <div class="btn-toolbar">
        <div class="btn-group">
            <a class="btn btn-primary btn-small" href="/admin/document/add">Добавить документ</a>
        </div>
    </div>
    <table class="table">
        <thead>
        <tr>
            <th>#</th>
            <th>Наименование</th>
            <th>Тип</th>
            <th>Размер</th>
            <th>Категория</th>
            <th></th>
        </tr>
        </thead>
        <tbody>
        <?$cnt = 0?>
        <?foreach($documents as $document):?>
        <tr>
            <td><?=++$cnt?></td>
            <td><?=$document->name?></td>
            <td><?=preg_replace('/^application\/(.+?)$/', '$1', $document->type)?></td>
            <td><?=(round($document->size/(1024^2), 2))?> kb</td>
            <td>административные</td>
            <td><a href="">редактировать</a></td>
            <td><a href="/admin/document/destruction?id=<?=$document->id?>">удалить</a></td>
        </tr>
        <?endforeach?>
        </tbody>
    </table>
</div>