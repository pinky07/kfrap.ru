<div class="span8">
    <form class="form-horizontal" method="POST" action="/admin/document/add" enctype="multipart/form-data">
        <fieldset>
            <div class="control-group">
                <label class="control-label" for="fileInput">Выберите файл</label>
                <div class="controls">
                    <input class="input-file" name="file" id="fileInput" type="file">
                </div>
            </div>
            <div class="control-group">
                <label class="control-label" for="input01">Название документа</label>
                <div class="controls">
                    <input type="text" name="document_form[name]" class="input-xlarge" id="input01">
                    <p class="help-block">Введите полное название документа</p>
                </div>
            </div>

            <div class="control-group">
                <label class="control-label" for="select01">Выберите категорию</label>
                <div class="controls">
                    <select id="select01">
                        <option>something</option>
                        <option>2</option>
                        <option>3</option>
                        <option>4</option>
                        <option>5</option>
                    </select>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label" for="textarea">Описание</label>
                <div class="controls">
                    <textarea name='document_form[description]' class="input-xlarge" id="textarea" rows="3"></textarea>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label" for="optionsCheckbox">Активность</label>
                <div class="controls">
                    <label class="checkbox">
                        <input type="checkbox" name='document_form[status]' id="optionsCheckbox" value="option1">
                    </label>
                </div>
            </div>
            <div class="form-actions">
                <button type="submit" class="btn btn-primary">Сохранить</button>
            </div>
        </fieldset>
    </form>
    </div>