<div class="container">
    <div class="page-header">
        <h1>Пользователи <small>редактирование пользователя "<?=$user->login?>"</small></h1>
    </div>
    <div class="row">
    <div class="span8" style="padding-top: 30px;">
        <?$formHelper->openForm('/admin/user/add', 'POST', array('class'=> 'form-horizontal', 'enctype' => 'multipart/form-data'))?>
        <fieldset>

            <div class="control-group">
                <?$formHelper->label(array('for' => 'status', 'class' => 'control-label'), 'Логин:')?>
                <div class="controls">
                    <?$formHelper->text(array('name'=>'user_form[login]'))?>
                </div>
            </div>
            <div class="control-group">
                <?$formHelper->label(array('for' => 'status', 'class' => 'control-label'), 'Email:')?>
                <div class="controls">
                    <?$formHelper->text(array('name'=>'user_form[email]'))?>
                </div>
            </div>
            <div class="control-group">
                <?$formHelper->label(array('for' => 'status', 'class' => 'control-label'), 'Пароль:')?>
                <div class="controls">
                    <?$formHelper->text(array('name'=>'user_form[password]'))?>
                </div>
            </div>
            <div class="control-group">
                <?$formHelper->label(array('for' => 'role', 'class' => 'control-label'), 'Группа:')?>
                <div class="controls">
                    <?$formHelper->select(array('name'=>'user_form[role_id]', 'id'=>'division'), $roles)?>
                </div>
            </div>
            <div class="control-group">
                <?$formHelper->label(array('for' => 'status', 'class' => 'control-label'), 'Статус:')?>
                <div class="controls">
                    <label class="checkbox">
                    <?$formHelper->checkbox(array('name'=>'user_form[status]' , 'checked' => 'checked'))?>
                    Отключить пользователя
                    </label>
                </div>
            </div>
            <div class="form-actions" style="margin-top: 90px;">
                <?$formHelper->submit(array('class' => 'btn btn-primary', 'value' => 'Сохранить'))?>
                <button class="btn" onClick="window.location = '/admin/user'; return false;">Отмена</button>
            </div>
        </fieldset>
        <?$formHelper->closeForm()?>
    </div>
    </div>
</div>