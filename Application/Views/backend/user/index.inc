<div class="container">
    <div class="page-header">
        <h1>Пользователи <small>список всех пользователей</small></h1>
    </div>
    <div class="btn-toolbar">
        <div class="btn-group">
          <a class="btn btn-primary btn-small" href="/admin/user/add">Добавить пользователя</a>
        </div>
    </div>
    <?if(!empty($elements)):?>
	<table class="table table-bordered" style="margin-top: 10px;">
		<tr bgcolor="#f3f3f3">
			<th>ID</th>
			<th>Логин</th>
			<th>Email</th>
			<th>Группа</th>
			<th>Последний вход</th>
			<th>IP</th>
			<th>Статус</th>
			<th>Действия</th>
		</tr>
		<?foreach($elements as $user):?>
		<tr>
			<td><?=$user->id?></td>
			<td><?=$user->login?></td>
			<td><?=$user->email?></td>
			<td><?=$user->role->name?></td>
            <td><?=date('d.m.Y H:i:s',$user->last_visit)?></td>
            <td><?=$user->last_visit_ip?></td>
			<td></td>
			<td><a href="<?=SITE_URL?>/<?=ADMIN_PATH?>/user/edit/id/<?=$user->id?>">Редактировать</a><br><a href="">Удалить</a></td>
		</tr>
		<?endforeach;?>
		</table>
        <?=$pages?>
	<?endif?>
</div>