<div class="container">
    <div class="page-header">
        <h1>Пользователи <small>редактирование пользователя "<?=$user->login?>"</small></h1>
    </div>
    <div class="row">
    <div class="span8" style="padding-top: 30px;">
        <?$formHelper->openForm('/admin/user/edit/id/'.$user->id, 'POST', array('class'=> 'form-horizontal', 'enctype' => 'multipart/form-data'))?>
        <fieldset>

            <div class="control-group">
                <?$formHelper->label(array('for' => 'status', 'class' => 'control-label'), 'Логин:')?>
                <div class="controls">
                    <?$formHelper->text(array('name'=>'user_form[login]', 'value' => $user->login))?>
                </div>
            </div>
            <div class="control-group">
                <?$formHelper->label(array('for' => 'status', 'class' => 'control-label'), 'Email:')?>
                <div class="controls">
                    <?$formHelper->text(array('name'=>'user_form[email]', 'value' => $user->email))?>
                </div>
            </div>
            <div class="control-group">
                <?$formHelper->label(array('for' => 'status', 'class' => 'control-label'), 'Пароль:')?>
                <div class="controls">
                    <?$formHelper->text(array('name'=>'user_form[password]', 'value' => $user->password))?>
                </div>
            </div>
            <div class="control-group">
                <?$formHelper->label(array('for' => 'role', 'class' => 'control-label'), 'Группа:')?>
                <div class="controls">
                    <?$formHelper->select(array('name'=>'user_form[role_id]', 'id'=>'division'), $roles, $user->role_id)?>
                </div>
            </div>
            <div class="control-group">
                <?$formHelper->label(array('for' => 'status', 'class' => 'control-label'), 'Статус:')?>
                <div class="controls">
                    <label class="checkbox">
                    <?$formHelper->checkbox(array('name'=>'user_form[status]'), $user->status)?>
                    Отключить пользователя
                    </label>
                </div>
            </div>
            <div class="control-group">
                <?$formHelper->label(array('for' => 'status', 'class' => 'control-label'), 'Дата регистрации:')?>
                <div class="controls">
                    <?$formHelper->text(array('disabled' => 'disabled', 'value' => date('Y-m-d H:i:s', $user->reg_date)))?>
                </div>
            </div>
            <div class="control-group">
                <?$formHelper->label(array('for' => 'status', 'class' => 'control-label'), 'Последний визит:')?>
                <div class="controls">
                    <?$formHelper->text(array('disabled' => 'disabled', 'value' => date('Y-m-d H:i:s', $user->last_visit)))?>
                </div>
            </div>
            <div class="control-group">
                <?$formHelper->label(array('for' => 'status', 'class' => 'control-label'), 'Последний ip:')?>
                <div class="controls">
                    <?$formHelper->text(array('disabled' => 'disabled', 'value' => $user->last_visit_ip))?>
                </div>
            </div>
            <div class="form-actions" style="margin-top: 90px;">
                <?$formHelper->submit(array('class' => 'btn btn-primary', 'value' => 'Сохранить'))?>
                <button class="btn" onClick="window.location = '/admin/user'; return false;">Отмена</button>
            </div>
        </fieldset>
        <?$formHelper->closeForm()?>
    </div>
    <?if(!empty($user->person_id)):?>
        <div class="span4">
            <h3 style="float: left;">Персональные данные</h3>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<button class="btn btn-mini btn-primary" onClick="window.location = '/admin/person/edit/id/<?=$user->person_id?>'; return false;">ред.</button>
                <p>
                    <?if(!empty($user->person->photo_link)):?>
                    <img style="width:210px; padding-bottom: 10px" alt="" src="/upload/people/<?=$user->person->photo_link?>"/>
                    <br/>
                    <?endif?>
                </p>
            <dl>
                <dt>ФИО</dt>
                <dd><?=$user->person->last_name?> <?=$user->person->first_name?> <?=$user->person->middle_name?></dd>
                <?if(!empty($user->person->division_id)):?>
                <dt>Подразделение</dt>
                <dd><?=$user->person->division->name?></dd>
                <?endif?>
                <dt>Должность</dt>
                <dd><?=$user->person->appointment?></dd>
            </dl>
            <hr>
            <h3>Общая информация</h3>
                <p>
                    <?=$user->person->info?>
                </p>
            <hr>
            <h3>Контактные данные</h3>
            <dl>
                <dt>Телефон</dt>
                <dd><?=$user->person->phone?></dd>
                <dt>Email</dt>
                <dd><?=$user->person->email?></dd>
            </dl>
        </div>
        <?endif?>
    </div>
</div>