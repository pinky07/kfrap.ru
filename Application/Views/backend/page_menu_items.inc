<? include('elements/head.php') ?>
<table border="0" width="860px" align="center">
<tr>
	<td height="80px">
	</td>
</tr>
<tr>
	<td>
		<?include_once('elements/admin_menu.php')?>
	</td>
</tr>
<tr>
	<td height="400px" valign="top" align="center">
		<a href="<?=SITE_URL?>/<?=ADMIN_PATH?>/page/viewPageMenu" style="float:left; padding:2px 4px 6px 0px;">Назад</a>
		<a href="<?=SITE_URL?>/<?=ADMIN_PATH?>/page/addMenuItem/menuId/<?=$menu['menu_id']?>" style="float:right; padding:2px 0px 6px 4px;">Создать новый пункт меню</a>
		<table cellpadding="6px" width="100%">
			<tr><td colspan="5" align="center">
				<?if(isset($menu['menu_name']) and !empty($menu['menu_name'])):?>
					<?=$menu['menu_name']?>
				<?endif?>
			</td></tr>
			<tr bgcolor="#e0e0e0">
				<td>ID</td>
				<td>Название</td>
				<td>Ссылка</td>
				<td>Порядок</td>
				<td width="25%">Действия
			</td>
			</tr>
			<?if(isset($menuItems) and !empty($menuItems)):?>
			<?foreach($menuItems as $items):?>
			<tr bgcolor="#f5f5f2">
				<td><?=$items['id']?></td>
				<td><a href="<?=SITE_URL?>/<?=ADMIN_PATH?>/page/editMenuItem/menuId/<?=$menu['menu_id']?>/itemId/<?=$items['id']?>"><?=$items['name']?></a></td>
				<td><?=$items['url']?></td>
				<td><?=$items['sort']?></td>
				<td>
					<a href="<?=SITE_URL?>/<?=ADMIN_PATH?>/page/editMenuItem/menuId/<?=$menu['menu_id']?>/itemId/<?=$items['id']?>" title="Редактировать">Редактировать</a> | 
					<a href="" title="">Удалить</a>
				</td>
			</tr>
			<?endforeach?>
			<?endif?>
		</table>
	</td>
</tr>
<tr>
	<td height="40px">
	</td>
</tr>