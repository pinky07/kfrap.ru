<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml: lang="ru" lang="ru">
<head>
    <link rel="StyleSheet" type="text/css" href="<?=SITE_URL?>/css/layouts/backend/style.css"/>
</head>
<body>
<div class="container">
    <div class="row" style="padding-top: 100px">
        <div class="span6 offset3">
            <?foreach($this->notices as $notice):?>
                <?=$notice->show()?>
            <?endforeach?>
        <?$formHelper->openForm(false, 'POST', array('class'=> 'form-horizontal'))?>
            <fieldset>
                <legend>Авторизация в системе</legend>
                <div class="control-group">
                        <?$formHelper->label(array('for' => 'login', 'class' => 'control-label'), 'Имя пользователя')?>
                    <div class="controls">
                        <?$formHelper->text(array('name'=>'login_form[login]', 'id'=>'login', 'class' => 'span3'))?>
                    </div>
                 </div>
                <div class="control-group">
                    <?$formHelper->label(array('for' => 'password', 'class' => 'control-label'), 'Пароль')?>
                    <div class="controls">
                    <?$formHelper->password(array('name'=>'login_form[password]', 'id'=>'password', 'class' => 'span3'))?>
                   </div>
                </div>
                <div class="form-actions">
                    <?$formHelper->submit(array('value' => 'Войти', 'class' => 'btn btn-primary'))?>
                </div>
            </fieldset>
        <?$formHelper->closeForm()?>
        </div>
    </div>
</div>
</body>
</html>