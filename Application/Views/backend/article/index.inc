<div class="container">
    <div class="page-header">
        <h1>Материалы <small>список разделов</small></h1>
    </div>
	<div class="btn-toolbar">
        <div class="btn-group">
          <a class="btn btn-primary btn-small" href="/admin/category/add">Создать раздел</a>
        </div>
        <div class="btn-group">
          <a class="btn btn-primary btn-small" href="/admin/article/add">Добавить материал</a>
        </div>
    </div>
	<table class="table">
        <thead>
          <tr>
            <th>#</th>
            <th width="60%">Название раздела</th>
            <th>Количество материалов</th>
            <th></th>
          </tr>
        </thead>
        <?foreach($categories as $category):?>
        <tr>
            <td style="vertical-align: middle"><?=$category->id?></td>
            <td style="vertical-align: middle"><a href="<?=SITE_URL.'/'.ADMIN_PATH?>/category/view/id/<?=$category->id?>"><?=$category->title?></a></td>
            <td style="vertical-align: middle"><?=count($category->articles)?></td>
            <td valign="center">
                <div class="btn-group">
                    <a class="btn dropdown-toggle" data-toggle="dropdown" href="#">Действия<span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="<?=SITE_URL.'/'.ADMIN_PATH?>/category/view/id/<?=$category->id?>">Открыть</a></li>
                        <li><a href="<?=SITE_URL.'/'.ADMIN_PATH?>/category/edit/id/<?=$category->id?>">Редактировать</a></li>
                        <li><a href="#">Деактивировать</a></li>
                        <li class="divider"></li>
                        <li><a href="#">Удалить</a></li>
                    </ul>
                </div>
            </td>
        </tr>
        <?endforeach?>
	</table>
</div>