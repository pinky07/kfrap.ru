<div class="container">
    <div class="page-header">
        <h1>Материалы <small>редактирование материала "<?=$article->title?>"</small></h1>
    </div>
    <?$formHelper->openForm('/admin/article/edit/id/'.$article->id, 'POST', array('class'=> 'form-vertical', 'enctype' => 'multipart/form-data'))?>
        <fieldset>
    <table width="100%">
        <tr>
            <td valign="top">
                <div class="control-group">
                    <?$formHelper->label(array('for' => 'title', 'class' => 'control-label'), 'Заголовок материала:')?>
                    <div class="controls">
                        <?$formHelper->text(array('name'=>'article_form[title]', 'id'=>'title', 'value' => $article->title))?>
                    </div>
                </div>
                <div class="control-group">
                    <?$formHelper->label(array('for' => 'category', 'class' => 'control-label'), 'Раздел:')?>
                    <div class="controls">
                        <?$formHelper->select(array('name' => 'article_form[category_id]'), $categories, $article->category_id)?>
                    </div>
                </div>
                <div class="control-group">
                    <?$formHelper->label(array('for' => 'sort', 'class' => 'control-label'), 'Порядок:')?>
                    <div class="controls">
                        <?$formHelper->text(array('name' => 'article_form[sort]', 'value' => $article->sort))?>
                    </div>
                </div>
                <div class="control-group">
                    <?$formHelper->label(array('for' => 'actual_date', 'class' => 'control-label'), 'Дата публикации:')?>
                    <div class="controls">
                        <?$formHelper->text(array('name'=>'article_form[actual_date]', 'id' => 'actual_date', 'value' => (!empty($article->actual_date)?$article->actual_date->format('Y-m-d'):'')))?>
                    </div>
                </div>
                <div class="control-group">
                    <?$formHelper->label(array('for' => 'slider', 'class' => 'control-label'), 'В слайдер:')?>
                    <div class="controls">
                        <label class="checkbox">
                        <?$formHelper->checkbox(array('name' =>' article_form[slider]', 'id' => 'slider', 'onclick' => 'showSliderPic()'), $article->slider)?>
                            Отображать в слайдах на главной
                        </label>
                    </div>
                </div>

                <div class="control-group" id="sl_pic" style="display:<?=($article->slider == 0)?'none':'block'?>">
                    <?$formHelper->label(array('for' => 'slider_picture', 'class' => 'control-label'), 'Изображение:')?>
                    <div class="controls">
                        <label class="checkbox">
                            <img style="width:210px; padding-bottom: 10px" alt="" src="/upload/images<?=(!empty($article->slider_picture)?'/'.$article->slider_picture:'')?>"/>
                            <?$formHelper->file(array('name'=>'slider_picture', 'id' => 'slider_picture'), $article->slider)?>
                            Изображение которое будет отображаться на слайдах
                        </label>
                    </div>
                </div>

                <div class="control-group">
                    <?$formHelper->label(array('for' => 'status', 'class' => 'control-label'), 'Активность:')?>
                    <div class="controls">
                        <label class="checkbox">
                        <?$formHelper->checkbox(array('name'=>'article_form[status]'), $article->status)?>
                            Отображение для пользователей
                        </label>
                    </div>
                </div>
                <div class="control-group">
                    <?$formHelper->label(array('for' => 'preview', 'class' => 'control-label'), 'Краткий анонс:')?>
                    <div class="controls">
                    <?$formHelper->textarea(array('name'=>'article_form[preview]', 'rows' => 4), $article->preview)?>
                    </div>
                </div>
                <div class="control-group">
                    <?$formHelper->label(array('for' => 'tags', 'class' => 'control-label'), 'Тэги:')?>
                    <div class="controls">
                        <?$formHelper->text(array('name' => 'article_form[tags]', 'value' => $article->tags))?>
                    </div>
                </div>
                <?if($article->twitter_published == 0):?>
                <div class="control-group">
                    <?$formHelper->label(array('for' => 'slider', 'class' => 'control-label'), 'В твиттер:')?>
                    <div class="controls">
                        <label class="checkbox">
                        <?$formHelper->checkbox(array('name' =>' article_form[twitter_published]'))?>
                            Опубликовать анонс в твиттер
                        </label>
                    </div>
                </div>
                <div class="control-group">
                    <?$formHelper->label(array('for' => 'slider', 'class' => 'control-label'), 'Документы:')?>
                    <div class="controls">
                        <div id="add-doc" class="btn btn-small">Добавить документы</div>
                        <div id="dialog" title="Добавить документы">
                            <select class="multiselect" multiple="multiple" name="article_form[documents][]" style="height: 250px;">
                                <?foreach($documents as $documentId => $document):?>
                                <option value="<?=$documentId?>" <?=(is_array(unserialize($article->documents)) && in_array($documentId,unserialize($article->documents)))?'selected':''?>><?=$document?></option>
                                <?endforeach?>
                            </select>
                        </div>
                    </div>
                </div>
                <?endif?>
            </td>
            <td width="74%" valign="top">
                <div class="tabbable" align="right" style="margin-bottom: 9px;">
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#tab1" data-toggle="tab">Редактирование</a></li>
                        <li class=""><a href="#tab2" data-toggle="tab" onclick="view(); return false;">Просмотр</a></li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="tab1">
                            <div><?$formHelper->CKEditor(array('name' => 'article_form[content]', 'id' => 'content'), $article->content)?></div>
                        </div>
                        <div class="tab-pane" id="tab2" align="left">
                            <div>
                            </div>
                        </div>
                    </div>
                </div>

            </td>
        </tr>
    </table>
        <div class="form-actions">
            <?$formHelper->submit(array('class' => 'btn btn-primary', 'value' => 'сохранить'))?>
            <button class="btn" onClick="window.location = '/admin/category/view/id/<?=$article->category_id?>'; return false;">Отмена</button>
          </div>
        </fieldset>
    <?$formHelper->closeForm()?>
</div>

<script type="text/javascript">
    function view() {
        var content = document.getElementsByTagName("iframe")[0].contentDocument.getElementsByTagName("body")[0].innerHTML;
        $('#tab2 div').html(content);
    }

    function showSliderPic() {
        if($('#slider').attr('checked') == 'checked') {

            $('#sl_pic').show();
        } else {

            $('#sl_pic').hide();
        }
    }

    $(function()
    {
        $.datepicker.setDefaults(
            $.extend($.datepicker.regional["ru"])
        );
        $("#actual_date").datepicker({dateFormat: "yy-mm-dd"});
    });
</script>
<script type="text/javascript" src="<?=SITE_URL?>/js/ui.multiselect.js"></script>
<script type="text/javascript">
    $(function(){
        $(".multiselect").multiselect({searchable: false});

        $("#dialog").dialog({
            autoOpen: false,
            height:400,
            width:700
        });
        $("#add-doc").click(function(){
            $("#dialog").dialog("open");
        });


    });
</script>