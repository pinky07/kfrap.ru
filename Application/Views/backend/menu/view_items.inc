<div id="l<?=$menu_id?>">
<div class="span8" style="margin:0">
<ul class="nav nav-pills">
    <li><a href="/admin/menu/edit/id/<?=$menu_id?>" rel="#l<?=$menu_id?> div div" onClick='ajax_button(this); return false;'>Редактировать</a></li>
    <li><a href="/admin/menu/add_item" rel="#l<?=$menu_id?> div div" params="id=<?=$menu_id?>" onClick='ajax_button(this); return false;'>Добавить пункт</a></li>
    <li><a href="#" id='del' menu_id='<?=$menu_id?>'>Удалить</a></li>
</ul>
    <div>
        <table class="table" style="width:500px">
            <?foreach($items as $item):?>
                <?if($item->parent_item_id == 0):?>
                <tr>
                    <td><?=$item->name?></td>
                    <td width="90px">
                        <div class="btn-group">
                        <a class="btn dropdown-toggle" data-toggle="dropdown" href="#">Действия<span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="/admin/menu/edit_item/id/<?=$item->id?>" rel="#l<?=$menu_id?> div div" params="menu_id=<?=$menu_id?>" onClick='ajax_button(this); return false;'>Редактировать</a></li>
                            <li><a href="#">Деактивировать</a></li>
                            <li class="divider"></li>
                            <li><a href="#">Удалить</a></li>
                        </ul>
                        </div>
                    </td>
                </tr>
                    <?foreach($item->children_items as $parent_item):?>
                    <tr>
                        <td> -- <?=$parent_item->name?></td>
                        <td>
                            <div class="btn-group">
                                <a class="btn dropdown-toggle" data-toggle="dropdown" href="#">Действия<span class="caret"></span></a>
                                <ul class="dropdown-menu">
                                    <li><a href="/admin/menu/edit_item/id/<?=$parent_item->id?>" rel="#l<?=$menu_id?> div div" params="menu_id=<?=$menu_id?>" onClick='ajax_button(this); return false;'>Редактировать</a></li>
                                    <li><a href="#">Деактивировать</a></li>
                                    <li class="divider"></li>
                                    <li><a href="#">Удалить</a></li>
                                </ul>
                            </div>
                        </td>
                    </tr>
                    <?endforeach?>
                <?endif?>
            <?endforeach?>
        </table>
    </div>
</div>
</div>