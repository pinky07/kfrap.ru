<?$formHelper->openForm('/admin/menu/edit/id/'.$menu->id, 'POST', array('class'=> 'form-horizontal'))?>
    <fieldset>
        <div class="control-group">
                <?$formHelper->label(array('for' => 'name', 'class' => 'control-label'), 'Название меню')?>
            <div class="controls">
                <?$formHelper->text(array('name'=>'name', 'id'=>'name', 'value' => $menu->name, 'class' => 'input-xlarge'))?>
            </div>
         </div>
        <div class="control-group">
            <?$formHelper->label(array('for' => 'active', 'class' => 'control-label'), 'Активен')?>
            <div class="controls">
            <?$formHelper->checkbox(array('name'=>'active', 'id'=>'active', 'class' => 'input-xlarge'), $menu->active)?>
           </div>
        </div>
        <div>
            <?$formHelper->submit(array('value' => 'Сохранить', 'class' => 'btn'))?>
        </div>
    </fieldset>
<?$formHelper->closeForm()?>