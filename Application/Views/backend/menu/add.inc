<?$formHelper->openForm('/admin/menu/add', 'POST', array('class'=> 'form-horizontal'))?>
    <fieldset>
        <div class="control-group">
                <?$formHelper->label(array('for' => 'name', 'class' => 'control-label'), 'Название меню')?>
            <div class="controls">
                <?$formHelper->text(array('name'=>'name', 'id'=>'name', 'class' => 'get_links.inc'))?>
            </div>
         </div>
        <div class="control-group">
            <?$formHelper->label(array('for' => 'name', 'class' => 'control-label'), 'Активен')?>
            <div class="controls">
            <?$formHelper->checkbox(array('name'=>'active', 'id'=>'active', 'class' => 'get_links.inc'))?>
           </div>
        </div>
        <div>
            <?$formHelper->submit(array('value' => 'Сохранить', 'class' => 'btn'))?>
        </div>
    </fieldset>
<?$formHelper->closeForm()?>