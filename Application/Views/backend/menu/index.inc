<div class="container">
    <div class="page-header">
        <h1>Меню <small>управление навигацией сайта</small></h1>
    </div>
    <div class="row">
        <div class="span4">
            <a class="btn btn-primary btn-small ajax" href="<?=SITE_URL.'/'.ADMIN_PATH?>/menu/add" rel=".tab-content">Новое меню</a>
        </div>
        <div class="span4">
            <h3>Меню</h3>
        </div>
    </div>

    <div class="row" style="padding-top: 10px">
        <div class="span10">
            <div class="tabbable tabs-left">
                <ul class="nav nav-tabs">
                    <?foreach($menus as $menu):?>
                        <li><a href="#" data-toggle="tab" params="id=<?=$menu->id?>"><?= $menu->name?></a></li>
                    <?endforeach?>
                </ul>
                <div class="tab-content">
                </div>
            </div> <!-- /tabbable -->

        </div>
    </div>
</div>

<script type="text/javascript">
    $(".nav-pills li a").click(function(){
        if($(".nav-pills li.active").length > 0)
            $(".nav-pills li.active").removeClass('active');

        $(this).parent('li').attr('class', 'active');
    });

    $(".nav-tabs li").click(function(){
        ajax_load('.tab-content', '/admin/menu/view_items', $(this).children('a').attr('params'));
    });
    
    function ajax_button(element) {
        ajax_load($(element).attr('rel'), $(element).attr('href'), $(element).attr('params'));
    }
</script>