<?$formHelper->openForm('/admin/menu/edit_item/id/'.$item->id.'?menu_id='.$menu_id, 'POST', array('class'=> 'form-horizontal'))?>
    <fieldset>
        <div class="control-group">
                <?$formHelper->label(array('for' => 'name', 'class' => 'control-label'), 'Название пункта')?>
            <div class="controls">
                <?$formHelper->text(array('name'=>'name', 'id'=>'name', 'value'=>$item->name, 'class' => 'input-xlarge'))?>
            </div>
        </div>
        <div class="control-group">
            <?$formHelper->label(array('for' => 'parent', 'class' => 'control-label'), 'Вложенность')?>
            <div class="controls">
            <?$formHelper->select(array('name'=>'parent', 'id'=>'parent', 'class' => 'input-xlarge'), $items, $item->parent_item_id)?>
           </div>
        </div>
        <div class="control-group">
            <?$formHelper->label(array('for' => 'link-type', 'class' => 'control-label'), 'Ссылка')?>
            <div class="controls">
            <?$formHelper->select(array('name'=>'link-type', 'id'=>'link-type', 'class' => 'input-xlarge'), array(0 => 'Внешняя', 1 => 'Страница', 2 => 'Материал'))?>
           </div>
        </div>
        <div class="control-group">
                <?$formHelper->label(array('for' => 'link', 'class' => 'control-label'), '')?>
            <div class="controls link">
                <?$formHelper->text(array('name'=>'link', 'id'=>'link', 'value' => $item->url, 'class' => 'input-xlarge'))?>
            </div>
        </div>
        <div class="control-group">
            <?$formHelper->label(array('for' => 'sort', 'class' => 'control-label'), 'Порядок)')?>
            <div class="controls">
            <?$formHelper->text(array('name'=>'sort', 'id'=>'sort', 'class' => 'input-xlarge', 'value' => $item->sort))?>
           </div>
        </div>
        <div class="control-group">
            <?$formHelper->label(array('for' => 'active', 'class' => 'control-label'), 'Активен')?>
            <div class="controls">
            <?$formHelper->checkbox(array('name'=>'active', 'id'=>'active', 'class' => 'input-xlarge'), $item->active)?>
           </div>
        </div>
        <div>
            <?$formHelper->submit(array('value' => 'Сохранить', 'class' => 'btn'))?>
        </div>
    </fieldset>
<?$formHelper->closeForm()?>
<script type="text/javascript">
    $("#link-type").change(function(){
        ajax_load ('.control-group div.link', '/admin/menu/get_links', 'link_type=' + $('#link-type :selected').val(), 'POST')
    });
</script>