<style type="text/css">
    .first
    {
        cursor: pointer;
    }
    .on
    {
        background: #f5f5f5;
    }

    .table tbody tr.more-question:hover td:first-child
    {
        background: transparent !important;
    }
    
    .check
    {
        float: right;
    }
</style>
<div class="container">
    <div class="page-header">
        <h1>Вопросы</h1>
    </div>
    <table class="table table-bordered">
        <thead>
        <tr>
            <th>#</th>
            <th>ФИО</th>
            <th>Подразделение</th>
            <th>Тема</th>
            <th>E-mail</th>
            <th>Статус</th>
        </tr>
        </thead>
        <tbody>
        <?foreach($elements as $question):?>
        <tr class="first" id="question<?=$question->id?>_question">
            <td><?=$question->id?></td>
            <td><?=sprintf('%s %s', $question->last_name, $question->first_name)?></td>
            <td><?=isset($question->division->name)?$question->division->name:''?></td>
            <td><?=$question->subject?></td>
            <td><?=!empty($question->email)?$question->email:''?></td>
            <td class="status"><?=($question->is_answered)?'Закрыт':'Открыт'?></td>
        </tr>
        <tr class="more-question" id="question<?=$question->id?>_answer" style="display: none;" >
            <td colspan="4">
                <div class="control-group" >
                    <label class="control-label" style="padding-top: 0;"><b>Вопрос:</b></label>
                        <p><?=$question->text?></p>
                </div>
            </td>
            <td colspan="2" class="answer-block" bgcolor="f5f5f5">
                <?if(!$question->is_answered):?>
                <div class="control-group">
                    <label class="control-label">Ответчик:</label>
                    <div class="controls">
                        <input type="text" class="input-xlarge answerer" style="width: 97%;"/>
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label" for='textarea' style="padding-top: 0;">Ответ:</label>
                    <div class="controls">
                        <textarea class="input-xlarge answer_text" id='textarea' rows="8" style="width: 97%;"></textarea>
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label" style="float:left;margin-right: 10px;">Отображать:</label>
                    <div class="controls" style="float: left;">
                        <input  type="checkbox"/>
                    </div>
                </div>
                <button style="float: right;" class="btn btn-primary" onclick="answeredQuestion(<?=$question->id?>)">Сохранить</button>
            <?else:?>
                <div class="control-group" >
                    <label class="control-label" style="padding-top: 0;"><b>Ответ:</b></label>
                        <p><?=$question->answer->answer_text?></p>
                </div>
            <?endif?>
            </td>
        </tr>
        <?endforeach;?>
        </tbody>
    </table>
</div>
<script type="text/javascript">
    function answeredQuestion(question_id)
    {
        var answerer    = $("#question"+question_id+"_answer").find('.answerer').val();
        var answer_text = $("#question"+question_id+"_answer").find('.answer_text').val();

        var request = {
            'question_answered_form' : {
                'answerer' : answerer,
                'answer_text': answer_text
            }
        };

        if(answerer == '')
        {
            alert('Не указано имя отвечающего'); return false;
        }
        if(answer_text == '')
        {
            alert('Поле ответ не может быть пустым'); return false;
        }

        $.ajax({
            url: '/backend/questions/answer_question?question_id='+question_id,
            type:'POST',
            dataType : 'json',
            data: request,
            success: function (data, textStatus)
            {
                if(data.status == 'error')
                {

                    alert(data.message);
                    return false;
                }

                var answerContent = '<label class="control-label" style="padding-top: 0;"><b>Ответ:</b></label><p>'
                                    + data.answer_text
                                    +'</p>';

                $("#question"+question_id+"_answer").find('.answer-block').html(answerContent);
                $("#question"+question_id+"_question").find(".status").html('Закрыт');
            }
        });

        return true;
    }
    $(document).ready(function(){
        $(".first").click(function()
        {
            var currQuestion    = $(this).next(".more-question");
            var otherQuestions  = currQuestion.siblings(".more-question:visible");

            (!otherQuestions.length) ? currQuestion.toggle("fast")
            : otherQuestions.toggle("fast", function() { currQuestion.toggle("fast"); });

            return false;
                                //.siblings(".more-question:visible").slideUp("fast");
                        //$(this).toggleClass("on");
                        //$(this).siblings(".first").removeClass("on");
        });
    });
</script>
