<div class="container">
    <div class="page-header">
        <h1>Материалы <small>редактирование раздела</small></h1>
    </div>
<div class="span8">
    <?$formHelper->openForm('/admin/category/edit/id/'.$category->id, 'POST', array('class'=> 'form-horizontal'))?>
    <fieldset>
        <div class="control-group">
            <?$formHelper->label(array('for' => 'title', 'class' => 'control-label'), 'Заголовоок раздела')?>
        <div class="controls">
            <?$formHelper->text(array('name'=>'title', 'id'=>'title', 'value' => $category->title, 'class' => 'input-xlarge'))?>
            <p class="help-block">In addition to freeform text, any HTML5 text-based input appears like so.</p>
        </div>
        </div>
        <div class="control-group">
            <?$formHelper->label(array('for' => 'at_home', 'class' => 'control-label'), 'На главной')?>
            <div class="controls">
                <label class="checkbox">
                <?$formHelper->checkbox(array('name'=>'at_home', 'id'=>'at_home'), $category->at_home)?>
                Отображать раздел материалов на главной странице
                </label>
            </div>
        </div>
        <div class="control-group">
            <?$formHelper->label(array('for' => 'active', 'class' => 'control-label'), 'Активен')?>
            <div class="controls">
                <label class="checkbox">
                <?$formHelper->checkbox(array('name'=>'active', 'id'=>'active'), $category->active)?>
                Отображение раздела
                </label>
            </div>
        </div>
        <div class="control-group">
            <?$formHelper->label(array('for' => 'description', 'class' => 'control-label'), 'Краткое описание')?>
            <div class="controls">
                <?$formHelper->textarea(array('name'=>'description', 'id'=>'description', 'class'=> 'input-xlarge', 'rows' => 3), $category->description)?>
            </div>
        </div>
        <div class="form-actions">
            <?$formHelper->submit(array('class' => 'btn btn-primary', 'value' => 'сохранить'))?>
            <button class="btn" onClick="window.location = '/admin/article'; return false;">Отмена</button>
        </div>
    </fieldset>
    <?$formHelper->closeForm()?>
</div>
</div>