<div class="container">
    <div class="page-header">
        <h1>Материалы <small>раздел "<?=$category->title?>"</small></h1>
    </div>
<?foreach($category->articles as $key => $article):?>
    <?if($key == 0 or ($key)%3 == 0):?>
    <div class="row">
        <div class="span12">
        <ul class="thumbnails">
    <?endif?>
        <li style="width:300px;">
            <div class="thumbnail">
                <div class="caption">
                    <div style="font-size:10px; text-align: right" class="muted"><?=$article->add_date->format('Y-m-d')?></div>
                <h5><?=$article->title?></h5>
                <p><?=$article->preview?></p>
                    <div class="btn-toolbar" style="margin-top: 18px;">
                        <div class="btn-group">
                            <a href="#" class="btn btn-small btn-primary dropdown-toggle" data-toggle="dropdown">Редактировать <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="/admin/article/edit/id/<?=$article->id?>">Изменить</a></li>
                            <?if($article -> status == 1):?>
                            <li><a href="<?=sprintf('/admin/article/deactivate?id=%d',$article->id)?>">Деактивировать</a></li>
                            <?else:?>
                            <li><a href="<?=sprintf('/admin/article/activate?id=%d',$article->id)?>">Активировать</a></li>
                            <?endif?>
                            <li><a href="#">На главную</a></li>
                            <li class="divider"></li>
                            <li><a href="#">Удалить</a></li>
                        </ul>
                        </div>
                        <div class="btn-group">
                            <a href="#" class="btn btn-small dropdown-toggle" data-toggle="dropdown">Перенести <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <?foreach($categories as $cat):?>
                            <li><a href="<?=sprintf('/admin/article/move?article_id=%d&category_id=%d',$article->id, $cat->id)?>"><?=$cat->title?></a></li>
                            <?endforeach?>
                        </ul>
                        </div>
                    </div>
                </div>
            </div>
        </li>
    <?if(($key)%3 == 2):?>
        </ul>
        </div>
    </div>
    <?endif?>
<?endforeach?>
</div>