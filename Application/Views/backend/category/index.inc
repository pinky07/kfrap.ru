<div class="subnav">
		<ul style="list-style: none; margin: 0;">
			<li>
				<a style="color: #fff; text-decoration: none;" href="<?=SITE_URL?>/<?=ADMIN_PATH?>/category/add"><button class="btn btn-small btn-primary"><b>+</b> Категорию</button></a>
			</li>
		</ul>
	</div>
    <table class="table table-bordered" style="margin-top: 3px;">
        <tr bgcolor="#f1f1f1">
            <th><h4>ID</h4></th>
            <th><h4>Название</h4></th>
            <th><h4>Тип</h4></th>
            <th><h4>Активность</h4></th>
            <th><h4>Материалов</h4></th>
            <th><h4>Доступ</h4></th>
            <th><h4>Действия</h4></th>
        </tr>
        <?if(!empty($categoryArray)):?>
        <?foreach($categoryArray as $key => $value):?>
        <tr>
            <td><?=$value['id']?></td>
            <td>
                <a href="<?=SITE_URL.'/'.ADMIN_PATH.'/category/edit/id/'.$value['id']?>"><?=$value['title']?></a>
            </td>
            <td>
                <span><?=($value['type']==1)?'Статьи':'Новости'?><span>
            </td>
            <td>
                <?=($value['active']==1)?'<font color="green">активен</font>':'<font color="red">не активен</font>'?>
            </td>
            <td>12</td>
            <td>Ограничен</td>
            <td class="options-width">
                <a href="<?=SITE_URL.'/'.ADMIN_PATH.'/category/edit/id/'.$value['id']?>" title="Редактировать">Редактировать</a><br>
                <a href="" title="">Удалить</a>
            </td>
        </tr>
        <?endforeach?>
        <?endif?>
    </table>