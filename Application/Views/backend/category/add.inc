<div class="container">
    <div class="page-header">
        <h1>Материалы <small>новый раздел</small></h1>
    </div>
<div class="span8">
    <?$formHelper->openForm('/admin/category/add', 'POST', array('class'=> 'form-horizontal'))?>
    <fieldset>
        <div class="control-group">
            <?$formHelper->label(array('for' => 'title', 'class' => 'control-label'), 'Заголовоок раздела')?>
        <div class="controls">
            <?$formHelper->text(array('name'=>'title', 'id'=>'title', 'class' => 'input-xlarge'))?>
            <p class="help-block">In addition to freeform text, any HTML5 text-based input appears like so.</p>
        </div>
        </div>
        <div class="control-group">
            <?$formHelper->label(array('for' => 'at_home', 'class' => 'control-label'), 'На главной')?>
            <div class="controls">
                <label class="checkbox">
                <?$formHelper->checkbox(array('name'=>'at_home', 'id'=>'at_home'))?>
                Отображать раздел материалов на главной странице
                </label>
            </div>
        </div>
        <div class="control-group">
            <?$formHelper->label(array('for' => 'active', 'class' => 'control-label'), 'Активен')?>
            <div class="controls">
                <label class="checkbox">
                <?$formHelper->checkbox(array('name'=>'active', 'id'=>'active'))?>
                Отображение раздела
                </label>
            </div>
        </div>
        <div class="control-group">
            <?$formHelper->label(array('for' => 'description', 'class' => 'control-label'), 'Краткое описание')?>
            <div class="controls">
                <?$formHelper->textarea(array('name'=>'description', 'id'=>'description', 'class'=> 'input-xlarge', 'rows' => 3))?>
            </div>
        </div>
        <div class="form-actions">
            <?$formHelper->submit(array('class' => 'btn btn-primary', 'value' => 'Создать'))?>
            <button class="btn" onClick="window.location = '/admin/article'; return false;">Отмена</button>
        </div>
    </fieldset>
    <?$formHelper->closeForm()?>
</div>
</div>