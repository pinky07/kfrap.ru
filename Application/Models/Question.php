<?php
/**
 * Created by JetBrains PhpStorm.
 * User: pinky
 * Date: 27.04.12
 * Time: 23:59
 * To change this template use File | Settings | File Templates.
 */
namespace Application\Models;
use \Framework\Model as Model;

class Question extends \ActiveRecord\Model
{
    static $belongs_to = array(
        array('division', 'class_name' => 'Application\Models\Division'),
        array('answer', 'class_name' => 'Application\Models\Answer')
    );

    static $validates_presence_of = array(
        array('first_name', 'message' => 'Поле "Имя" должно быть заполнено!'),
        array('last_name', 'message'  => 'Поле "Фамилия" должно быть заполнено!')
    );

    //static $validates_format_of = array(
        //array('email', 'with' => '/^[^0-9][A-z0-9_]+([.][A-z0-9_]+)*[@][A-z0-9_]+([.][A-z0-9_]+)*[.][A-z]{2,4}$/', 'message' => 'Неправильный формат email!')
    //);
}
