<?php
class Model_Fs extends Model {

	//���� ��������!!!
	function getBackendControllers() {
		$dir = ('controllers/backend');
		$d = opendir($dir);
		while($name = readdir($d)) {
			if($name == '.' or $name == '..')
				continue;
			if(is_file($dir.'/'.$name)) {
				$name = str_replace('.php', '', $name);
				$class = 'Controller_Backend_'.$name;
				if(class_exists($class)) {
					if(property_exists($class, 'name'))
						$controller_name = $class::$name;
					else
						$controller_name = '';
					$class_methods = get_class_methods($class);
					$dirs[] = array('controller'=>$name, 'name' => $controller_name, 'methods' => $class_methods);
				}
			}
		}
		return $dirs;
	}
	
	function getFrontendControllers() {
		$dir = ('controllers/frontend');
		$d = opendir($dir);
		while($name = readdir($d)) {
			if($name == '.' or $name == '..')
				continue;
			if(is_file($dir.'/'.$name)) {
				$name = str_replace('.php', '', $name);
				$class = 'Controller_Frontend_'.$name;
				if(class_exists($class)) {
					if(property_exists($class, 'name'))
						$controller_name = $class::$name;
					else
						$controller_name = '';
					$class_methods = get_class_methods($class);
					$dirs[] = array('controller'=>$name, 'name' => $controller_name, 'methods' => $class_methods);
				}
			}
		}
		return $dirs;
	}
	
}
?>