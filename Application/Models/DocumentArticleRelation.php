<?php
/**
 * Created by JetBrains PhpStorm.
 * User: pinky
 * Date: 31.05.12
 * Time: 23:32
 * To change this template use File | Settings | File Templates.
 */
namespace Application\Models;

class DocumentArticleRelation extends \ActiveRecord\Model
{
    static $belongs_to = array(
        array(
            'document',
            'class_name' => 'Application\Models\Document'),
        array(
            'article',
            'class_name' => 'Application\Models\Article'),
        );
}
