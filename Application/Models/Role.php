<?php
namespace Application\Models;

class Role extends \ActiveRecord\Model
{
    //права пользователей, менять id - грех))
    const ROOT              = 1;
    const ADMINISTRATOR     = 2;
    const MODERATOR         = 3;
    const EDITOR            = 4;
    const LECTURER          = 5;
    const EMPLOEE           = 6;
    const STUDENT           = 7;
    const GUEST             = 8;

	private $_access;

	function getRoles() {
		$sql = "SELECT id, group_name, orders FROM users_group;";
		$query = db::getInstance()->query($sql);
		while($result = mysql_fetch_array($query, MYSQL_ASSOC)) {
			$res[] = $result;
		}
		return $res;
	}

	function getRole($id) {
		$sql = "SELECT id, group_name FROM users_group WHERE id = '".(integer)$id."';";
		$query = db::getInstance()->query($sql);
		$result = mysql_fetch_array($query, MYSQL_ASSOC);
		return $result;
	}

	function getOrders($id) {
		$sql = "SELECT orders FROM users_group WHERE id = '".(integer)$id."';";
		$query = db::getInstance()->query($sql);
		$result = mysql_fetch_array($query, MYSQL_ASSOC);
		if(is_array($result) and !empty($result))
			return unserialize($result['orders']);
		else
			return false;
	}

	function editRole($id, $title, $access) {
		$sql = "UPDATE users_group SET group_name='".$title."', orders='".$access."' WHERE id = '".(integer)$id."';";
		db::getInstance()->query($sql);
		return $sql;
	}

    public static function getBackendControllers() {
		$dir = ('controllers/backend');
		$d = opendir($dir);
		while($name = readdir($d)) {
			if($name == '.' or $name == '..')
				continue;
			if(is_file($dir.'/'.$name)) {
				$name = str_replace('.php', '', $name);
				$class = 'Controller_Backend_'.$name;
				if(class_exists($class)) {
					if(property_exists($class, 'name'))
						$controller_name = $class::$name;
					else
						$controller_name = '';
					$class_methods = get_class_methods($class);
					$dirs[] = array('controller'=>$name, 'name' => $controller_name, 'methods' => $class_methods);
				}
			}
		}
		return $dirs;
	}
}
?>