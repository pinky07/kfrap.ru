<?php
namespace Application\Models;
use Framework\FrontController;
class Page extends \ActiveRecord\Model
{
    static $belongs_to = array(
        array(
            'responsible_user',
            'foreign_key' => 'responsible',
            'class_name' => 'Application\Models\User'),
        array(
            'parent_page',
            'foreign_key' => 'parent_page_id',
            'class_name' => 'Application\Models\Page')
    );

    public function getDocuments()
    {
        $documents = unserialize($this->documents);

        if(!is_array($documents))
        {
            return false;
        }

        return \Framework\Model::ActiveRecord('Document')->all(array('conditions' => array('id in (?)', $documents)));
    }

    /**
     * Возвращает наиболее подходящий урл
     * @return string
     */
    public function getUrl()
    {
        $url = sprintf('page/view/id/%d', $this->id);

        $route = FrontController :: getInstance()
                                 -> getRouter()
                                 -> findRoute($url);
        if($route)
        {
            return "/{$route}";
        }

        return "/{$url}";
    }

	function add($params = array()) {
	
		$valuesArr = array('id' => "'NULL'");
		
		if(isset($params['title'])) {	
			$valuesArr['title'] = "'".clean($params['title'], true)."'";
		}
		
		if(isset($params['content'])) {
			$valuesArr['content'] = "'".$params['content']."'";
		}
		
		if(isset($params['menu'])) {
			$valuesArr['menu_id'] = "'".(int)$params['menu']."'";
		}
		
		if(isset($params['status'])) {
			$valuesArr['status'] = "'".(int)$params['status']."'";
		}
		
		$collsArr = array_keys($valuesArr);
		
		$colls = implode(', ',$collsArr);
		
		$values = implode(', ',$valuesArr);
		
		$sql = "INSERT INTO page (".$colls.") values (".$values.");";

		$result = db::getInstance()->query($sql);
		
		return $result;
	}
	
	function getAll($filter = array(), $sort = array(), $select = array(), $limit = false) {
		
		$where = array();
		
		if(isset($filter['id']))
			$where[] = "id ='".(int)$filter['id']."'";
		
		if(isset($filter['status']))	
			$where[] = "status ='".(int)$filter['status']."'";
	
		
		$order = array();
		
		$sql 	= "SELECT * FROM page";
		
		if(count($where)>0) {
			$sqlWhere = implode(' and ',$where);
			$sql .= ' WHERE '.$sqlWhere;
		}
		
		if(count($order)>0) {
			$sqlOrder = implode(' ,',$order);
			$sql .= ' ORDER BY '.$sqlOrder;
		} else {
			$sql .= " ORDER BY id ASC;";
		}

		$query	= db::getInstance()->query($sql);
		
		while($result = mysql_fetch_array($query, MYSQL_ASSOC)) {
			$res[] = $result;
		} 
		if(isset($res))
			return $res;
		else
			false;
	}
	
	function edit($id, $params = array()) {
		
		if(isset($id)) {
		
			$id				= (int)$id;
			$setArray		= array();
			
			if(isset($params['title']) and !empty($params['title'])) {
				$title			= clean($params['title'], true);
				$setArray[]		= "title = '".$title."'";
			}
			
			if(isset($params['menu'])) {
				$menu	= (int)$params['menu'];				
				$setArray[]		= "menu_id = '".$menu."'";
			}
			
			if(isset($params['content'])) {
				$content	=  $params['content'];
				$setArray[]		= "content = '".$content."'";
			}
			
			if(isset($params['status'])) {
				$status	= (int)$params['status'];
				$setArray[]		= "status = '".$status."'";
			}
			
			if(count($setArray)>0) {
				
				$setStr = implode(', ',$setArray);
				
				$sql = "UPDATE page SET ".$setStr;
				
				$sql .= " WHERE id = ".(int)$id.";";
				
				$result = db::getInstance()->query($sql);
				
				return $result;
			
			} else {
				return false;
			}
		} else {
			return false;
		}
	}
	
	function delArticle($articleId) {
		$sql = "DELETE FROM articles WHERE id = '".$articleId."'";
		db::getInstance()->query($sql);
	}
	
	function getAllPageMenu($filter = array()) {
		
		if(isset($filter['id']))
			$where[] = "id ='".(int)$filter['id']."'";
			
			
		$sql = "SELECT id as menu_id, name as menu_name, sort as menu_sort FROM page_menu";
		
		if(count($where)>0) {
			$sqlWhere = implode(' and ',$where);
			$sql .= ' WHERE '.$sqlWhere;
		}
		//echo $sql; exit;
		$query = db::getInstance()->query($sql);
		
		while($result = mysql_fetch_array($query, MYSQL_ASSOC)) {
			$res[] = $result;
		}
		
		if(isset($res) and !empty($res))
			return $res; 
		else
			return false;
	}
	
	function createPageMenu($params = array()) {
	
		$valuesArr = array('id' => "'NULL'");
		
		if(isset($params['name'])) {	
			$valuesArr['name'] = "'".clean($params['name'], true)."'";
		}
		
		if(isset($params['sort'])) {
			$valuesArr['sort'] = "'".(int)$params['sort']."'";
		}
		
		$collsArr = array_keys($valuesArr);
		
		$colls = implode(', ',$collsArr);
		
		$values = implode(', ',$valuesArr);
		
		$sql = "INSERT INTO page_menu (".$colls.") values (".$values.");";

		$result = db::getInstance()->query($sql);
		
		return $result;	
	}
	
	function editPageMenu($id, $params = array()) {
		
		if(isset($id)) {
		
			$id				= (int)$id;
			$setArray		= array();
			
			if(isset($params['name']) and !empty($params['name'])) {
				$name			= clean($params['name'], true);
				$setArray[]		= "name = '".$name."'";
			}
			
			if(isset($params['sort'])) {
				$sort	= (int)$params['sort'];
				$setArray[]		= "sort = '".$sort."'";
			}
			
			if(count($setArray)>0) {
				
				$setStr = implode(', ',$setArray);
				
				$sql = "UPDATE page_menu SET ".$setStr;
				
				$sql .= " WHERE id = ".(int)$id.";";
				
				$result = db::getInstance()->query($sql);
				
				return $result;
			
			} else {
				return false;
			}
		} else {
			return false;
		}
	}
	
	
	function getAllPageMenuItem($filter = array()) {
		
		if(isset($filter['id']))
			$where[] = "id ='".(int)$filter['id']."'";
			
		if(isset($filter['menuId']))
			$where[] = "page_menu_id ='".(int)$filter['menuId']."'";
			
			
		$sql = "SELECT id, name, url, sort, page_menu_id, active FROM page_menu_items";
		
		if(count($where)>0) {
			$sqlWhere = implode(' and ',$where);
			$sql .= ' WHERE '.$sqlWhere;
		}
		//echo $sql; exit;
		$query = db::getInstance()->query($sql);
		
		while($result = mysql_fetch_array($query, MYSQL_ASSOC)) {
			$res[] = $result;
		}
		
		if(isset($res) and !empty($res))
			return $res; 
		else
			return false;
	}
	
	function editPageMenuItem($id, $params = array()) {
		
		if(isset($id)) {
		
			$id				= (int)$id;
			$setArray		= array();
			
			if(isset($params['name']) and !empty($params['name'])) {
				$name			= clean($params['name'], true);
				$setArray[]		= "name = '".$name."'";
			}
			
			if(isset($params['sort'])) {
				$sort	= (int)$params['sort'];
				$setArray[]		= "sort = '".$sort."'";
			}
			
			if(isset($params['url'])) {
				$url	= clean($params['url'], true);
				$setArray[]		= "url = '".$url."'";
			}
			
			if(isset($params['active'])) {
				$active	= (int)$params['active'];
				if($active == 1)
					$setArray[]		= "active = '1'";
				else
					$setArray[]		= "active = '0'";
			}
			
			if(count($setArray)>0) {
				
				$setStr = implode(', ',$setArray);
				
				$sql = "UPDATE page_menu_items SET ".$setStr;
				
				$sql .= " WHERE id = ".(int)$id.";";
				
				$result = db::getInstance()->query($sql);
				
				return $result;
			
			} else {
				return false;
			}
		} else {
			return false;
		}
	}
	
	function addPageMenuItem($params = array()) {
	
		$valuesArr = array('id' => "'NULL'");
		
		if(isset($params['name'])) {	
			$valuesArr['name'] = "'".clean($params['name'], true)."'";
		}
		
		if(isset($params['url'])) {	
			$valuesArr['url'] = "'".clean($params['url'], true)."'";
		}
		
		if(isset($params['sort'])) {
			$valuesArr['sort'] = "'".(int)$params['sort']."'";
		}
		
		if(isset($params['active'])) {
			if($active == 1)
				$valuesArr['active'] = "'1'";
			else
				$valuesArr['active'] = "'0'";
		}
		
		if(isset($params['sort'])) {
			$valuesArr['sort'] = "'".(int)$params['sort']."'";
		}
		
		if(isset($params['menuId'])) {
			$valuesArr['page_menu_id'] = "'".(int)$params['menuId']."'";
		}
		
		
		$collsArr = array_keys($valuesArr);
		
		$colls = implode(', ',$collsArr);
		
		$values = implode(', ',$valuesArr);
		
		$sql = "INSERT INTO page_menu_items (".$colls.") values (".$values.");";

		$result = db::getInstance()->query($sql);
		
		return $result;	
	}
}
?>