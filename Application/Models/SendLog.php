<?php
/**
 * Created by JetBrains PhpStorm.
 * User: pinky
 * Date: 26.05.12
 * Time: 23:01
 * To change this template use File | Settings | File Templates.
 */
namespace Application\Models;

class SendLog extends \ActiveRecord\Model
{
    static $belongs_to = array(array('message_template', 'class_name' => 'Application\Models\MessageTemplate'));
}
