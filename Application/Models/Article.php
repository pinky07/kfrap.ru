<?php
namespace Application\Models;
use \Application\Classes\Messenger\MessengerClient;
use \Application\Classes\Messenger\Message;

class Article extends \ActiveRecord\Model
{
    public $publish_twitter;

    static $belongs_to = array(array(
        'category',
        'class_name' => 'Application\Models\Category')
    );

    static $has_many = array(array(
        'document_article_relations',
        'class_name' => 'Application\Models\DocumentArticleRelation')
    );

    /**
     * Перед созданием нового материала записываем id
     * пользователя и дату создания
     */
    public function before_create()
    {
        $this->add_user_id  = User::getCurrent()->id;
        $this->add_date     = date('Y-m-d H:i:s');
    }

    /**
     * Перед сохранением изменений записываем id
     * пользователя обновившего материал и дату изменения
     */
    public function before_update()
    {
        $this->edit_user_id  = User::getCurrent()->id;
        $this->edit_date     = date('Y-m-d H:i:s');
    }

    /**
     * @param $twitter
     */
    public function set_twitter_published($twitter)
    {
        if($this->read_attribute('twitter_published') == 0 && $twitter)
        {
            $this->publish_twitter = true;
            $this->assign_attribute('twitter_published', 1);
        }
    }

    /**
     *
     */
    public function after_save()
    {
        if($this->publish_twitter)
        {
            $statusText = sprintf('%s %s/news/view/id/%s',
                $this->read_attribute('preview'),
                \Framework\Framework::app()->getConfig('common')->site_url,
                $this->id
            );

            $message = MessengerClient::createMessage(Message::TWITTER, array('message' => $statusText), 'kfrap');
            MessengerClient::send($message);
        }
    }

    /**
     * @param $picture
     */
    public function set_slider_picture($picture)
    {
        if(!empty($picture))
        {
            $this->assign_attribute('slider_picture', $picture);
        }
    }

    /**
     * Возвращает список опубликованных материалов (новостей),
     * если указать необязательный параметр $category_id
     * вернет все материалы из указанного раздела, по умолчаниию
     * возвращает материалы из всех разделов
     * @param $params
     * @return array
     */
    public function getPublished($params)
    {
        if(isset($params['category']) && !empty($params['category']))
        {
            $condition = array(
                'status = ? and categories.active = ? and categories.id = ?', 1, 1, (int)$params['category']);
        }
        else
        {
            $condition = array('status = ? and categories.active = ?', 1, 1);
        }

        if(isset($params['date']) && !empty($params['date']))
        {
            $condition[0] .= ' and add_date = ?';
            $condition[]   = $params['date'];
        }

        return $this->all(array(
            'joins'         => array('category'),
            'conditions'    => $condition,
            'order'         => (isset($params['sort']) && $params['sort'] == 'date')?'add_date DESC':'sort ASC',
            'limit'         => (isset($params['limit']))?(int)$params['limit']:0
        ));
    }


    /**
     * Возвращает массив картинок содержащихся
     * в контенте материала
     * @param bool $first
     * @return array
     */
    public function getImages($first = false)
    {
        $images = array();

        preg_match_all("/<img[^>]*?src=[\"']+(.*)[\"']+/iU", $this->content, $result);

        foreach($result[1] as $image)
        {
            if(file_exists(_WEB_ROOT_DIR_.$image))
            {
                $images[] = $image;
            }
        }

        if($first)
        {
            return (isset($images[0])) ? $images[0] : null;
        }

        return $images;
    }

    public function getDocuments()
    {
        $documents = unserialize($this->documents);

        if(!is_array($documents))
        {
            return false;
        }

        return \Framework\Model::ActiveRecord('Document')->all(array(
            'conditions' => array('id in (?)', $documents))
        );
    }



	function getAccessArticle($id) {

		$category = Model::factory('Category');

		$article = self::getArticle($id);

		$categoriesArr	= $category->getAccessCategories('id');

		if(!empty($article) and !empty($categoriesArr)) {
			$userRole = User::getUserRole();

			if(!empty($userRole)) {
				if((in_array($article['category'], $categoriesArr)) or $userRole == 1) {
					return $article;
				} else {
					return false;
				}
			} else {
				return false;
			}
		} else {
			return false;
		}
	}

	function getAccessArticles() {

		$category = Model::factory('Category');

		$articlesArr 	= self::getAllArticles();

		$categoriesArr	= $category->getAccessCategories('id');


		if(!empty($articlesArr) and !empty($categoriesArr)) {
			foreach($articlesArr as $value) {
				foreach($categoriesArr as $val) {
					if($value['category'] == $val)
						$articles[] = $value;
				}
			}
			if(isset($articles)) {
				return $articles;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}

	function getArticle($articleId) {
		$sql = "SELECT * FROM article where id ='".(integer)$articleId."';";
		$query = db::getInstance()->query($sql);
		$result = mysql_fetch_array($query, MYSQL_ASSOC);
		if(isset($result))
			return $result;
		else
			false;
	}

	function editArticle($id, $params = array()) {

		if(isset($id)) {

			$id				= (int)$id;
			$setArray		= array();

			if(isset($params['title']) and !empty($params['title'])) {
				$title			= clean(clean($params['title']), true);
				$setArray[]		= "title = '".$title."'";
			}

			if(isset($params['description'])) {
				$preview	= clean($params['description'], true);
				$setArray[]		= "preview = '".$preview."'";
			}

			if(isset($params['slider'])) {
				$slider	= (int)$params['slider'];
				$setArray[]		= "slider = '".$slider."'";
			}

			if(isset($params['sort'])) {
				$sort	= (int)$params['sort'];
				$setArray[]		= "sort = '".$sort."'";
			}

			if(isset($params['content'])) {
				$content	= clean($params['content'], true);
				$setArray[]		= "content = '".$content."'";
			}

			if(isset($params['tags'])) {
				$tags	= clean($params['tags'], true);
				$setArray[]		= "tags = '".$tags."'";
			}

			if(isset($params['category'])) {
				$category	= (int)$params['category'];
				$setArray[]		= "category = '".$category."'";
			}

			if(isset($params['picture'])) {
				$picture	= clean($params['picture'], true);
				$setArray[]		= "picture_link = '".$picture."'";
			}

			if(isset($params['thumb'])) {
				$thumb	= clean($params['thumb'], true);
				$setArray[]		= "thumb_link = '".$thumb."'";
			}

			if(isset($params['status'])) {
				$status	= (int)$params['status'];
				$setArray[]		= "status = '".$status."'";
			}

			if(count($setArray)>0) {
				$setArray[]		= "last_edit = '".date('Y-m-d H:i')."'";

				$setStr = implode(', ',$setArray);

				$sql = "UPDATE article SET ".$setStr;

				$sql .= " WHERE id = ".(int)$id.";";

				$result = db::getInstance()->query($sql);

				return $result;

			} else {
				return false;
			}
		} else {
			return false;
		}
	}

	function delArticle($articleId) {
		$sql = "DELETE FROM articles WHERE id = '".$articleId."'";
		db::getInstance()->query($sql);
	}
	function getArticlesCatId($categoryId) {
		$sql = "SELECT * FROM articles WHERE category_id = '".(integer)$categoryId."' order by date DESC;";
		$query = db::getInstance()->query($sql);
		while($result = mysql_fetch_array($query, MYSQL_ASSOC)) {
		$res[] = $result;
		}
		return $res;
	}

	function getArticlesGoHome($categoryId) {

		$sql = "SELECT * FROM articles WHERE category_id = '".(integer)$categoryId."' and place_at_home='1' order by date DESC;";

		$query = db::getInstance()->query($sql);
		while($result = mysql_fetch_array($query, MYSQL_ASSOC)) {
		$res[] = $result;
		}
		return $res;
	}

	function getForActivate() {
		$sql = "select * from article where pos='0'";

		$query = db::getInstance()->query($sql);
		while($result = mysql_fetch_array($query, MYSQL_ASSOC)) {
		$res[] = $result;
		}
		if(isset($res))
			return $res;
		else
			false;
	}
}
?>