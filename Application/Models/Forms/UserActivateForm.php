<?php
/**
 * Created by JetBrains PhpStorm.
 * User: pinky
 * Date: 21.05.12
 * Time: 21:30
 * To change this template use File | Settings | File Templates.
 */
namespace Application\Models\Forms;
use Framework\Model;

class UserActivateForm extends \Framework\Form\Model
{
    public $email;

    static $validates_email = array(
            array(
                'email',
                'message' => 'Неправильный формат Email'
            )
        );

    public function validate()
    {
        if(!Model::ActiveRecord('Person')->exists(
            array(
                'email' => $this->email, 'user_id' => ''
            )
        ))
        {
            $this -> error -> add('email', "email '{$this->email}' не найден в базе данных");
        }
    }

    public function activate()
    {

    }
}
