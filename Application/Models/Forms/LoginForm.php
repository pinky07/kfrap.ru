<?php
/**
 * Created by JetBrains PhpStorm.
 * User: pinky
 * Date: 19.02.12
 * Time: 18:26
 * To change this template use File | Settings | File Templates.
 */
namespace Application\Models\Forms;
use Framework\Model;
use Application\Models\User;

class LoginForm extends \Framework\Form\Model
{
    public   $login       = null;
    public   $password    = null;

    static $validates_presence_of = array(
        array(
            'login',
            'message'    => 'Необходимо ввести логин!'),
        array(
            'password',
            'message' => 'Необходимо ввести пароль!'),
    );

    static $validates_format_of = array(
        array(
            'login',
            'with' => '/^[a-zA-Z0-9@_]{3,26}$/',
            'message' => 'Логин должен содержать только латинские буквы, цифры и знаки (\'@\', \'_\', \'.\')'),
        array(
            'password',
            'with' => '/^[a-zA-Z0-9]{3,16}$/',
            'message' => 'Пароль должен содержать только латинские буквы и цифры'),
    );

    public function validate()
    {
        $this->login = \Framework\String::lower($this->login);

        $findParams = array('conditions' => array('login = ? or email = ?', $this->login, $this->login));

        if(!Model::ActiveRecord('User')->exists($findParams))
        {
            $this -> error -> add('login', "Пользователь '{$this->login}' не существует");
        }
        else
        {
            $user = Model::ActiveRecord('User')->first($findParams);

            if($user->password !== $this->password)
            {
                $this -> error -> add('login', "Неправильнаяя пара логин/пароль");
            }
        }
    }

    public function auth()
    {
        return User::auth($this->login);
    }
}
