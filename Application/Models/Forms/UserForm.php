<?php
/**
 * Created by JetBrains PhpStorm.
 * User: pinky
 * Date: 21.05.12
 * Time: 1:51
 * To change this template use File | Settings | File Templates.
 */
namespace Application\Models\Forms;
use Application\Models\User;

class UserForm extends \Framework\Form\Model
{
    public  $login,
            $email,
            $password,
            $role_id,
            $status,
            $userId;

    static $validates_presence_of = array(
            array(
                'login',
                'message'   => 'Не указано логин'),
            array(
                 'email',
                 'message'   => 'Не указан email'),
            array(
                'password',
                'message'   => 'Не указан пароль'),
            array(
                'role_id',
                'message'   => 'Не выбрана группа'),
            );

    static $validates_email = array(
            array(
                'email',
                'message' => 'Неправильный формат Email'
            )
        );

    static $validates_format_of = array(
            array(
                'login',
                'with' => '/^[a-zA-Z0-9@_]{3,16}$/',
                'message' => 'Логин должен содержать только латинские буквы, цифры'),
            array(
                'password',
                'with' => '/^[a-zA-Z0-9]{3,16}$/',
                'message' => 'Пароль должен содержать только латинские буквы и цифры'),
        );

    public function validate()
    {
        $this->login = \Framework\String::lower($this->login);
        $this->email = \Framework\String::lower($this->email);

        if(!\Framework\Model::ActiveRecord('Role')->exists($this->role_id))
        {
            $this -> error -> add('role_id', 'Группа не найдена');
        }

        if(\Framework\Model::ActiveRecord('User')->exists(array(
            'conditions' => array(
                'id <> ? and login = ?', $this->userId, $this->login)
            )
        ))
        {
            $this -> error -> add('login', "Пользователь с логином {$this->login} уже существует");
        }

        if($this->email && \Framework\Model::ActiveRecord('User')->exists(
            array(
                'conditions' => array(
                            'id <> ? and email = ?', $this->userId, $this->email)
            )
        ))
        {
            $this -> error -> add('login', "Пользователь с email {$this->email} уже существует");
        }
    }
}
