<?php
/**
 * Created by JetBrains PhpStorm.
 * User: pinky
 * Date: 20.05.12
 * Time: 16:12
 * To change this template use File | Settings | File Templates.
 */
namespace Application\Models\Forms;

class PageForm extends \Framework\Form\Model
{
    public  $title,
            $menu_id,
            $tags,
            $content,
            $layout,
            $status,
            $responsible,
            $parent_page_id,
            //$sef_url,
            $documents;

    static $validates_presence_of = array(
                array(
                    'title',
                    'message'   => 'Не заполнен заголовок страницы'),
                array(
                    'content',
                    'message'   => 'Не заполнен контент материала')
                );

    public function validate()
    {
        if(!\Framework\Model::ActiveRecord('Menu')->exists($this->menu_id) && $this->menu_id != 0)
        {
            $this -> error -> add('menu_id', 'Меню не найдено');
        }

        if(!in_array($this->layout, array('frontend/html', 'frontend/html2')))
        {
            $this -> error -> add('layout', 'Неправильный макет страницы');
        }

        if(!empty($this->responsible) && \Application\Models\User::getCurrent()->id !== 1)
        {
            $this -> error -> add('responsible', 'Ответственного может назначать только администратор');
        }
        
        $this->documents = serialize($this->documents);
    }
}
