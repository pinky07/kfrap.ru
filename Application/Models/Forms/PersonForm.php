<?php
/**
 * Created by JetBrains PhpStorm.
 * User: pinky
 * Date: 20.05.12
 * Time: 18:06
 * To change this template use File | Settings | File Templates.
 */
namespace Application\Models\Forms;
use Framework\Uploaders\Uploader as Uploader;
use \Framework\Model;

class PersonForm extends \Framework\Form\Model
{
    public  $first_name,
            $last_name,
            $middle_name,
            $division_id,
            $appointment,
            $activity,
            $email,
            $phone,
            $tags,
            $info,
            $publish,
            $photo_link;

    public  $personId; # вспомогательное свойство

    static $validates_presence_of = array(
        array(
            'first_name',
            'message'   => 'Не указано имя'),
        array(
             'last_name',
             'message'   => 'Не указана фамилия'),
        array(
            'division_id',
            'message'   => 'Не выбрано подразделение'),
        array(
            'appointment',
            'message'   => 'Не указана должность'),
        array(
            'activity',
            'message'   => 'Не указано направление деятельности'),
    );

    static $validates_email = array(
        array(
            'email',
            'message' => 'Неправильный формат Email'
        )
    );

    static $validates_numericality_of = array(
        array(
            'phone',
            'only_integer'  => true,
            'message'       => 'Неправильный формат телефона'
        )
    );

    public function validate()
    {
        /** @var $imageUploader \Framework\Uploaders\ImageUploader */
        $imageUploader = Uploader::factory('Image');

        if(!$imageUploader -> isEmpty('photo_link'))
        {
            $imageUploader -> to = '../public/upload/people';

            if($imageUploader -> uploads('photo_link'))
            {
                $this -> photo_link = $imageUploader -> name;
            }
            else
            {
                $this -> error -> add('photo_link', 'Не удалось загрузить изображение');
            }
        }

        //Проверка email'a на уникальность
        if((!empty($this->email)) && (Model::ActiveRecord('Person')->exists(array(
                'conditions' => array('id <> ? and email = ?', $this->personId, $this->email))) ||
            Model::ActiveRecord('User')->exists(array(
                 'conditions' => array('person_id <> ? and email = ?', $this->personId, $this->email)))))
        {
            $this -> error -> add('email', 'Персоналия или пользователь с данным email уже существует');
        }

        if(!Model::ActiveRecord('Division')->exists($this->division_id))
        {
            $this -> error -> add('division_id', 'Подразделение не найдено');
        }
    }
}
