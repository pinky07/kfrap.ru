<?php
/**
 * Created by JetBrains PhpStorm.
 * User: pinky
 * Date: 23.05.12
 * Time: 1:29
 * To change this template use File | Settings | File Templates.
 */
namespace Application\Models\Forms;
use \Framework\Model;

class QuestionAnsweredForm extends \Framework\Form\Model
{
    public  $answerer,
            $answer_text;

    static $validates_presence_of = array(
            array(
                'answerer',
                'message'   => 'Не указано имя отвечающего'),
            array(
                 'answer_text',
                 'message'   => 'Поле ответ не может быть пустым'),
    );
}
