<?php
/**
 * Created by JetBrains PhpStorm.
 * User: pinky
 * Date: 18.05.12
 * Time: 1:27
 * To change this template use File | Settings | File Templates.
 */
namespace Application\Models\Forms;
use Application\Models as Models;
use Framework\Model as Model;
use Framework\Uploaders\Uploader as Uploader;

class ArticleForm extends \Framework\Form\Model
{
    public  $title,
            $preview,
            $content,
            $category_id,
            $tags,
            $sort,
            $actual_date,
            $slider_picture,
            $status,
            $slider,
            $twitter_published,
            $documents;

    static $validates_presence_of = array(
            array(
                'title',
                'message'   => 'Заголовок метериала не может быть пустым'),
            array(
                 'preview',
                 'message'   => 'Анонс не может быть пустым'),
            array(
                'content',
                'message'   => 'Контент материала должен содержать текст'),
            array(
                'category_id',
                'message'   => 'Не выбран раздел материала'),
            array(
                'actual_date',
                'message'   => 'Не установлена дата публикации'),
        );

    public function validate()
    {
        if(!\Framework\Model::ActiveRecord('Category')->exists($this->category_id))
        {
            $this -> error -> add('first_name', 'Выбран неправильный  раздел');
        }

        $imageUploader = Uploader::factory('Image');

        if(!$imageUploader -> isEmpty('slider_picture'))
        {
            if($imageUploader -> uploads('slider_picture'))
            {
                $this -> slider_picture = $imageUploader -> name;
            }
            else
            {
                $this -> error -> add('slider_picture', 'Не удалось загрузить изображение');
            }
        }
        
        $this->documents = serialize($this->documents);
    }
}
