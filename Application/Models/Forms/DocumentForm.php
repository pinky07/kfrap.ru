<?php
/**
 * Created by JetBrains PhpStorm.
 * User: pinky
 * Date: 23.05.12
 * Time: 1:29
 * To change this template use File | Settings | File Templates.
 */
namespace Application\Models\Forms;
use \Framework\Model;
use Framework\Uploaders\Uploader as Uploader;
class DocumentForm extends \Framework\Form\Model
{
    public  $name,
            $description,
            $status,
            $filename,
            $type,
            $size;

    static $validates_presence_of = array(
            array(
                'name',
                'message'   => 'Не указано имя отвечающего')
    );

    public function validate()
    {
        $imageUploader = Uploader::factory('File');

        if(!$imageUploader -> isEmpty('file'))
        {

            if($imageUploader -> uploads('file'))
            {
                $this -> filename = $imageUploader -> name;
                $this -> type = $imageUploader->type;
                $this -> size = $imageUploader->size;
            }
            else
            {
                $this -> error -> add('file', 'Не удалось загрузить file');
            }
        }
        else
        {
            $this -> error -> add('file', 'Не выбран файл для загрузки!');
        }
    }
}