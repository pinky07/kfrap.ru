<?php
/**
 * Created by JetBrains PhpStorm.
 * User: pinky
 * Date: 21.02.12
 * Time: 22:52
 * To change this template use File | Settings | File Templates.
 */
namespace Application\Models;

class MenuItem extends \ActiveRecord\Model
{
    static $belongs_to = array(array('menu', 'class_name' => 'Application\Models\Menu'), array('parent_item', 'foreign_key' => 'parent_item_id', 'class_name' => 'Application\Models\MenuItem'));
    static $has_many = array(
        array('children_items', 'foreign_key' => 'parent_item_id', 'class_name' => 'Application\Models\MenuItem')
    );
}
