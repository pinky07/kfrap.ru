<?php
class Cookie extends Model {
	
	public static $expiration = COOKIE_EXP;

	public static $path = COOKIE_PATH;

	public static $domain = COOKIE_DOMAIN;

	public static $secure = COOKIE_SECURE;

	public static $httponly = COOKIE_HTTPONLY;

}
?>