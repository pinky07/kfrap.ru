<?php
namespace Application\Models;
use \Framework\Model as Model;

class Menu extends \ActiveRecord\Model
{
    static $has_many = array(array('menu_items', 'class_name' => 'Application\Models\MenuItem'));
	
	public function getAsMainMenu()
    {
        $items = Model::ActiveRecord('MenuItem')->find('all', array('conditions' => array('menu_id = ? ',$this->id), 'order' => 'sort asc'));

        $main_menu[] = array('url' => \Framework\Framework::app()->getConfig('common')->site_url, 'name' => '<img style="padding:5px 2px 6px" src="/images/ic_menu_home.png"/>', 'parents' => 0);
        foreach($items as $item)
        {
            if($item->parent_item_id != 0)
                continue;

            $parents = array();

            foreach($items as $parent_item)
            {
                if($item->id == $parent_item->parent_item_id)
                    $parents[] = array('url' => $parent_item->url, 'name' => $parent_item->name);
            }

            $main_menu[] = array('url' => $item->url, 'name' => $item->name, 'parents' => $parents);
        }
        return $main_menu;
    }
	
	public function getItems() {
        
	}
	
	function getForActivate() {
		$sql = "select * from article where pos='0'";
		
		$query = db::getInstance()->query($sql);
		while($result = mysql_fetch_array($query, MYSQL_ASSOC)) {
		$res[] = $result;
		}
		if(isset($res))
			return $res;	
		else
			false;
	}
}
?>