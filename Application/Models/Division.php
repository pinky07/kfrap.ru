<?php
namespace Application\Models;

class Division extends \ActiveRecord\Model
{
    static $has_many = array(array(
        'person',
        'class' => 'Application\Models\Person')
    );

    //список всех кафедр
    const TRIBUNE_OF_STATE_AND_LEGAL                = 18;
    const TRIBUNE_OF_CIVIL_CASE                     = 19;
    const TRIBUNE_OF_CIVIL_PROCEDURE                = 20;
    const TRIBUNE_OF_SOCIAL_AND_ECONOMIC            = 21;
    const TRIBUNE_OF_GENERAL_STUDIES                = 22;
    const TRIBUNE_OF_GENERAL_THEORETICAL            = 23;
    const TRIBUNE_OF_LEGAL_INFORMATICS              = 24;
    const TRIBUNE_OF_CRIMINAL_LAW                   = 25;
    const TRIBUNE_OF_PHYSICAL_EDUCATION             = 26;
    const TRIBUNE_OF_LINGUISTICS                    = 27;

    private $_tribunes = array(
        self::TRIBUNE_OF_STATE_AND_LEGAL,
        self::TRIBUNE_OF_CIVIL_CASE,
        self::TRIBUNE_OF_CIVIL_PROCEDURE,
        self::TRIBUNE_OF_SOCIAL_AND_ECONOMIC,
        self::TRIBUNE_OF_GENERAL_STUDIES,
        self::TRIBUNE_OF_LEGAL_INFORMATICS,
        self::TRIBUNE_OF_CRIMINAL_LAW,
        self::TRIBUNE_OF_PHYSICAL_EDUCATION,
        self::TRIBUNE_OF_LINGUISTICS
    );

    public function getTribunes()
    {
        return $this->_tribunes;
    }
}