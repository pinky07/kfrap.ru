<?php
namespace Application\Models;

class Category extends \ActiveRecord\Model
{
    static $has_many = array(array(
        'articles',
        'class_name' => 'Application\Models\Article')
    );

	function showTitle()
    {
	    echo $this -> title;
	}
	
	function getDesc()
    {
	    return $this -> description;
	}
	
	function getId()
    {
	    return $this -> id;
	}
}
?>