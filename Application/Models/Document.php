<?php
namespace Application\Models;

class Document extends \ActiveRecord\Model
{
    /**
     * Удаляем файл, перед удалением в бд
     */
    public function before_destroy()
    {
        $filePath = sprintf(
            '/upload/files/%s',
            $this->filename
        );

        unlink($filePath);
    }
}