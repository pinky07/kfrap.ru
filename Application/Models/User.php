<?php
namespace Application\Models;
use \Framework\Session as Session;
use \Framework\Model;

class User extends \ActiveRecord\Model
{
    static $belongs_to = array(
        array(
            'role', 'class_name'  => 'Application\Models\Role'
        ),
        array(
            'person', 'class_name'=> 'Application\Models\Person'
        )
    );

    /**
     * Перед создание нового пользователя автоматически создается персоналия для него
     */
    public function before_create()
    {
        //Дата регистрации
        $this->reg_date = time();

        if(empty($this->person_id) || !Model::ActiveRecord('Person')->exists($this->person_id))
        {
            $person = Model::ActiveRecord('Person');

            $person -> set_attributes(array(
                'email'     => $this->email,
                'publish'   => 0
            ));

            $person -> save();

            $this->person_id = $person->id;
        }
    }

    public function after_create()
    {
        $this->person->user_id = $this->id;

        $this->person->save();
    }

    /**
     * Проверка перед сохранением, что бы случайно не сохранить гостя
     * @throws \ActiveRecord\ModelException
     */
    public function before_save()
    {
        if($this->role_id == Role::GUEST)
        {
            throw new \ActiveRecord\ModelException('User with guest role can not be saved');
        }

        $this->login = \Framework\String::lower($this->login);
        $this->email = \Framework\String::lower($this->email);
    }
    /**
     * Возвращает текущего пользователя
     * @static
     * @return User|null
     */
    public static function getCurrent()
    {

        if(Session::getInstance()->get('id', 'user') && Model::ActiveRecord('User')->exists((int)Session::getInstance()->get('id', 'user')))
        {
            return Model::ActiveRecord('User')->find(Session::getInstance()->get('id', 'user'));
        }

        $user = Model::ActiveRecord('User');
        $user -> set_attributes(array(
                    'login'     => 'guest',
                    'role_id'   => Role::GUEST,
                    'name'      => 'Гость'
                ));

        return $user;
    }

    /**
     * @static
     * @return bool
     */
    public static function isAuth()
    {
        return Session::getInstance()->get('auth', 'user');
    }

    /**
     * @static
     * @param $login
     * @return bool
     */
    public static function auth($login)
    {
        $login = \Framework\String::lower($login);

        $findParams = array('conditions' => array('login = ? or email = ?', $login, $login));

        if(!self::exists($findParams))
        {
            return false;
        }

        $user = self::first($findParams);

        $user->set_attributes(array(
            'last_visit'    => time(),
            'last_visit_ip' => self::getUserIp()
            )
        );

        $user->save();

        Session::getInstance()->set('id',   $user->id, 'user');
        Session::getInstance()->set('login', $user->login, 'user');
        Session::getInstance()->set('role', $user->role->id, 'user');
        Session::getInstance()->set('auth', true, 'user');

        return true;
    }
	//надо поменять!!!
	/*function checkAccessController($controller) {
		if(self::getUserId() > 0) {
			$userGroupId = self::getUserRole();

			$userOrders = Model_Roles::getOrders($userGroupId);
			
			$splits = explode('_', $controller);
			if(strtolower($splits['1']) == 'backend') {
				if(isset($userOrders[strtolower($splits['2'])]['access']) and $userOrders[strtolower($splits['2'])]['access'] == 1) {
					return true;
				} else {
					return false;
				}
			} else {
				if(isset($userOrders[strtolower($splits['2'])]['access']) and $userOrders[strtolower($splits['2'])]['access'] == 1) {
					return true;
				} else {
					return true;
				}
			}
		//если пользователь не авторизован
		} else {
			$splits = explode('_', $controller);
			if(strtolower($splits['1']) == 'backend' and strtolower($splits['2']) != 'auth')
				return false;
			else
				return true;
		}
	}

	function checkAccessAction($controller) {
		return true;
	}
	
	function getUserRole($id = 'self') {
		if($id == 'self') {
			$userId = self::getUserId();
		} else {
			$userId = $id;
		}
		$sql = 'SELECT group_id FROM users WHERE id =\''.$userId.'\'';
		$query = db::getInstance()->query($sql);
		$result = mysql_fetch_array($query, MYSQL_ASSOC);
		if(isset($result))
			return $result['group_id'];	
		else
			false;
	}
	
	public static function getUserId() {
		if(isset($_SESSION['userId'])) {
			return (integer)$_SESSION['userId'];
		} else {
			return 0;
		}
	}*/
	




    public static function getLogin() {
        return Session::getInstance()->get('login', 'user');
    }

    public static function getUserIp() {
        if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
            $ip = $_SERVER['HTTP_CLIENT_IP'];
        }
        elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        } else {
            $ip = $_SERVER['REMOTE_ADDR'];
        }
        return $ip;
    }
	
	function phpBBAuth($username, $password) {
	
		$sql = mysql_query("select * from phpbb_users where username='".mysql_escape_string($username)."'");		
		$line = mysql_fetch_array($sql);
		
		$user_password = $line_phpbb['user_password'];
		
		if(phpbb_check_hash($password, $user_password)) {
		
		}
	}
	
	function authSiteAndForum($login, $pass) {
		if ((preg_match("/^[a-z0-9]{3,16}$/", $login)) and (preg_match("/^[a-z0-9]{3,16}$/", $pass))) {
			$sql = 'SELECT	site.username, site.pass, site.id, site.name, forum.user_id, forum.username, forum.user_password
					FROM	users AS site, phpbb_users AS forum
					WHERE	site.username =  \''.$login.'\'
					AND		site.username = forum.username;';
					
			$userArray = mysql_fetch_array(db::getInstance()->query($sql));
			
			if(db::getInstance()->numRows() > 0) {
				if(($userArray['pass'] == $pass) and (phpbb_check_hash($pass, $userArray['user_password']))) {
				
					$siteUserId 	= $userArray['id'];
					$forumUserId	= $userArray['user_id'];
					
					$updSqlSite 	= 'UPDATE users SET lastonline=\''.time().'\', lastip=\''.getUserIp().'\' WHERE id = \''.$siteUserId.'\';';
					
					$updSqlForum	= 'UPDATE phpbb_users set user_lastvisit = \''.time().'\' where user_id = \''.$forumUserId.'\';';
					
					if((db::getInstance()->query($updSqlSite)) and (db::getInstance()->query($updSqlForum))) {
						Session::getInstance()->_regenerate();
						Session::getInstance()->_addVal(array(
															'auth'	=> 1,
															'userId'	=> $userArray['id'],
															'login'		=> $userArray['username'],
															'name'		=> $userArray['name']));
						
						$autologin = false;
						
						$cookie_expires = time() + ($autologin ? 86400*2 : COOKIE_EXP);
						
						// сессионная кука (обновляем ее)
						
						Cookie::set(SESSION_NAME, session_id(), $cookie_expires);
						
						// кука с ид юзера
						
						Cookie::set(COOKIE_PREFIX.'_u', $forumUserId, $cookie_expires);
						//print_r($_SESSION);
						if($autologin == true){
							$ewe = "автологин вкл";
							set_login_key($forumUserId);
							last_time_update($forumUserId, $autologin);
						}
						else {
							$ewe = "автологин выкл";
							Cookie::set(COOKIE_PREFIX.'_k', "", time() - COOKIE_EXP);
							last_time_update($forumUserId);
						}									
						return 'ok';
					} else {
						return 'ошибка';
					}
				} else {
					return 'неправильнаяя пара логин/пароль.';			
				}
			} else {
				return 'пользователь '.$login.' не существует.';
			}
		} else {
			return 'логин и пароль должны содержать только латинские буквы и цифры';
		}	
	}
	
	public static function logout()
    {
		unset($_SESSION['user']);
	}
	
	public static function register($username, $pass, $mail, $group, $active, $name = '', $surname = '', $country = '', $city = '', $timezone = 3, $avatar = '', $info = '') {
		if ((preg_match("/^[a-z0-9]{3,16}$/", $username)) and (preg_match("/^[a-z0-9]{3,16}$/", $pass)) and (preg_match("/^[a-zA-Z0-9]{2,16}@[a-zA-Z0-9]{3,16}\.[a-zA-Z]{2,3}$/", $mail))) {
		
			$username	= mysql_escape_string($username);
			$pass		= mysql_escape_string($pass);
			$mail		= mysql_escape_string($mail);
			$group		= (int)$group;
			$active		= (int)$active;
			$tipster	= 0;
			$name		= mysql_escape_string($name);
			$surname	= mysql_escape_string($surname);
			$country	= (int)$country;
			$city		= mysql_escape_string($city);
			$timezone	= (int)$timezone;
			$avatar		= mysql_escape_string($avatar);
			$info		= mysql_escape_string($info);
			
			
			$sql = "SELECT * FROM users WHERE username = '".$username."' or email = '".$mail."';";
			
			$userArray = mysql_fetch_array(db::getInstance()->query($sql));
			
			if(db::getInstance()->numRows() == 0) {
				$regdate = time();
				
				$code = '';
				
				for( $i=0; $i<=9; $i++ ) {
					 $code .= chr( mt_rand( 65, 90 ) );
				}
				
				$sqlInsert = "INSERT INTO users (
					`id` ,
					`username` ,
					`pass` ,
					`email` ,
					`group_id` ,
					`name` ,
					`surname` ,
					`country` ,
					`city` ,
					`timezone` ,
					`avatar` ,
					`info` ,
					`tipster` ,
					`recdate` ,
					`lastonline` ,
					`lastip` ,
					`code` ,
					`pos`
					)
					VALUES (
					NULL ,  '".$username."',  '".$pass."',  '".$mail."',  ".(int)$group.",  '".$name."',  '".$surname."',  ".(int)$country.",  '".$city."',  '".(int)$timezone."',  '".$avatar."',  '".$info."',  ".(int)$tipster.",  ".time().",  '',  '".$_SERVER['REMOTE_ADDR']."',  '".$code."',  ".$active.");";
					
					$hpass = phpbb_hash($pass);

					$hemail = crc32(strtolower($mail) . strlen($mail));
					
					$sqlInsertPhpbb = "insert into phpbb_users 
								(username, username_clean, user_email, user_email_hash, user_password, user_regdate, user_form_salt, group_id, user_permissions, user_ip) values ('".$username."', '".strtolower($username) ."', '". $mail ."', '".$hemail."', '".$hpass."',  '".$regdate."', '".unique_id()."',  2, '', '".$_SERVER['REMOTE_ADDR']. "')";
					if((db::getInstance()->query($sqlInsert)) and (db::getInstance()->query($sqlInsertPhpbb))) {			
						
						$phpbb_user	= mysql_fetch_array(db::getInstance()->query("select user_id from phpbb_users where username='".$username."'"));
						
						$site_user	= mysql_fetch_array(db::getInstance()->query("SELECT id from users WHERE username ='".$username."'"));
						
						
						$phpbb_user_id	= $phpbb_user['user_id'];
						
						$site_user_id	= $site_user['id'];
						
						
						db::getInstance()->query("insert into phpbb_user_group
							   (user_id, user_pending, group_id)
								values (".$phpbb_user_id.", 0, 2)");
								
						db::getInstance()->query("update phpbb_config set config_value=`config_value`+1 where `config_name`='num_users'");
						
						db::getInstance()->query("update phpbb_config set config_value=".$phpbb_user_id." where config_name='newest_user_id'");
						
						db::getInstance()->query("update phpbb_config set config_value='".$username."' where config_name='newest_username'");
						
						if($active == 0) {
							$template		= mysql_fetch_array(db::getInstance()->query("select * from templates where name='[MAIL]'"));
							$mailtemplate	= mysql_fetch_array(db::getInstance()->query("select msg,subject from mailtemplate where name='[UYELIK]'"));
							
							$mesaj	= str_replace('[email]', $mail, $mailtemplate['msg']);
							$mesaj	= str_replace('[uid]', $site_user_id, $mesaj);
							$mesaj	=	str_replace('[username]', $username, $mesaj);
							$mesaj	=	str_replace('[password]', $pass, $mesaj);
							$mesaj	=	str_replace('[code]', $code, $mesaj);
							$email	=	str_replace('[name]', $mailtemplate['subject'], $template['template']);
							$email	=	str_replace('[content]', $mesaj, $email);
							automail($mail, $mailtemplate['subject'], $email);
						}
						return 'ok';
					} else {
						$error[]	= "Не удалось добавить пользователя!";
					}
			} else {
				if($userArray['username'] == $username)
					$error[]	= "Пользователь <strong>".$username."</strong> уже существует!";
				if($userArray['email'] == $mail)
					$error[]	= "Пользователь с адресом <strong>".$mail."</strong> уже существует!";	
				return $error;
			}
		} else {
			if(!preg_match("/^[a-z0-9]{3,16}$/", $username))
				$error[] = 'логин должен содержать только латинские буквы и цифры';
			if(!preg_match("/^[a-z0-9]{3,16}$/", $pass))
				$error[] = 'пароль должен содержать только латинские буквы и цифры';
			if(!preg_match("/^[a-zA-Z0-9]{2,16}@[a-zA-Z0-9]{3,16}\.[a-zA-Z]{2,3}$/", $mail))
				$error[] = 'неправильный формат электронной почты';
			return $error;
		}
	}
	
	function edit($id, $params) {

		if ((!isset($params['username']) or checkLogin($params['username'])) and (!isset($params['pass']) or checkPass($params['pass'])) and (!isset($params['mail']) or checkMail($params['mail']))) {
			
			$setArraySite	= array();
			$setArrayForum	= array();
			
			if(isset($params['username'])) {
				$username	= mysql_escape_string($params['username']);
				
				$setArraySite[]	 = "login = '".$username."'";
			}
			
			if(isset($params['pass'])) {
				$pass	 = mysql_escape_string($params['pass']);
				
				$setArraySite[]	 = "pass = '".$pass."'";
			}
			
			if(isset($params['mail'])) {
				$mail	 = mysql_escape_string($params['mail']);
				
				$setArraySite[]	 = "email = '".$mail."'";
			}
			
			if(isset($params['group'])) {
				$group	 = (int)$params['group'];
				
				$setArraySite[]	 = "group_id = ".$group;
			}
			
			if(isset($params['active'])) {
				$active	 = (int)$params['active'];
				
				$setArraySite[]	 = "status = ".$active;
			}
			
			if(isset($params['name'])) {
				$name	 = mysql_escape_string($params['name']);
				
				$setArraySite[]	 = "name = '".$name."'";
			}
			
			if(isset($params['surname'])) {
				$surname = mysql_escape_string($params['surname']);
				
				$setArraySite[]	 = "surname = '".$surname."'";
			}
			
			if(isset($params['city'])) {
				$city	 = mysql_escape_string($params['city']);
				
				$setArraySite[]	 = "city = '".$city."'";
			}
			
			if(isset($params['avatar'])) {
				$avatar	 = mysql_escape_string($params['avatar']);
				
				$setArraySite[]	 = "avatar = '".$avatar."'";
			}
			
			if(isset($params['info'])) {
				$info	 = mysql_escape_string($params['info']);
				
				$setArraySite[]	 = "info = '".$info."'";
			}	
			
			if(count($setArraySite)>0) {
				
				$sql	= "SELECT * FROM users WHERE id = '".(int)$id."';";
				
				$curUser	= mysql_fetch_array(db::getInstance()->query($sql));
				
				if(isset($username)) {
					$sql2	= "SELECT * FROM users WHERE login = '".$username."';";
					$userArray	= mysql_fetch_array(db::getInstance()->query($sql2));
				}
				
				if(!isset($username) or (db::getInstance()->numRows() == 0 or $userArray['login'] == $curUser['login'])) {
					
					$setStrSite	= implode(', ',$setArraySite);
					
					$sqlUpd  = "UPDATE users SET ".$setStrSite;				
					$sqlUpd .= " WHERE id = ".(integer)$id.";";
					
					
					
					if(db::getInstance()->query($sqlUpd)) {
						return 'ok';
					} else {
						return 'Не удалось обновить данные';
					}
				} else {
					if($userArray['username'] == $username)
						$error[]	= "Пользователь <strong>".$username."</strong> уже существует!";
					if($userArray['email'] == $mail)
						$error[]	= "Пользователь с адресом <strong>".$mail."</strong> уже существует!";	
					return $error;		
				}
			}
		} else {
			if(checkLogin($params['username']) == false)
				$error[] = 'логин должен содержать только латинские буквы и цифры';
			if(checkPass($params['pass']) == false)
				$error[] = 'пароль должен содержать только латинские буквы и цифры';
			if(checkMail($params['mail']) == false)
				$error[] = 'неправильный формат электронной почты';
			return $error;
		}
	} 
	
	function updateAvatar($uplFile, $userId) {
		$sql = "SELECT * FROM users WHERE id = '".$userId."'";
		
		$userArray = mysql_fetch_array(db::getInstance()->query($sql));
		
		if(db::getInstance()->numRows() == 1) {
			$arr = explode('/', $uplFile['type']);
			$type = $arr['1'];
			if($type == 'jpeg' or $type == 'png' or $type == 'gif') {
			
				$imgName = $userArray['login'].'_'.rand().'.'.$type;
				
				$uplPath = 'upload'.DIRECTORY_SEPARATOR.'avatars'.DIRECTORY_SEPARATOR.$userArray['login'];
				
				if (file_exists($uplPath)) {
				
					imgUpload($uplFile['tmp_name'], $uplPath.DIRECTORY_SEPARATOR.$imgName, 100, 100);
					$sqlUpd = "UPDATE  users SET  avatar =  '".$imgName."' WHERE  id = ".$userArray['id'].";";
					db::getInstance()->query($sqlUpd);
					
				} else {
				
					if(mkdir($uplPath)) {
						imgUpload($uplFile['tmp_name'], $uplPath.DIRECTORY_SEPARATOR.$imgName, 100, 100);
						$sqlUpd = "UPDATE  users SET  avatar =  '".$imgName."' WHERE  id = ".$userArray['id'].";";
						db::getInstance()->query($sqlUpd);
					} else {
						$error[] = 'Невозможно создать директорию';
					}
				}
			} else {
				$error[] = 'Неправильный тип загружаемого файла';
			}
		} else {
			$error[] = 'Пользователь '.$username.' не найден!';
		}
	}
}