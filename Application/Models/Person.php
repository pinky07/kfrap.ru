<?php
/**
 * Created by JetBrains PhpStorm.
 * User: pinky
 * Date: 26.02.12
 * Time: 22:11
 * To change this template use File | Settings | File Templates.
 */
namespace Application\Models;
use \Framework\Model as Model;

class Person extends \ActiveRecord\Model
{
    static $belongs_to = array(array('division', 'class_name' => 'Application\Models\Division'));

    public function set_photo_link($picture)
    {
        if(!empty($picture))
        {
            $this->assign_attribute('photo_link', $picture);
        }
    }
}
