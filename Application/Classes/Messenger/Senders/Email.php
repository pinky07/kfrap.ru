<?php
/**
 * Created by JetBrains PhpStorm.
 * User: pinky
 * Date: 27.05.12
 * Time: 21:19
 * To change this template use File | Settings | File Templates.
 */
namespace Application\Classes\Messenger\Senders;

class Email extends \Application\Classes\Messenger\Sender implements ISender
{
    const DEFAULT_SUBJECT = '';
    const FROM            = '';

    public function send()
    {
        $message = \Swift_Message::newInstance(
            !empty($this->params['subject'])?$this->params['subject']:self::DEFAULT_SUBJECT,
            $this -> messageBody
        );

        $message -> setFrom(array(
            'site@kfrap.ru' => 'Оффициальный сайт казанского филиала академии правосудия'));
        $message -> setTo(array($this->sendTo));


        if(!\Framework\Framework::app()->getMailer()->send($message))
        {
            return false;
        }

        return true;
    }
}
