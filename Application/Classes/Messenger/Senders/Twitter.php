<?php
/**
 * Created by JetBrains PhpStorm.
 * User: pinky
 * Date: 27.05.12
 * Time: 22:51
 * To change this template use File | Settings | File Templates.
 */
namespace Application\Classes\Messenger\Senders;

class Twitter extends \Application\Classes\Messenger\Sender implements ISender
{
    public function send()
    {
        \Application\Classes\TwitterClient::getInstance()->updateStatus("#{$this->sendTo} {$this->messageBody}");

        return true;
    }
}
