<?php
/**
 * Created by JetBrains PhpStorm.
 * User: pinky
 * Date: 27.05.12
 * Time: 21:19
 * To change this template use File | Settings | File Templates.
 */
namespace Application\Classes\Messenger\Senders;

interface ISender
{
    public function send();
}
