<?php
/**
 * Created by JetBrains PhpStorm.
 * User: pinky
 * Date: 26.05.12
 * Time: 20:16
 * To change this template use File | Settings | File Templates.
 */
namespace Application\Classes\Messenger;

class Message extends \Framework\Foundation
{
    private $sender;
    private $template;
    private $body = array();
    private $to = array();

    const EMAIL     = 'email';
    const FEEDBACK  = 'feedback';
    const TWITTER   = 'twitter';

    const EMAIL_DEFAULT_TEMPLATE    = 1;
    const FEEDBACK_DEFAULT_TEMPLATE = 2;
    const TWITTER_DEFAULT_TEMPLATE  = 3;

    /**
     * @param $sender
     * @param array $sendTo
     * @param array $data
     */
    public function __construct($sender, array $data = array(), $sendTo = array())
    {
        $this -> sender = $sender;
        $this -> setTo($sendTo);
        $this -> setBody($data);

        switch($sender)
        {
            case self::EMAIL :
                $this->template = self::EMAIL_DEFAULT_TEMPLATE;
                break;
            case self::FEEDBACK:
                $this->template = self::FEEDBACK_DEFAULT_TEMPLATE;
                break;
            case self::TWITTER :
                $this->template = self::TWITTER_DEFAULT_TEMPLATE;
        }
    }

    /**
     * @param $messageType
     */
    public function setTemplate($messageType)
    {
        $this->template = $messageType;
    }

    /**
     * @param array $data
     */
    public function setBody(array $data)
    {
        $this->body = $data;
    }

    /**
     * @param $sendTo
     */
    public function setTo($sendTo)
    {
        if(is_array($sendTo))
        {
            foreach($sendTo as $to)
            {
                $this->setTo($to);
            }
        }
        else
        {
            array_push($this->to, $sendTo);
        }
    }

    /**
     * @return mixed
     */
    public function getSender()
    {
        return $this->sender;
    }

    /**
     * @return mixed
     */
    public function getTo()
    {
        return $this->to;
    }

    /**
     * @return mixed
     */
    public function getBody()
    {
        return $this->body;
    }

    /**
     * @return int
     */
    public function getTemplate()
    {
        return $this->template;
    }
}
