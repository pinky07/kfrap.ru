<?php
/**
 * Created by JetBrains PhpStorm.
 * User: pinky
 * Date: 27.05.12
 * Time: 20:56
 * To change this template use File | Settings | File Templates.
 */
namespace Application\Classes\Messenger;

abstract class Sender
{
    protected $messageTemplate;
    protected $params;
    protected $sendTo;
    protected $messageBody;
    protected $error;

    const STATUS_SUCCESS    = 'success';
    const STATUS_ERROR      = 'error';


    public function __construct(\Application\Models\SendLog $log)
    {
        $this -> messageTemplate = $log->message_template->body;
        $this -> params          = unserialize($log->data);
        $this -> sendTo          = $log->send_to;
    }

    public function formattedMessage()
    {
        $params = array();
        $values = array();

        foreach($this -> params as $param => $value)
        {
            $params[] = "{{$param}}";
            $values[] = $value;
        }

        $this->messageBody = str_replace($params, $values, $this->messageTemplate);
    }
}
