<?php
/**
 * Created by JetBrains PhpStorm.
 * User: pinky
 * Date: 27.05.12
 * Time: 21:26
 * To change this template use File | Settings | File Templates.
 */
namespace Application\Classes\Messenger;

class Messenger
{
    private static $instance;
    private $forSend    = array();

    private function __construct() {}

    /**
     * @static
     * @return Messenger
     */
    public static function getInstance()
    {
        if(!self::$instance instanceof Sender)
            self::$instance = new Messenger();

        return self::$instance;
    }

    public function run()
    {
        $this->loadLogs();

        foreach($this->forSend as $log)
        {
            $sender = self::createSender($log);

            if($sender instanceof \Application\Classes\Messenger\Senders\ISender)
            {
                $sender->formattedMessage();

                if($sender->send())
                {
                    $log->status = Sender::STATUS_SUCCESS;
                }
                else
                {
                    $log->status = Sender::STATUS_ERROR;
                }

                $log->save();
            }
        }
    }

    public function loadLogs()
    {
        $this->forSend = \Framework\Model::ActiveRecord('SendLog')->all(
                    array('conditions' => array("status = ? AND actual_send_date < ?", 'waiting', time()))
                );
    }

    /**
     * @static
     * @param \Application\Models\SendLog $log
     * @return \Application\Classes\Messenger\Sender
     */
    private static function createSender(\Application\Models\SendLog $log)
    {
        $senderClassName = sprintf('\Application\Classes\Messenger\Senders\%s', ucfirst($log->sender));


        if(class_exists($senderClassName))
        {
            return new $senderClassName($log);
        }

        return false;
    }
}
