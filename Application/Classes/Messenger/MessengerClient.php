<?php
/**
 * Created by JetBrains PhpStorm.
 * User: pinky
 * Date: 26.05.12
 * Time: 20:10
 * To change this template use File | Settings | File Templates.
 */
namespace Application\Classes\Messenger;

class MessengerClient
{
    public static function createMessage($messageType, array $data = array(), $sendTo = array())
    {
        return new Message($messageType, $data, $sendTo);
    }

    public static function send(Message $message, $send_date = null)
    {
        if(!is_numeric($send_date))
        {
            $send_date = strtotime($send_date);
        }

        $sendTo = $message->getTo();

        if(empty($sendTo))
        {
            return false;
        }

        if(is_array($sendTo))
        {
            foreach($sendTo as $to) {
                \Framework\Model::ActiveRecord('SendLog')->create(array(
                    'sender'    => $message -> getSender(),
                    'send_to'   => $to,
                    'data'      => serialize($message -> getBody()),
                    'message_template_id' => $message -> getTemplate(),
                    'actual_send_date' => ($send_date)?$send_date:time()
                ));
            }
        }
        else
        {
            \Framework\Model::ActiveRecord('SendLog')->create(array(
                'sender'    => $message -> getSender(),
                'send_to'   => $sendTo,
                'data'      => serialize($message -> getBody()),
                'message_template_id' => $message -> getTemplate(),
                'actual_send_date' => ($send_date)?$send_date:time()
            ));
        }
        return true;
    }
}
