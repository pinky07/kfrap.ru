<?php
/**
 * Created by JetBrains PhpStorm.
 * User: pinky
 * Date: 25.05.12
 * Time: 2:07
 * To change this template use File | Settings | File Templates.
 */
namespace Application\Classes;

class TwitterClient extends \Framework\Foundation
{
    /** @var TwitterClient */
    private static $_instance   = null;

    /** @var \Framework\Twitter\TwitterOAuth */
    private $oauth              = null;

    const TWITTER_KEY       = 'VVTImgbkBzgPSyeYr4eQA';
    const TWITTER_SECRET    = 'fnXFtMOEEYit0fkA0MMkwEkal4e5TBKDPSb18jfo8';
    const ACCESS_TOKEN      = '565464187-W1prb4X2ee84NpUoftrjwvWWAJHBGqvDpETd2KkM';
    const ACCESS_SECRET     = 'EgOLGK12LNy1hLbiqvxE7qqTpLuhOAGCG6UUo3E5mw';

    private function __construct()
    {
        require_once _FWK_ROOT_DIR_."/Twitter/twitteroauth.php";

        $this->oauth = new \Framework\Twitter\TwitterOAuth(self::TWITTER_KEY, self::TWITTER_SECRET, self::ACCESS_TOKEN, self::ACCESS_SECRET);
    }

    /**
     * @static
     * @return TwitterClient
     */
    public static function getInstance()
    {
        if(!self::$_instance instanceof self)
            self::$_instance = new self;

        return self::$_instance;
    }

    public function getCreditails()
    {
        $credentials = $this->oauth->get("account/verify_credentials");

        return $credentials;
    }

    public function updateStatus($message)
    {
        $this->oauth->post('statuses/update', array('status' => "$message"));
    }

    public function getStatuses($id)
    {
        $statuses = $this->oauth->get("statuses/show/$id");

        return $statuses;
    }

    public function get($uri, array $params = array())
    {
        return $this->oauth->get($uri, $params);
    }
    
    public function getMessages() 
    {
		return $this->oauth->get('statuses/user_timeline');
	}
}
