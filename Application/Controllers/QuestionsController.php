<?php
/**
 * Created by JetBrains PhpStorm.
 * User: pinky
 * Date: 23.03.12
 * Time: 0:49
 * To change this template use File | Settings | File Templates.
 */
namespace Application\Controllers;
use \Framework\Model as Model;
class QuestionsController extends FrontendController
{
    public function indexAction()
    {
        $this->_actionView->setLayout('frontend/html2');
	$questions = Model::ActiveRecord('Question')->all(array('is_answered' => 1));

	return compact('questions');
    }
}
