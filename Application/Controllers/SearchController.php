<?php
/**
 * Created by JetBrains PhpStorm.
 * User: pinky
 * Date: 23.03.12
 * Time: 0:49
 * To change this template use File | Settings | File Templates.
 */
namespace Application\Controllers;

class SearchController extends FrontendController
{
    private $entry  = 'content';
    private $type   = self::SEARCH_IN_PAGES;
    private $newsCategories = array();
    private $query = null;

    const SEARCH_IN_PAGES    = 'page';
    const SEARCH_IN_NEWS     = 'news';



    public function before()
    {
        parent::before();

        if($this->get('type') && $this->get('type') == self::SEARCH_IN_NEWS)
        {
            $this->type = 'article';

            if($this->get('categories'))
            {
                $this->newsCategories = $this->get('categories');
            }
        }
        elseif($this->get('type') == self::SEARCH_IN_PAGES)
        {
            $this->type = self::SEARCH_IN_PAGES;
        }

        if($this->get('entry') && $this->get('entry') == 'title')
        {
            $this->entry = 'title';
        }
    }

    public function indexAction()
    {
        $this->_actionView->setLayout('frontend/html2');

        $newsCategories = \Framework\Model::ActiveRecord('Category')->all();

        if($this->get('q'))
        {
            $this->query = $this->get('q');

            $conditions = array('conditions' => array("{$this->entry} LIKE '%{$this->query}%'"));

            if($this->type == self::SEARCH_IN_NEWS && !empty($this->newsCategories))
            {
                $conditions['conditions'][0] .= ' AND category_id in (?)';
                $conditions['conditions'][] = $this->newsCategories;
            }

            $results = array();
            foreach(\Framework\Model::ActiveRecord(ucfirst($this->type))->find('all', $conditions) as $element)
            {
                $context = \Framework\String::truncate(mb_stristr(strip_tags($element->content), $this->query, null, \Framework\Framework::app()->encoding), 200);

                $context = str_ireplace($this->query, "<b>{$this->query}</b>", $context);

                $results[] = array(
                    'id'        => $element->id,
                    'title'     => $element->title,
                    'context'   => $context,
                    'type'      => ($this->get('type') == self::SEARCH_IN_NEWS)?"news":"page"
                );
            }

            $query = $this->query;

            return compact('results', 'newsCategories', 'query');
        }

        return compact('newsCategories');
    }
}