<?php
/**
 * Created by JetBrains PhpStorm.
 * User: PC
 * Date: 04.03.12
 * Time: 12:36
 * To change this template use File | Settings | File Templates.
 */
namespace Application\Controllers;
use \Framework\Model as Model;

class FrontendController extends ApplicationController
{
    protected $_protect         = false; #Доступ есть у всех

    public    $showPageMenu     = true;
    public    $showBlockPeople  = true;
    public    $showBlockNews    = true;
    public    $showDocuments    = true;
    public    $mainMenu         = array();
    public    $pageMenu         = array();
    public    $blockNews        = array();
    public    $blockPeople      = array();
    public    $blockDocuments   = array();
    protected $_pageMenuId      = null;
    protected $_tags            = array();

    protected $_viewsDir= 'frontend';

    public function beforeRender()
    {
        parent::beforeRender();

        $this->_loadMenu();

        if($this->showPageMenu)
        {
            $this->_loadPageMenu();
        }

        if($this->showBlockPeople)
        {
            $this->_loadBlockPeople();
        }

        if($this->showBlockNews)
        {
            $this->_loadBlockNews();
        }
    }

    private function _loadMenu()
    {
        $this->mainMenu = Model::ActiveRecord('Menu')->find(1)->getAsMainMenu();
    }

    protected function _setTags($tags)
    {

        if(is_array($tags))
        {
            $this->_tags = $tags;
        } else
        {
            if(empty($tags))
            {
                $this->_tags = array();

            } else
            {
                $this->_tags = explode(',', '$tags');

                foreach($this->_tags as $k=> $v)
                {
                    $this->_tags[$k] =  '(^|[,])[ ]*'.$tags.'[ ]*([,]|$)';
                }
            }
        }
    }

    private function _loadBlockPeople()
    {
        if(!empty($this->_tags))
        {
            $this->blockPeople = Model::ActiveRecord('Person')->find('all', array('conditions' => "tags REGEXP '".implode('|', $this->_tags)."'"));

            if(empty($this->blockPeople))
                $this->showBlockPeople = false;
        } else
        {
            $this->showBlockPeople = false;
        }

    }

    private function _loadBlockNews()
    {

    }

    private function _loadPageMenu()
    {
        if(!empty($this->_pageMenuId))
        {
            if(!is_array($this->_pageMenuId) and Model::ActiveRecord('Menu')->exists($this->_pageMenuId))
            {
                $this->pageMenu = Model::ActiveRecord('Menu')->find($this->_pageMenuId);
                return true;
            }
            elseif (is_array($this->_pageMenuId))
            {

                foreach($this->_pageMenuId as $pMenuId)
                {
                    if((int)$pMenuId > 0 and Model::ActiveRecord('Menu')->exists($pMenuId))
                    {
                        $this->pageMenu[] = Model::ActiveRecord('Menu')->find($pMenuId);
                    }
                }
                if(!empty($this->pageMenu))
                {
                    return true;
                }
            }
        }

        $this->showPageMenu = false;
        return false;
    }
}
