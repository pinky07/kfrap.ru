<?php
/**
 * Created by JetBrains PhpStorm.
 * User: pinky
 * Date: 25.05.12
 * Time: 1:13
 * To change this template use File | Settings | File Templates.
 */
namespace Application\Controllers\Api;
use Application\Classes\TwitterClient;

class TwitterController extends ApiController
{
    /** @var TwitterClient */
    private $client = null;

    protected $withoutAuth = array('index');

    public function before()
    {
        $this->client = TwitterClient::getInstance();

        parent::before();
    }

    public function indexAction()
    {
		
        foreach($this->client->getMessages() as $message) 
        {
			if(strpos($message->text, '#kfrap') !== false)
				continue;
				
			$twits[] = array(
				'date'	=> date('d.m.Y H:i:s', strtotime($message->created_at)),
				'text'	=> $message->text,
				'user_pic' => $message->user->profile_image_url
			);
		}
		
		$this->getResponse()->ajax($twits);
		
    }
}
