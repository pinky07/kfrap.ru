<?php
/**
 * Created by JetBrains PhpStorm.
 * User: pinky
 * Date: 12.02.12
 * Time: 19:50
 * To change this template use File | Settings | File Templates.
 */
namespace Application\Controllers;
use Application\Models\Role;
use Application\Models\User;
use Framework\Helpers\FormHelper as FormHelper;

class ApplicationController extends \Framework\ActionController
{
    /**
     * Список групп пользователей, у которых есть доступ к конкретному контроллеру
     * @var array
     */
    protected   $_access        = array(Role::ROOT);

    /**
     * Флаг проверки прав доступа к контроллеру
     * @var bool
     */
    protected   $_protect       = true;


    private     $_imageUploader = null;

    /**
     * Флаг определяющий показывать или нет цепочку навигации
     * @var bool
     */
    public      $showBreadcrumb = true;

    /**
     * Массив элементов цепочки навигации
     * @var array
     */
    public      $breadcrumbs = array();

    /**
     * Текущий макет страницы
     * @var string
     */
    protected  $_layout = 'frontend/html';

    /**
     *
     */
    protected function __onLoad()
    {
        $this   ->set('formHelper', FormHelper::getInstance());
    }

    /**
     *
     */
    public function before()
    {
        if($this->_protect)
        {
            if(!$this->isAllowed())
            {
                exit('Нет прав доступа');
            }
        }
    }

    /**
     * Проверяет права пользователя на доступность контроллера, у пользователя
     * состоящего в группе ROOT есть доступ к любому контроллеру
     * @return bool
     */
    final protected function isAllowed()
    {
        return (in_array($this->getUser()->role_id, $this->_access) || $this->getUser()->role_id == Role::ROOT);
    }

    /**
     * Возвращает текущего пользователя
     * @return \Application\Models\User
     */
    public function getUser()
    {
        return User::getCurrent();
    }

    /**
     * Добавить группе доступ к контроллеру
     * @param $role
     * @return ApplicationController
     */
    protected function addAccess($role)
    {
        array_push($this->_access, (int)$role);

        return $this;
    }

    /**
     * Устанавливает список групп у которых есть доступ
     * к текущему контроллеру и дочернему
     * @param array $access
     */
    protected function setAccess(array $access)
    {
        $this->_access = $access;
    }

    public function getUploader($uploader)
    {
        if($uploader == 'image' or $uploader == 'file')
        {
            if($this->{'_'.$uploader.'Uploader'} == null)
                $this->{'_'.$uploader.'Uploader'} = \Framework\Uploaders\Uploader::factory($uploader);

        } else {

            return false;
        }

        return $this->{'_'.$uploader.'Uploader'};
    }

    /**
     * @return \Framework\Uploaders\ImageUploader
     */
    public function getImageUploader()
    {
        return $this->getUploader('image');
    }
}
