<?php
class Controller_Frontend_News extends Controller_Frontend_Controller {
	
	//устанавливаем название компонента (контроллера)
	static public $name = '';
	
	function index() {
	
		$system = Controller_Main::getInstance();
							
		$view = View::factory('frontend/news', array());
		
		$result = $view -> render();
		
		$system -> setBody($result);	
	
	}
	
	function view() {
	
		$system = Controller_Main::getInstance();
		
		$params = $system->getParams();
		
		if(isset($params['id'])) {
			
			$articleObj = Model::factory('Article');
			
			$menuObj = Model::factory('Menu');
		
			$menuArr = $menuObj->getAll(array('active' => 1, 'parent' => 0));
			
			foreach($menuArr as $key => $value) {
			$menu[$key]['id']		= $value['id'];
			$menu[$key]['name']		= $value['name'];
			$menu[$key]['url']		= $value['url'];
			$parentsArr = $menuObj->getAll(array('parent' => $value['id']));
			
			if(!empty($parentsArr)) {
				foreach($parentsArr as $keyParent => $val) {
					$menu[$key]['parents'][$keyParent]['id']	= $val['id'];
					$menu[$key]['parents'][$keyParent]['name']	= $val['name'];
					$menu[$key]['parents'][$keyParent]['url']	= $val['url'];
				}
				}
			}
			
			array_unshift($menu, array('id'=>0, 'name'=> '<img style="padding:5px" src="'.SITE_URL.'/views/frontend/kfrap/images/ic_menu_home.png">', 'url'=>SITE_URL));
			
			$arrResult['menu']	= $menu;
			
			$id = (int)$params['id'];
			
			$article = $articleObj->getAll(array('id'=>$id));
			
			$arrResult['article']	= $article['0'];
			
			$view = View::factory('frontend/news', $arrResult);
			
			$result = $view -> render();
			
			$system -> setBody($result);	
		} else {
		
		}
	
	}	
}
?>