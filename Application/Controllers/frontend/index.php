<?php
class Controller_Frontend_Index extends Controller {

	function index() {
		$system = Controller_Main::getInstance();
		
		$menuObj = Model::factory('Menu');
		
		$articleObj = Model::factory('Article');
		
		$categoryObj =  Model::factory('Category');
		
		$menuArr = $menuObj->getAll(array('active' => 1, 'parent' => 0));
		
		$articleArr = $articleObj->getAll(array('status' => 1, 'category' => 26));
		
		$categoryArr = $categoryObj->getAccessCategories();
		
		if(!empty($categoryArr)) {
			foreach($categoryArr as $cat) {
				if($cat['homepage'] == 1) {
					$categories[] = $cat;
				}
			}
		}
		
		if(!empty($articleArr)) {
			foreach($articleArr as $value) {
				if($value['slider'] == 1) {
					$articlesToSlide[] = $value;
				}
			}
			$arrResult['articles']	= $articleArr;
			$arrResult['toSlide'] = $articlesToSlide;
		}
		foreach($menuArr as $key => $value) {
			$menu[$key]['id']		= $value['id'];
			$menu[$key]['name']		= $value['name'];
			$menu[$key]['url']		= $value['url'];
			$parentsArr = $menuObj->getAll(array('parent' => $value['id']));
			
			if(!empty($parentsArr)) {
				foreach($parentsArr as $keyParent => $val) {
					$menu[$key]['parents'][$keyParent]['id']	= $val['id'];
					$menu[$key]['parents'][$keyParent]['name']	= $val['name'];
					$menu[$key]['parents'][$keyParent]['url']	= $val['url'];
				}
			}
		}
		
		array_unshift($menu, array('id'=>0, 'name'=> '<img style="padding:5px" src="'.SITE_URL.'/views/frontend/kfrap/images/ic_menu_home.png">', 'url'=>'index'));
		
		$arrResult['menu']	= $menu;
		
		if(isset($categories))
			$arrResult['categories']	= $categories;
			
		$view = View::factory('frontend/index', $arrResult);
		$result = $view->render();
		$system ->setBody($result);	
	}
	
	function ajax() {
		
		$system = Controller_Main::getInstance();
		
		$params = $system->getParams();
		
		$path = 'views'.DIRECTORY_SEPARATOR.'frontend'.DIRECTORY_SEPARATOR.BACKEND_TEMPLATE.DIRECTORY_SEPARATOR.'ajax'.DIRECTORY_SEPARATOR;
		
		if(isset($params['getNewsCat'])) {
		
			$articleObj = Model::factory('Article');
			
			$cat = (int)$params['getNewsCat'];
			
			$articleArr = $articleObj->getAll(array('status' => 1, 'category' => $cat));
			
			$filePath = $path.'news_block.php';
			
			$view = View::factory('frontend/ajax', array('path' => $filePath, 'articles' => $articleArr));
			
			$result = $view->render();
			
			$system ->setBody($result);	
		}
	}

}
?>