<?php
/**
 * Created by JetBrains PhpStorm.
 * User: PC
 * Date: 03.03.12
 * Time: 22:23
 * To change this template use File | Settings | File Templates.
 */
namespace Application\Controllers;
use Framework\Model as Model;

class PersonController extends FrontendController
{
    public $breadcrumbs = array('Персоналии' => '/person/all');

    private $letters = array(
                            'А', 'Б', 'В', 'Г', 'Д', 'Е', 'Ж',
                            'З', 'И', 'К', 'Л', 'М', 'Н', 'О', 'П',
                            'Р', 'С', 'Т', 'У', 'Ф', 'Х', 'Ц', 'Ч',
                            'Ш', 'Щ', 'Э', 'Ю', 'Я');

    public function indexAction()
    {
        $this->redirect('/person/all');
    }

    public function viewAction()
    {
        if(Model::ActiveRecord('Person')->exists($this->params('id')))
        {
            $personObject = Model::ActiveRecord('Person')->find($this->params('id'));

            $this->breadcrumbs[$personObject->last_name.' '.$personObject->first_name] = '';

            return compact('personObject');
        } else
        {
            $this->redirectTo(array('controller' => 'index'));
        }
    }

    public function allAction()
    {
        $this->_actionView->setLayout('frontend/html2');

        $letters = $this->letters;

        $people     = Model::ActiveRecord('Person');

        $divisions      = array();
        $departments    = array();

        foreach(Model::ActiveRecord('Division')->all(array(
            'order' => 'name asc')) as $division)
        {
            if(preg_match('/^Кафедра/', $division->name))
            {

                $departments[] = $division;
            } else
            {
                $divisions[] = $division;
            }
        }

        $result = \Framework\Paginator::paginate($people, array(
            'conditions' => array(
                'publish = ?', 1),
            'order' => 'last_name asc'), 5);

        $peopleArray    = $result['elements'];
        $pages          =   $result['pages'];

        if($this->getRequest()->isXmlHttpRequest())
        {
            $persons        = array();
            $suggestions    = array();

            foreach($result['elements'] as $person)
            {
                $persons[] = array(
                                    'id'            => $person->id,
                                    'photo'         => !empty($person->photo_link)?"/upload/people/{$person->photo_link}":'/images/default-person.jpg',
                                    'last_name'     => $person->last_name,
                                    'first_name'    => $person->first_name,
                                    'middle_name'   => $person->middle_name,
                                    'division'      => (isset($person->division->name)?$person->division->name:''),
                );

                $suggestions[] = $person->last_name.' '.$person->first_name;
            }
            $suggestions[] = 'qwerty';

            $this->getResponse()->ajax(array('persons' => $persons, 'pages'=> $result['pages'], 'data'=> $suggestions,'suggestions' => $suggestions));
        }


        return compact('letters', 'peopleArray', 'pages', 'divisions', 'departments');
    }

    public function searchAction()
    {
        if($this->get('find_by') == 'query')
        {
            $params = array(
                'conditions' => array(
                    "publish = 1 and last_name LIKE '%{$this->get('query')}%' or first_name LIKE '%{$this->get('query')}%'"));
            $query  = $this->get('query');
        }
        elseif($this->get('find_by') == 'letter')
        {
            if(!in_array($this->get('letter'), $this->letters))
                exit;

            $params = array(
                'conditions' => "publish = 1 and last_name REGEXP '^".$this->get('letter')."'");
            $query  = $this->get('letter');
        }
        elseif($this->get('find_by') == 'division')
        {
            $params = array(
                'conditions' => array("publish = 1 and division_id = ?", (int)$this->get('division'))
            );
            $query  = $this->get('division');
        }
        else {
            return exit;
        }
        $params['order'] = 'last_name asc';

        $result = \Framework\Paginator::paginate(Model::ActiveRecord('Person'), $params, 5);

        $persons        = array();
        $suggestions    = array();


        foreach($result['elements'] as $person)
        {
            $persons[] = array(
                                'id'            => $person->id,
                                'photo'         => !empty($person->photo_link)?"/upload/people/{$person->photo_link}":'/images/default-person.jpg',
                                'last_name'     => $person->last_name,
                                'first_name'    => $person->first_name,
                                'middle_name'   => $person->middle_name,
                                'division'      => (isset($person->division->name)?$person->division->name:''),
            );

            $suggestions[] = $person->last_name.' '.$person->first_name;
        }
        $suggestions[] = 'qwerty';

        $this->getResponse()->ajax(array('persons' => $persons, 'pages'=> $result['pages'], 'data'=> $suggestions,'suggestions' => $suggestions, 'query' => $query));
    }

    public function activateAction()
    {
        $this->_actionView->setLayout('frontend/html2');

        /** @var $person \Application\Models\Person */
        $person = Model::ActiveRecord('Person');

        /** @var $personForm \Application\Models\Forms\UserActivateForm */
        $userActivateForm = Model::Form('UserActivateForm');

        if($this->isPost())
        {
            if($userActivateForm->is_invalid())
            {
                $this->noticeTo(
                    array(
                        'type'      => 'error',
                        'message'   => $personForm -> getErrors())
                );
            }

            $userActivateForm->activate();
        }
    }
}
