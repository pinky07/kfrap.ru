<?php
/**
 * Created by JetBrains PhpStorm.
 * User: PC
 * Date: 10.02.12
 * Time: 23:17
 * To change this template use File | Settings | File Templates.
 */
namespace Application\Controllers;
use Framework\Model as Model;
use Application\Classes\Messenger\MessengerClient;
use Application\Classes\Messenger\Message;

class IndexController extends FrontendController
{
    public $showBreadcrumb = false;

    public function indexAction()
    {
        $articles   = Model::ActiveRecord('Article')->getPublished(array('limit' => 8));

        $categories = Model::ActiveRecord('Category')->all(array('conditions' => array('active = ? and at_home = ?', 1, 1 )));

        $forSlider = array();

        foreach($articles as $article)
        {
            if($article->slider == 1)
                $forSlider[] = $article;
        }

        $this->_pageMenuId = array(8,9); // 7 и 8 это id меню которые будут на главной

        return compact('articles', 'forSlider', 'categories');
    }

    public function getNewsByCategoryAction()
    {
        if($this->post('cat_id'))
        {
            $articles = array();

            foreach(Model :: ActiveRecord('Article')
                          -> getPublished(array(
                                'category'   => ($this->post('cat_id') == 'all') ? null : $this->post('cat_id'),
                                'limit'         => 8))
                    as $article)
            {

                $articles[] = array('id'        => $article->id,
                                    'image'     => ($article->getImages(true))?$article->getImages(true):(($article->slider_picture)?"/upload/images/{$article->slider_picture}":''),
                                    'title'     => $article->title,
                                    'preview'   => $article->preview);
            }

            $this->getResponse()->ajax($articles);
        }
    }

    public function getDivisionsAction()
    {
        if(!$this->isPost())
        {
            exit;
        }

        $divisions = array();

        foreach(Model::ActiveRecord('Division')->all(array('order' => 'name asc')) as $division)
        {
            $divisions[] = array(
                'id'    => $division->id,
                'name'  => $division->name
            );
        }

        $this->getResponse()->ajax($divisions);
    }

    public function addQuestionAction()
    {
        if(!$this->isPost() || !$this->post('question_params'))
        {
            exit;
        }

        $newQuestion = Model::ActiveRecord('Question')->create($this->post('question_params'));

        if($newQuestion->is_valid())
        {
            $newQuestion->save();

            exit(json_encode(array('status' => 'success')));
        }

        $this->getResponse()->ajax(array('status' => 'error', 'errors' => $newQuestion->errors->full_messages()));
    }
}

