<?php
/**
 * Created by JetBrains PhpStorm.
 * User: pinky
 * Date: 26.02.12
 * Time: 20:10
 * To change this template use File | Settings | File Templates.
 */
namespace Application\Controllers\Backend;
use \Framework\Model as Model;

class PersonController extends BackendController
{

    public function indexAction()
    {
        $result = \Framework\Paginator::paginate(Model::ActiveRecord('Person'), array('order' => 'last_name asc'), 10);

        return $result;
    }

    /**
     * @return array
     */
    public function addAction()
    {
        /** @var $person \Application\Models\Person */
        $person = Model::ActiveRecord('Person');

        /** @var $personForm \Application\Models\Forms\PersonForm */
        $personForm = Model::Form('PersonForm');

        if($this->isPost())
        {
            if($personForm->is_invalid())
            {
                $this->noticeTo(
                    array(
                        'type'      => 'error',
                        'message'   => $personForm -> getErrors())
                );
            }

            if($person -> saveForm($personForm))
            {
                $this->noticeTo(
                    array(
                        'type'      => 'success',
                        'message'   => 'Персона сохранена'),
                    array(
                        'controller'=> 'admin\person'
                    )
                );
            }
        }

        foreach(Model::ActiveRecord('Division')->find('all') as $division)
        {
            $divisions[$division->id] = $division->name;
        }

        return compact('divisions');
    }

    /**
     * @return array
     */
    public function editAction()
    {
        if(!Model::ActiveRecord('Person')->exists($this->params('id')))
        {
            $this->noticeTo(
                array(
                    'type'      => 'error',
                    'message'   => 'Персона не найдена'),
                array(
                    'controller'=> 'admin\person'
                )
            );
        }

        /** @var $person \Application\Models\Person */
        $person = Model::ActiveRecord('Person')->find($this->params('id'));

        /** @var $personForm \Application\Models\Forms\PersonForm */
        $personForm = Model::Form('PersonForm');

        $personForm->personId = $this->params('id');

        if($this->isPost())
        {
            if($personForm->is_invalid())
            {
                $this->noticeTo(
                    array(
                        'type'      => 'error',
                        'message'   => $personForm -> getErrors())
                );
            }

            if($person -> saveForm($personForm))
            {
                $this->noticeTo(
                    array(
                        'type'      => 'success',
                        'message'   => 'Персона сохранена'),
                    array(
                        'controller'=> 'admin\person'
                    )
                );
            }
        }

        foreach(Model::ActiveRecord('Division')->find('all') as $division)
        {
            $divisions[$division->id] = $division->name;
        }

        return compact('person', 'divisions');
    }
}
