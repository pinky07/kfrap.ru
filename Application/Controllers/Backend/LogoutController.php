<?php
/**
 * Created by JetBrains PhpStorm.
 * User: PC
 * Date: 12.02.12
 * Time: 0:18
 * To change this template use File | Settings | File Templates.
 */
namespace Application\Controllers\Backend;
use Application\Models\User as User;

class LogoutController extends BackendController
{
    function indexAction()
    {
        User::logout();
        $this->redirectTo(array('controller' => 'admin\auth'));
    }
}
