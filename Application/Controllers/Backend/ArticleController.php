<?php
/**
 * Created by JetBrains PhpStorm.
 * User: PC
 * Date: 11.02.12
 * Time: 23:56
 * To change this template use File | Settings | File Templates.
 */
namespace Application\Controllers\Backend;
use \Framework\Model as Model;
use \Framework\Uploaders\Uploader as Uploader;

class ArticleController extends BackendController
{
    //Права на раздел
    protected $_access        = array(
        \Application\Models\Role::ADMINISTRATOR,
        \Application\Models\Role::EDITOR,
    );

    /**
     * @return array
     */
    public  function indexAction()
    {
        $categories = Model::ActiveRecord('Category')->all();

        return compact('categories');
    }

    /**
     * Редактирование материала
     * @return array
     */
    public function editAction()
    {
        // Если материал с текущим id не существует, то редирект с ошибкой
        if(!Model::ActiveRecord('Article')->exists($this->params('id')))
        {
            $this->noticeTo(
                array(
                    'type'      => 'error',
                    'message'   => 'Материал не найден'),
                array(
                    'controller' => 'backend\article')
            );
        }

        /** @var $article \Application\Models\Article */
        $article = Model::ActiveRecord('Article')->find($this->params('id'));

        /** @var $articleForm \Application\Models\Forms\ArticleForm */
        $articleForm = Model::Form('ArticleForm');


        if($this->isPost())
        {
            if($articleForm->is_invalid())
            {
                $this->noticeTo(
                    array(
                        'type'      => 'error',
                        'message'   => $articleForm->getErrors())
                );
            }

            if($article->saveForm($articleForm))
            {
                $this->noticeTo(
                    array(
                        'type'      => 'success',
                        'message'   => 'Материал успешно сохранен'),
                    array(
                        'controller'=> 'backend\category',
                        'action'    => 'view',
                        'params'    => array(
                            'id' => $article->category_id)
                    )
                );
            }

        }

        foreach(Model::ActiveRecord('Category')->all() as $category)
        {
            $categories[$category->id] = $category->title;
        }

        foreach(Model::ActiveRecord('Document')->all() as $document)
        {
            $documents[$document->id] = $document->name;
        }

        return compact('article', 'categories', 'documents');
    }

    /**
     * @return array
     */
    public function addAction()
    {
        if($this->isPost())
        {
            /** @var $article \Application\Models\Article */
            $article = Model::ActiveRecord('Article');

            /** @var $articleForm \Application\Models\Forms\ArticleForm */
            $articleForm = Model::Form('ArticleForm');

            if($articleForm->is_invalid())
            {
                $this->noticeTo(
                    array(
                        'type'      => 'error',
                        'message'   => $articleForm->getErrors())
                );
            }

            if($article->saveForm($articleForm))
            {
                $this->noticeTo(
                    array(
                        'type'      => 'success',
                        'message'   => 'Материал успешно сохранен'),
                    array(
                        'controller'=> 'backend\category',
                        'action'    => 'view',
                        'params'    => array(
                            'id' => $article -> category_id)
                    )
                );
            }
        }

        foreach(Model::ActiveRecord('Category')->all() as $category)
        {
            $categories[$category->id] = $category->title;
        }

        foreach(Model::ActiveRecord('Document')->all() as $document)
        {
            $documents[$document->id] = $document->name;
        }

        return compact('categories', 'documents');
    }

    public function activateAction()
    {
        // Если материал с текущим id не существует, то редирект с ошибкой
        if(!Model::ActiveRecord('Article')->exists($this->get('id')))
        {
            $this->noticeTo(
                array(
                    'type'      => 'error',
                    'message'   => 'Материал не найден'),
                array(
                    'controller' => 'backend\article')
            );
        }

        /** @var $article \Application\Models\Article */
        $article = Model::ActiveRecord('Article')->find($this->get('id'));
        $article -> status = 1;
        $article -> save();

        $this->noticeTo(
            array(
                'type'      => 'success',
                'message'   => sprintf('Материал "%s" включен для показа', $article -> title)),
            array(
                'controller'=> 'backend\category',
                'action'    => 'view',
                'params'    => array(
                    'id' => $article -> category_id)
            )
        );
    }

    public function deactivateAction()
    {
        // Если материал с текущим id не существует, то редирект с ошибкой
        if(!Model::ActiveRecord('Article')->exists($this->get('id')))
        {
            $this->noticeTo(
                array(
                    'type'      => 'error',
                    'message'   => 'Материал не найден'),
                array(
                    'controller' => 'backend\article')
            );
        }

        /** @var $article \Application\Models\Article */
        $article = Model::ActiveRecord('Article')->find($this->get('id'));
        $article -> status = 0;
        $article -> save();

        $this->noticeTo(
            array(
                'type'      => 'success',
                'message'   => sprintf('Материал "%s" отключен для показа', $article -> title)),
            array(
                'controller'=> 'backend\category',
                'action'    => 'view',
                'params'    => array(
                    'id' => $article -> category_id)
            )
        );
    }

    public function moveAction()
    {
        if(!Model::ActiveRecord('Category')->exists($this->get('category_id')))
        {
            $this->noticeTo(
                array(
                    'type'      => 'error',
                    'message'   => 'Раздел не найден'),
                array(
                    'controller'=> 'backend\category'
                )
            );
        }

        if(!Model::ActiveRecord('Article')->exists($this->get('article_id')))
        {
            $this->noticeTo(
                array(
                    'type'      => 'error',
                    'message'   => 'Материал не найден'),
                array(
                    'controller'=> 'backend\article'
                )
            );
        }

        /** @var $article \Application\Models\Article */
        $article = Model::ActiveRecord('Article')->find($this->get('article_id'));
        $article -> category_id = $this->get('category_id');
        $article -> save();

        $this->noticeTo(
            array(
                'type'      => 'success',
                'message'   => sprintf('Материал "%s" перемещен в раздел "%s"',
                    $article -> title,
                    $article -> category -> title)
            ),
            array(
                'controller'=> 'backend\category',
                'action'    => 'view',
                'params'    => array(
                    'id' => $article -> category_id)
            )
        );
    }

    /**
     * @return array
     */
    public function viewContentAction()
    {
        if(!$this->post('content', false))
            exit('пусто');

        $content = $this->post('content', false);


        return compact('content');
    }
}
