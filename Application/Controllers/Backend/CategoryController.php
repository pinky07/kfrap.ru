<?php
/**
 * Created by JetBrains PhpStorm.
 * User: PC
 * Date: 11.02.12
 * Time: 23:57
 * To change this template use File | Settings | File Templates.
 */
namespace Application\Controllers\Backend;
use \Framework\Model as Model;

class CategoryController extends BackendController
{
    //Права на раздел
    protected $_access        = array(
        \Application\Models\Role::ADMINISTRATOR,
        \Application\Models\Role::EDITOR,
    );

    public function indexAction()
    {
        //$this->forward('index', 'Backend\Article');
    }

    public function viewAction()
    {
        if(Model::ActiveRecord('Category')->exists($this->params('id'))) {
            $category   = Model::ActiveRecord('Category')->find($this->params('id'));
            $categories = Model::ActiveRecord('Category')->all();
            return compact('category', 'categories');
        } else {
            $this->error('Раздел не найден');
            $this->redirect('/admin/article');
        }
    }

    public function editAction()
    {
        if(Model::ActiveRecord('Category')->exists($this->params('id'))) {

            $category = Model::ActiveRecord('Category')->find($this->params('id'));

            if($this->isPost()) {
                if($this->post('title')) {

                    $category->title        = $this->post('title');
                    $category->description  = $this->post('description');

                    if($this->post('at_home'))
                        $category->at_home = 1;
                    else
                        $category->at_home = 0;

                    if($this->post('active'))
                        $category->active = 1;
                    else
                        $category->active = 0;

                    $category->save();

                    $this->success('Раздел успешно изменён');

                    $this->redirect('/admin/article');

                } else {
                    $this->error('Заголовок не может быть пустым');
                }
            }


            return compact('category');
        } else {
            $this->error('Раздел с id '.$this->params('id').' не существует');
        }

        $this->redirect('/admin/article');
    }

    public function addAction()
    {
        if($this->isPost()) {
            if($this->post('title')) {
                $category = Model::ActiveRecord('Category');

                $category->title        = $this->post('title');

                $category->description  = $this->post('description');

                if($this->post('at_home'))
                    $category->at_home = 1;
                else
                    $category->at_home = 0;

                if($this->post('active'))
                    $category->active = 1;
                else
                    $category->active = 0;

                $category->save();

                $this->success('Раздел успешно создан');

                $this->redirect('/admin/article');

            } else {
                $this->error('Заголовок не может быть пустым');
            }
        }
    }
}
