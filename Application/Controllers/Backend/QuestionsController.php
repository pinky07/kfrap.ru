<?php
/**
 * Created by JetBrains PhpStorm.
 * User: pinky
 * Date: 30.04.12
 * Time: 11:53
 * To change this template use File | Settings | File Templates.
 */
namespace Application\Controllers\Backend;
use \Framework\Model as Model;

class QuestionsController extends BackendController
{
    public function indexAction()
    {
        $result = \Framework\Paginator::paginate(Model::ActiveRecord('Question'), array('order' => 'is_answered ASC, date DESC'), 10);

        return $result;
    }

    public function answerQuestionAction()
    {
        if(!Model::ActiveRecord('Question')->exists($this->get('question_id')))
        {
            $ajaxAnswer = array('status' => 'error', 'message' => 'Вопрос не найден');
        }
        else
        {
            /** @var $question \Application\Models\Question */
            $question = Model::ActiveRecord('Question')->find($this->get('question_id'));

            /** @var $answer \Application\Models\Answer */
            $answer = Model::ActiveRecord('Answer');

            /** @var $answeredForm \Application\Models\Forms\QuestionAnsweredForm */
            $answeredForm = Model::Form('QuestionAnsweredForm');

            if($question->is_answered)
            {
                $ajaxAnswer =  array('status' => 'error', 'message' => 'Вопрос уже отвечен');
            } else
            {
                if($answeredForm -> is_invalid())
                {
                    $ajaxAnswer = array('status' => 'error', 'message' => implode("\n", $answeredForm -> getErrors()));
                }
                else
                {
                    $answer -> question_id = $this->get('question_id');
                    $answer -> saveForm($answeredForm);
                    $question -> is_answered = 1;
                    $question -> answer_id = $answer->id;
                    $question -> save();

                    $ajaxAnswer = array('status' => 'success', 'message' => 'Ответ сохранен', 'answerer' => $answer->answerer, 'answer_text' => $answer->answer_text);
                }
            }

        }
        exit(json_encode($ajaxAnswer));
    }
}
