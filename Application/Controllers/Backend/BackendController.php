<?php
/**
 * Created by JetBrains PhpStorm.
 * User: PC
 * Date: 11.02.12
 * Time: 23:53
 * To change this template use File | Settings | File Templates.
 */
namespace Application\Controllers\Backend;
use Application\Models\Role;

class BackendController extends \Application\Controllers\ApplicationController
{
    protected  $_layout     = 'backend/html';

    protected $_adminMenu     = array(
                                '0'	=> array(
                                    'name'	=> 'Материалы',
                                    'link'	=> 'article',
                                    'roles' => array(
                                        Role::ADMINISTRATOR,
                                        Role::EDITOR)),
                                '1'	=> array(
                                    'name'	=> 'Страницы',
                                    'link'	=> 'page',
                                    'roles' => array(
                                        Role::ADMINISTRATOR,
                                        Role::EDITOR
                                    )),
                                '2' => array(
                                    'name'	=> 'Объявления',
                                    'link'	=> 'advert',
                                    'roles' => array()),
                                '3'	=> array(
                                    'name'	=> 'Персоналии',
                                    'link'	=> 'person',
                                     'roles' => array()),
                                '4'	=> array(
                                    'name'	=> 'Документы',
                                    'link'	=> 'document',
                                    'roles' => array(
                                        Role::ADMINISTRATOR,
                                        Role::EDITOR
                                    )),
                                '5'	=> array(
                                    'name'	=> 'Расписание',
                                    'link'	=> 'schedule',
                                    'roles' => array()),
                                '6'	=> array(
                                    'name'	=> 'Альбомы',
                                    'link'	=> 'album',
                                    'roles' => array()),
                                '7'	=> array(
                                    'name'	=> 'Меню',
                                    'link'	=> 'menu',
                                     'roles' => array()),
                                '8'	=> array(
                                    'name'	=> 'Пользователи',
                                    'link'	=> 'user',
                                    'roles' => array()),
                                '10'	=> array(
                                    'name'	=> 'Вопросы',
                                    'link'	=> 'questions',
                                    'roles' => array(
                                        Role::ADMINISTRATOR,
                                        Role::EMPLOEE
                                    )));

    public function before()
    {
        parent::before();
    }

    public function getAdminMenu()
    {
        foreach($this->_adminMenu as $k => $item)
        {
            if(!in_array($this->getUser()->role_id, $item['roles']) && $this->getUser()->role_id != Role::ROOT)
                unset($this->_adminMenu[$k]);
        }
        return $this->_adminMenu;
    }

    public function setAdminMenu($menu)
    {
        $this->_adminMenu = $menu;

        return $this;
    }

    protected function loadMenu() {
        
    }
}
