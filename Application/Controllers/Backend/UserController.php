<?php
/**
 * Created by JetBrains PhpStorm.
 * User: PC
 * Date: 12.02.12
 * Time: 1:55
 * To change this template use File | Settings | File Templates.
 */
namespace Application\Controllers\Backend;
use Framework\Model;

class UserController extends BackendController
{
    /**
     * @return array
     */
    public function indexAction()
    {
        $result = \Framework\Paginator::paginate(\Framework\Model::ActiveRecord('User'), array(), 10);

        return $result;
    }

    /**
     * @return array
     */
    public function addAction()
    {
        /** @var $user \Application\Models\User */
        $user = Model::ActiveRecord('User');

        /** @var $articleForm \Application\Models\Forms\UserForm */
        $userForm = Model::Form('UserForm');

        if($this->isPost())
        {
            if($userForm->is_invalid())
            {
                $this->noticeTo(
                    array(
                        'type'      => 'error',
                        'message'   => $userForm->getErrors())
                );
            }

            if($user->saveForm($userForm))
            {
                $this->noticeTo(
                    array(
                        'type'      => 'success',
                        'message'   => 'Пользователь успешно создан'),
                    array(
                        'controller'=> 'backend\user'
                    )
                );
            }
        }

        foreach(Model::ActiveRecord('Role')->all() as $role)
        {
            if($role->id == \Application\Models\Role::GUEST)
                continue;

            $roles[$role->id] = $role->name;
        }

        return compact('user', 'roles');
    }


    /**
     * @return array
     */
    public function editAction()
    {
        if(!Model::ActiveRecord('User')->exists($this->params('id')))
        {
            $this->noticeTo(
                array(
                    'type'      => 'error',
                    'message'   => 'Пользователь не найден'),
                array(
                    'controller'=> 'admin\user'
                )
            );
        }

        /** @var $user \Application\Models\User */
        $user = Model::ActiveRecord('User')->find($this->params('id'));

        /** @var $articleForm \Application\Models\Forms\UserForm */
        $userForm = Model::Form('UserForm');

        $userForm->userId = $this->params('id');

        if($this->isPost())
        {
            if($userForm->is_invalid())
            {
                $this->noticeTo(
                    array(
                        'type'      => 'error',
                        'message'   => $userForm->getErrors())
                );
            }

            if($user->saveForm($userForm))
            {
                $this->noticeTo(
                    array(
                        'type'      => 'success',
                        'message'   => 'Пользователь успешно сохранен'),
                    array(
                        'controller'=> 'backend\user'
                    )
                );
            }
        }

        foreach(Model::ActiveRecord('Role')->all() as $role)
        {
            if($role->id == \Application\Models\Role::GUEST)
                continue;

            $roles[$role->id] = $role->name;
        }

        return compact('user', 'roles');
    }

    public function account()
    {

    }
}
