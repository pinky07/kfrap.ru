<?php
/**
 * Created by JetBrains PhpStorm.
 * User: PC
 * Date: 11.02.12
 * Time: 23:45
 * To change this template use File | Settings | File Templates.
 */
namespace Application\Controllers\Backend;

class IndexController extends BackendController
{
    protected $_access        = array(
            \Application\Models\Role::ADMINISTRATOR,
            \Application\Models\Role::EDITOR,
            \Application\Models\Role::EMPLOEE,
        );

    function indexAction()
    {
    }
}
