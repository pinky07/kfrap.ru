<?php
/**
 * Created by JetBrains PhpStorm.
 * User: PC
 * Date: 12.02.12
 * Time: 0:17
 * To change this template use File | Settings | File Templates.
 */
namespace Application\Controllers\Backend;
use \Framework\Model as Model;

class MenuController extends BackendController
{
    public function indexAction()
    {
        $menus = Model::ActiveRecord('Menu')->all();
        return compact('menus');
    }

    public function editAction()
    {
        if(!Model::ActiveRecord('Menu')->exists($this->params('id')))
            exit;

        $menu = Model::ActiveRecord('Menu')->find($this->params('id'));

        if($this->isPost()) {
            if($this->post('name')) {
                $menu->name = $this->post('name');

                if($this->post('active'))
                    $menu->active = 1;
                else
                    $menu->active = 0;

                $menu->save();

                $this->success('Меню успешно сохранено');
            }
            else
                $this->error('Не указано название меню');

            $this->redirect('/admin/menu');
        }


        return compact('menu');
    }

    public function addAction()
    {
        if($this->isPost()) {
            if($this->post('name')) {
                $menu = Model::ActiveRecord('Menu');

                $menu->name = $this->post('name');

                if($this->post('active'))
                    $menu->active = 1;
                else
                    $menu->active = 0;

                $menu->save();

                $this->success('Меню успешно создано');

            }
            else
                $this->error('Не указано название меню');

            $this->redirect('/admin/menu');
        }
    }

    public function editItemAction()
    {
        if(!Model::ActiveRecord('MenuItem')->exists($this->params('id')))
            exit;

        $item = Model::ActiveRecord('MenuItem')->find($this->params('id'));

        if($this->isPost()) {
            if($this->post('name') and $this->post('link')) {
                $item->name             = $this->post('name');
                $item->menu_id          = $this->get('menu_id');
                $item->url              = $this->post('link');
                $item->parent_item_id   = $this->post('parent');
                $item->sort             = $this->post('sort');

                if($this->post('active'))
                    $item->active = 1;
                else
                    $item->active = 0;

                $item->save();
                $this->addNotice('success', 'Пункт меню успешно изменен');
            } else {
                if(!$this->post('name'))
                    $this->addNotice('error', 'Не указано название пункта');
                if(!$this->post('link'))
                    $this->addNotice('error', 'Необходимо указать ссылку для пункта меню');

            }
            
            $this->redirectTo(array('controller'=>'/admin/menu'));
        }

        $itemsObj = Model::ActiveRecord('MenuItem')->all(array('conditions' => array('id <> ? AND menu_id = ? AND parent_item_id = ?', $this->params('id'), $this->get('menu_id'), 0)));

        $items[0] = 'Верхний уровень';
        $menu_id  = $this->get('menu_id');
        
        foreach($itemsObj as $it) {
            $items[$it->id] = '-- '.$it->name;
        }

        return compact('items', 'menu_id', 'item');
    }

    public function addItemAction()
    {
        if(!Model::ActiveRecord('Menu')->exists($this->get('id')))
            exit;
        
        if($this->isPost()) {
            if($this->post('name') and $this->post('link')) {
                $itemObj = Model::ActiveRecord('MenuItem');
                $itemObj->name              = $this->post('name');
                $itemObj->menu_id           = $this->get('id');
                $itemObj->url               = $this->post('link');
                $itemObj->parent_item_id    = $this->post('parent');
                $itemObj->sort              = $this->post('sort');

                if($this->post('active'))
                    $itemObj->active = 1;
                else
                    $itemObj->active = 0;

                $itemObj->save();
                $this->success('Новый пункт меню успешно добавлен');
            } else {
                if(!$this->post('name'))
                    $this->error('Не указано название пункта');
                if(!$this->post('link'))
                    $this->error('Необходимо указать ссылку для пункта меню');
            }

            $this->redirect('/admin/menu');
        }
        
        $menu_id = $this->get('id');

        $itemsObj = Model::ActiveRecord('MenuItem')->all(array('conditions' => array('menu_id = ? AND parent_item_id = ?', $this->get('id'), 0)));

        $items[0] = 'Верхний уровень';

        foreach($itemsObj as $item) {
            $items[$item->id] = '-- '.$item->name;
        }
        
        return compact('items', 'menu_id');
    }

    public function getLinksAction()
    {
        if($this->post('link_type') === false)
            exit;

        if($this->post('link_type') == 1) {
            foreach(Model::ActiveRecord('Page')->all() as $page) {
                $links['/page/view/id/'.$page->id] = $page->title;
            }
        } elseif ($this->post('link_type') == 2) {
            foreach(Model::ActiveRecord('Article')->all() as $article) {
                $links['/news/view/id/'.$article->id] = $article->title;
            }
        } else {
            $links = null;
        }
        
        return compact('links');
    }

    public function viewItemsAction()
    {
        if($this->get('id')) {
            $menu_id = $this->get('id');

            $items = Model::ActiveRecord('MenuItem')->find('all', array('conditions' => array('menu_id = ? ',$this->get('id')), 'order' => 'sort asc'));

            return compact('menu_id', 'items');
        } else {
            exit;
        }
    }
}
