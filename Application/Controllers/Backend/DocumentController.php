<?php
/**
 * Created by JetBrains PhpStorm.
 * User: pinky
 * Date: 30.04.12
 * Time: 11:53
 * To change this template use File | Settings | File Templates.
 */
namespace Application\Controllers\Backend;
use Framework\Model as Model;
class DocumentController extends BackendController
{
    //Права на раздел
    protected $_access        = array(
        \Application\Models\Role::ADMINISTRATOR,
        \Application\Models\Role::EDITOR,
    );

    public function indexAction()
    {
        $documents = Model::ActiveRecord('Document')->all();

        return compact('documents');
    }

    public function addAction()
    {
        if($this->isPost())
        {
            $documentForm = Model::Form('DocumentForm');
            $documentModel = Model::ActiveRecord('Document');

            if($documentForm->is_valid())
            {
                $documentModel->saveForm($documentForm);

                $this->redirectTo(array('action' => 'index'));
            }

            $this->noticeTo(
                array(
                    'type'      => 'error',
                    'message'   => $documentForm->getErrors()
                  )
                );
        }
    }

    /**
     * Удаление документов
     */
    public function destructionAction()
    {
        if(!Model::ActiveRecord('Document')->exists($this->get('id')))
        {
            $this->noticeTo(
                array(
                    'type'      => 'error',
                    'message'   => 'Документ не найден'),
                array(
                    'controller' => 'backend\document')
            );
        }

        /** @var $document  \Application\Models\Document */
        $document = Model::ActiveRecord('Document')->find($this->get('id'));

        if($document->delete())
        {
            $this->noticeTo(
                array(
                    'type'      => 'success',
                    'message'   => 'Документ удален'),
                array(
                    'controller' => 'backend\document')
            );
        }

        $this->noticeTo(
            array(
                'type'      => 'error',
                'message'   => 'Не удалось удалить документ'),
            array(
                'controller' => 'backend\document')
        );
    }
}