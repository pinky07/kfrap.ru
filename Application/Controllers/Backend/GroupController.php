<?php
/**
 * Created by JetBrains PhpStorm.
 * User: PC
 * Date: 12.02.12
 * Time: 0:04
 * To change this template use File | Settings | File Templates.
 */
namespace Application\Controllers\Backend;
use \Framework\Model as Model;

class GroupController extends BackendController
{
    function indexAction() {
        $roleObjects = Model::ActiveRecord('Role')->all();
        return compact('roleObjects');
    }

    function addAction() {
        if($this->isPost()) {
            if($this->post('name') and $this->post('access')) {
                Model::ActiveRecord('Role')->create(
                                            array(
                                                        'name' =>$this->post('name'),
                                                         'access'  => serialize($this->post('access')
                                                         )
                                            ) );
            } else {
                if(!$this->post('name'))
                    $this->error('Не указано название группы');
                if(!$this->post('access'))
                    $this->error('Необходимо указать хотя бы одну привилегию');
            }
        }
        $controllers = \Framework\Framework::getAllActions(true);
        return compact('controllers');
    }

    function editAction() {
        if($this->params('id')) {
            $roleObject = Model::ActiveRecord('Role')->find($this->params('id'));
            if($this->isPost()) {
                if($this->post('name') and $this->post('access')) {
                    $roleObject->name = $this->post('name');
                    $roleObject->access = serialize($this->post('access'));
                    $roleObject->save();
                    $this->success('Данные успешно обновлены');
                } else {
                    if(!$this->post('name'))
                        $this->error('Не указано название группы');
                    if(!$this->post('access'))
                        $this->error('Необходимо указать хотя бы одну привилегию');
                }
            }
            $controllers = \Framework\Framework::getAllActions(true);
            return compact('roleObject', 'controllers');
        } else {

        }
    }
}
