<?php
/**
 * Created by JetBrains PhpStorm.
 * User: PC
 * Date: 11.02.12
 * Time: 23:59
 * To change this template use File | Settings | File Templates.
 */
namespace Application\Controllers\Backend;
use Application\Models\User as User;

class AuthController extends BackendController
{
    protected $_protect = false;
    protected $_layout  = 'empty';

    function indexAction()
    {
        if(User::isAuth())
            $this->redirectTo(array(
                'controller' => 'backend\index'
            ));

        /** @var $loginForm \Application\Models\Forms\LoginForm */
        $loginForm = \Framework\Model::Form('LoginForm');

        if($this->isPost())
        {
            if($loginForm->is_valid())
            {
                if($loginForm->auth())
                {
                    $this->redirectTo(array(
                        'controller' => 'backend\index'
                    ));
                }
                else
                {
                    $this->redirectTo(array(
                        'message' => 'Не удалось авторизоваться'
                    ));
                }
            } else
            {
                $this->addNotice('error', $loginForm->getErrors());
            }
        }
    }
}
