<?php
/**
 * Created by JetBrains PhpStorm.
 * User: PC
 * Date: 12.02.12
 * Time: 0:00
 * To change this template use File | Settings | File Templates.
 */
namespace Application\Controllers\Backend;
use \Framework\Model as Model;
use \Application\Models\User as User;
use \Application\Models\Role;

class PageController extends BackendController
{
    //Права на раздел
    protected $_access        = array(
        \Application\Models\Role::ADMINISTRATOR,
        \Application\Models\Role::EDITOR,
    );

    /**
     * @return array
     */
    public function indexAction()
    {
        $result = \Framework\Paginator::paginate(Model::ActiveRecord('Page'), array(), 10);

        return $result;
    }

    /**
     * @return array
     */
    public function responsibleAction()
    {
        $this->setView('index');

        $findParams = array(
            'conditions' => array(
                'responsible = '.$this->user->id
            )
        );

        $result = \Framework\Paginator::paginate(
            Model::ActiveRecord('Page'), $findParams, 10
        );

        return array_merge($result, array('responsible' => true));
    }

    /**
     * @return array
     */
    public function editAction()
    {
        if(!Model::ActiveRecord('Page')->exists($this->params('id')))
        {
            $this->noticeTo(
                array(
                    'type'      => 'error',
                    'message'   => 'Страница не найдена'),
                array(
                    'controller'=> 'admin\page'
                )
            );
        }

        /** @var $page \Application\Models\Page */
        $page = Model::ActiveRecord('Page')->find($this->params('id'));

        if(User::getCurrent()->id !== $page->responsible && User::getCurrent()->id !== 1)
        {
            $this->noticeTo(
                array(
                    'type'      => 'error',
                    'message'   => 'Редактировать данную страницу может только ответственный'),
                array(
                    'controller'=> 'admin\page'
                )
            );
        }

        /** @var $pageForm \Application\Models\Forms\PageForm */
        $pageForm = Model::Form('PageForm');

        if($this->isPost())
        {
            if($pageForm -> is_invalid())
            {
                $this->noticeTo(
                    array(
                        'type'      => 'error',
                        'message'   => $pageForm -> getErrors())
                );
            }
            if($page -> saveForm($pageForm))
            {
                $this->noticeTo(
                    array(
                        'type'      => 'success',
                        'message'   => 'Страница сохранена'),
                    array(
                        'controller'=> 'admin\page'
                    )
                );
            }
        }

        $menus[0] = 'Нет меню';
        foreach(Model::ActiveRecord('Menu')->all() as $menu)
        {
            $menus[$menu->id] = $menu->name;
        }

        $users[0] = 'Нет ответственного';
        foreach(Model::ActiveRecord('User')->all() as $user)
        {
            $users[$user->id] = $user->surname.' '.$user->name;
        }

        foreach(Model::ActiveRecord('Document')->all() as $document)
        {
            $documents[$document->id] = $document->name;
        }

        $pages[0] = 'Нет';
        foreach(Model::ActiveRecord('Page')->all() as $pg)
        {
            $pages[$pg->id] = $pg->title;
        }

        return compact('page', 'pages', 'menus', 'users', 'documents');
    }

    /**
     * @return array
     */
    public function addAction()
    {
        if(User::getCurrent()->role_id !== Role::ROOT && User::getCurrent()->role_id !== Role::ADMINISTRATOR)
        {
            $this->noticeTo(
                array(
                    'type'      => 'error',
                    'message'   => 'Нет прав для создания страниц'),
                array(
                    'controller'=> 'admin\page'
                )
            );
        }

        /** @var $page \Application\Models\Page */
        $page = Model::ActiveRecord('Page');

        /** @var $pageForm \Application\Models\Forms\PageForm */
        $pageForm = Model::Form('PageForm');

        if($this->isPost())
        {
            if($pageForm -> is_invalid())
            {
                $this->noticeTo(
                    array(
                        'type'      => 'error',
                        'message'   => $pageForm -> getErrors())
                );
            }

            if($page -> saveForm($pageForm))
            {
                $this->noticeTo(
                    array(
                        'type'      => 'success',
                        'message'   => 'Страница сохранена'),
                    array(
                        'controller'=> 'admin\page'
                    )
                );
            }
        }

        $menus[0] = 'Нет меню';
        foreach(Model::ActiveRecord('Menu')->all() as $menu)
        {
            $menus[$menu->id] = $menu->name;
        }

        $users[0] = 'Нет ответственного';
        foreach(Model::ActiveRecord('User')->all() as $user)
        {
            $users[$user->id] = $user->surname.' '.$user->name;
        }

        foreach(Model::ActiveRecord('Document')->all() as $document)
        {
            $documents[$document->id] = $document->name;
        }

        $pages[0] = 'Нет';
        foreach(Model::ActiveRecord('Page')->all() as $page)
        {
            $pages[$page->id] = $page->title;
        }

        return compact('menus', 'pages', 'documents', 'users');
    }
}
