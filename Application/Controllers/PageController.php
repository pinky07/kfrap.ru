<?php
/**
 * Created by JetBrains PhpStorm.
 * User: PC
 * Date: 12.02.12
 * Time: 0:22
 * To change this template use File | Settings | File Templates.
 */
namespace Application\Controllers;
use Framework\Model as Model;

class PageController extends FrontendController
{
    function indexAction()
    {
        $pageObject = Model::ActiveRecord('Page');
        return compact('pageObject');
    }

    public function viewAction()
    {
        if($this->params('id'))
        {
            /** @var \Application\Models\Page $pageObject  */
            $pageObject = Model::ActiveRecord('Page')->find($this->params('id'));

            if(!empty($pageObject->layout))
                $this->_actionView->setLayout($pageObject->layout);

            if(isset($pageObject->parent_page->id))
            {
                $this->breadcrumbs[$pageObject->parent_page->title] = $pageObject->parent_page->getUrl();
                $this->breadcrumbs[$pageObject->title] = $pageObject->getUrl();
            }
            else {
                $this->breadcrumbs[$pageObject->title] = $pageObject->getUrl();
            }


            $this->_setTags($pageObject->tags);

            $this->_pageMenuId = $pageObject->menu_id;

            $this->blockDocuments = $pageObject->getDocuments();

            return compact('pageObject');
        }
    }
}
