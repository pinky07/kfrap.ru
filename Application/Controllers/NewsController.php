<?php
/**
 * Created by JetBrains PhpStorm.
 * User: PC
 * Date: 11.02.12
 * Time: 3:09
 * To change this template use File | Settings | File Templates.
 */
namespace Application\Controllers;
use Framework\Model as Model;

class NewsController extends FrontendController
{
    public $breadcrumbs = array('Новости' => '/news');

    function indexAction()
    {
        $this->redirectTo(array('action' => 'category'));
    }

    function viewAction()
    {
        if(Model::ActiveRecord('Article')->exists($this->params('id')))
        {
            $newsObject = Model::ActiveRecord('Article')->find($this->params('id'));

            $this->breadcrumbs[$newsObject->category->title] = "/news/category/id/{$newsObject->category_id}";
            $this->breadcrumbs[$newsObject->title] = "";

            $this->blockDocuments = $newsObject->getDocuments();

            return compact('newsObject');
        } else
        {
            $this->redirect('/index/index');
        }
    }

    public function categoryAction()
    {
        $this->_actionView->setLayout('frontend/html2');

        if($this->params('id') && !Model :: ActiveRecord('Category')->exists($this->params('id')))
        {
            $this->redirect('/news/category');
        }

        $news       = Model :: ActiveRecord('Article')
                            -> getPublished(array(
                                    'category'  => ($this->params('id')) ? $this->params('id') : null,
                                    'sort'      => 'date',
                                    'date'      => ($this->get('date'))?$this->get('date'):null
        ));
        $categories = Model :: ActiveRecord('Category')
                            -> all();
        $date       = ($this->get('date'))?$this->get('date'):'';

        $categoryId = '';

        if($this->params('id'))
        {
            $currentCategory = Model :: ActiveRecord('Category')
                                        ->find($this->params('id'));
            $categoryTitle = $currentCategory -> title;
            $categoryId    = $currentCategory -> id;
            $this->breadcrumbs[$categoryTitle] = '';
        }
        else
            $categoryTitle = 'Все новости';

        $return = compact('news', 'categories', 'categoryTitle', 'categoryId', 'date');

        if($this->isAjax())
        {
            foreach($return['news'] as $key => $article)
            {
                $return['news'][$key]           = $article->to_array();
                $return['news'][$key]['image']  = ($article->getImages(true))?$article->getImages(true):(($article->slider_picture)?"/upload/images/{$article->slider_picture}":'/images/defaultimage.jpg');
            }

            $this->getResponse()->ajax($return);
        }

        return $return;
    }

}
