<?php
/**
 * Created by JetBrains PhpStorm.
 * User: pinky
 * Date: 05.02.12
 * Time: 16:15
 * To change this template use File | Settings | File Templates.
 */
use Framework\Autoloader;
use Framework\Framework;

define('_ROOT_DIR_', __DIR__.'/..');
define('_APP_ROOT_DIR_', __DIR__);
define('_FWK_ROOT_DIR_', __DIR__ . '/../Framework');
define('_WEB_ROOT_DIR_', _ROOT_DIR_.'/public');
define('_APP_BASE_DIR_', basename(_APP_ROOT_DIR_));
define('_FWK_BASE_DIR_', basename(_FWK_ROOT_DIR_));

include_once _FWK_ROOT_DIR_ . '/Autoloader.php';

Autoloader::getInstance()
      -> setNamespaces(
                    array(
                        'Framework'     => __DIR__.'/..',
                        'ActiveRecord'  => __DIR__.'/../Framework',
                        'Application'   => __DIR__.'/..',))
      -> register();

Framework::app()->envoirenment = 'development';

require_once 'include/config.php';

Framework::app()->init();
