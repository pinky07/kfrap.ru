/**
 * Created by JetBrains PhpStorm.
 * User: pinky
 * Date: 25.02.12
 * Time: 0:48
 * To change this template use File | Settings | File Templates.
 */
var MAIN_MENU_TIMER = null;


$(document).ready(
    function(){
	    setup_navbar_main();
	}
);

function setup_navbar_main() {
    var links   = $("#primary ul li a:not(#primary ul li ul li a)");
    var sublist = $(".navbar-item-sub");
    var content = $("#main-menu");
    for( var x = 0; x < links.length; x++ ) {

    var link = links.eq(x);
        link.attr("subid", "nav-" + x);

    var sub  = $( "div." + link.attr("subid") );

        if( sub.length == 0 )
            continue;

        sub.find("div.mark").css({"width": link.outerWidth()});
    }

    links.bind("mouseover", function() {
        clearTimeout(MAIN_MENU_TIMER);
        sublist.hide();
    var self = $(this);

        links.removeClass("active");
        self.addClass("active");

    var sub = $("div." + self.attr("subid"));
        if (sub.length == 0)
            return;

        if (!sub.hasClass("navbar-item-sub-left")) {
        var self_x = self.position().left;
            if (self_x + sub.width() > content.position().left + content.width()) {
                self_x = (content.position().left + position.width()) - sub.width();
                sub.find("div.mark").css({"left": self.position().left - self_x, "right": "auto"});
            }
        } else {
        var self_x = (self.position().left - sub.outerWidth()) + self.outerWidth();
            if (self_x < content.position().left) {
                sub.find("div.mark").css({"right": content.position().left - self_x});
                self_x = content.position().left
            }
        }

    var self_y = self.position().top + self.outerHeight() + 1;
        sub.css({"left": self_x, "top": self_y});
        sub.show();
    });

    links.bind("mouseout", function() {
        clearTimeout(MAIN_MENU_TIMER);
        MAIN_MENU_TIMER = setTimeout( function(){links.removeClass("active");sublist.hide();}, 500 );
    });

    sublist.bind("mouseout", function() {
        clearTimeout(MAIN_MENU_TIMER);
        MAIN_MENU_TIMER = setTimeout( function(){links.removeClass("active");sublist.hide();}, 500 );
    });

    sublist.bind("mouseover", function() {
        clearTimeout(MAIN_MENU_TIMER);
        MAIN_MENU_TIMER = setTimeout( function(){this.show();}, 500 );
    });
}

function setCookie(name, value) {
      var valueEscaped = escape(value);
      var expiresDate = new Date();
      expiresDate.setTime(expiresDate.getTime() + 365 * 24 * 60 * 60 * 1000); // 1 год
      var expires = expiresDate.toGMTString();
      var newCookie = name + "=" + valueEscaped + "; path=/; expires=" + expires;
      if (valueEscaped.length <= 4000) document.cookie = newCookie + ";";
}

function getCookie(name) {
      var prefix = name + "=";
      var cookieStartIndex = document.cookie.indexOf(prefix);
      if (cookieStartIndex == -1) return null;
      var cookieEndIndex = document.cookie.indexOf(";", cookieStartIndex + prefix.length);
      if (cookieEndIndex == -1) cookieEndIndex = document.cookie.length;
      return unescape(document.cookie.substring(cookieStartIndex + prefix.length, cookieEndIndex));
}
