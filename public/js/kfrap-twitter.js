/**
 * Created by JetBrains PhpStorm.
 * User: Pinky
 * Date: 24.05.12
 * Time: 18:31
 * To change this template use File | Settings | File Templates.
 */

var kfrapTwitterApp = function (containerId, refreshTime)
{
    var account         = ''; //аккаунт пользователя

    var twitterApiUrl   = '';

    var messages        = {};

    var container     = ''; //id контейнера куда подгружаются данные

    var content         = '';

    var errors          = '';

    var refresh         = 30; //периодичность обновления

    //Конструтор класса, инициализирует приложение
    function __initApp(containerId, refreshTime)
    {
        container   = containerId;
        refresh     = refreshTime;
    }

    //Обработчик аякс запросов
    function ajaxErrorHandler(e, jqxhr, settings, exception)
    {
        alert("AJAX error on loading url: "+settings.url);
    }

    //Обработчик ошибок полученных с сервера
    function responseErrorHandler(data)
    {

        return false;
    }

    function processResponse(data)
    {

        return true
    }

    //Общий метод для выполнения запросов на сервер
    function request(data)
    {
        $.ajax({
            url: twitterApiUrl,
            dataType: "json",
            data:data,
            success: (responseErrorHandler) ? processResponse:'',
            error: ajaxErrorHandler
        });
    }


    //обновляет данные в приложении
    this.update = function()
    {

    }

    //запускает приложение
    this.start = function()
    {

    }

    __initApp(containerId, refreshTime);
};