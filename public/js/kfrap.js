function XmlHttp(){
var xmlhttp;
try {
xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
} catch (e) {
try {xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");} 
catch (E) {xmlhttp = false;}
}
if (!xmlhttp && typeof XMLHttpRequest!='undefined') {
xmlhttp = new XMLHttpRequest();
}
return xmlhttp;
}

function ajaxDataMaker(data){
	if(typeof data!="object")return;
	var result="",a=0,c=0;
	for (var i in data)a++;
	for (var i in data){c++;result+= i+"="+data[i]+(c!=a?"&":'')}
	return result;
}

function ajax(param){//method,url,data,success
	var req = new XmlHttp();
	var method=(!param.method ? "POST" : param.method.toUpperCase());
	var send=(method=="GET"?null:(param.data?ajaxDataMaker(param.data):null));
	req.open(method, param.url, true);
	req.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	req.setRequestHeader("X-Requested-With", "XMLHttpRequest");
	req.send(send);
	req.onreadystatechange = function(){
		if (req.readyState==1 || req.readyState=="loading") {
			if(param.load) param.load();
		}
		if (req.readyState == 4 && req.status == 200) {//���� ����� �������������
			if(param.success) param.success(req.responseText,req.getResponseHeader('error'));
			ajaxEvent();
		}
	}
}

function go(param,event,success) {
	var url,success=success||param.success||false,event=event||param.event||false;
	if(typeof param.href!="undefined")url=param.href;
	else {url=(typeof param=="string"?param:(typeof param.url=="object" ? param.url.href : param.url));}
	if(!histAPI){window.location=url;return true;}
	if(event && event.button==1)return true;
		ajax({
			url:(typeof param.data!="undefined" ? url+"&"+param.data : url),
			method:"GET",
			data:param.data,
			success:function(data){
				history.pushState(null,null, url);
			}
		})
		return false;
}