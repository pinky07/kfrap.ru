<?php
/**
 * Created by JetBrains PhpStorm.
 * User: pinky
 * Date: 19.02.12
 * Time: 16:44
 * To change this template use File | Settings | File Templates.
 */
namespace Framework\Validators;

interface ValidatorInterface
{
    public function validateAttribute();

    public function clientValidateAttribute();
}
