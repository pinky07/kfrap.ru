<?php
/**
 * Created by JetBrains PhpStorm.
 * User: pinky
 * Date: 05.02.12
 * Time: 16:34
 * To change this template use File | Settings | File Templates.
 * @Package Framework
 * @Version 0.2
 */
namespace Framework;

/**
 * @property bool       $_running
 * @property int        $_envoirenment
 * @property Framework  $_application
 * @property Config     $config
 */
class Framework extends Foundation
{

    /**
     * @var bool
     */
    private $_running      = false;
    /**
     * @var int
     */
    private $_envoirenment = self::DEVELOPMENT_ENV; //current aplication envoirenment
    /**
     * @var string
     */
    private $_contentType   = 'text/html';
    /**
     * @var string
     */
    private $_baseUrl       = '/';
    /**
     * @var string
     */
    private $_encoding       = 'utf-8';
    /**
     * @var string
     */
    private $locale        = 'eng';
    /**
     * @var Config|null
     */
    private $config        = null;   //config object

    /**
     * @var
     */
    public  $log;                    //log object
    /**
     * @var null
     */
    private static $_application = null; //aplication instance (singleton pattern)
    /**
     * @var \Swift_Mailer
     */
    private $mailer;
    /**
     *
     */
    const AUTHOR            = 'Hamidullin_Kamil';
    /**
     *
     */
    const VERSION           = '0.2';

    /**
     *
     */
    const DEVELOPMENT_ENV   = 100;
    /**
     *
     */
    const PRODUCTION_ENV    = 200;

    /**
     *
     */
    private function __construct()
    {
        $this->config = new Config();
    }

    /**
     * @param $envoirenment
     * @throws FrameworkException
     */
    public function setEnvoirenment($envoirenment)
    {
        if($this->isRunning())
            throw new FrameworkException('Application already running');

        if(strtolower($envoirenment) == 'production')
        {
            $this->_envoirenment = self::PRODUCTION_ENV;
        }
        elseif(strtolower($envoirenment) == 'development')
        {
            $this->_envoirenment = self::DEVELOPMENT_ENV;
        }
    }

    /**
     * @return int
     */
    public function getEnvoirenment()
    {
        return $this->_envoirenment;
    }

    /**
     * @static
     * @return Framework
     */
    public static function app()
    {
        if(!self::$_application instanceof Framework)
            self::$_application = new Framework();

        return self::$_application;
    }

    /**
     *
     */
    public function init()
    {
        $this->initActiveRecord();
        $this->initSwiftMailer();
        Session::getInstance();
    }

    /**
     * @return bool
     */
    public function isRunning()
    {
        return $this->_running;
    }

    /**
     *
     */
    private function initActiveRecord()
    {
        include_once 'ActiveRecord/ActiveRecord.php';

        \ActiveRecord\Config::initialize(function($cfg){
            $cfg->set_model_directory('../Application/Models');
            $cfg->set_connections(array(
            'development' => Framework::app()->getConfig('db')->connect));
        });
    }

    /**
     *
     */
    private function initSwiftMailer()
    {
        require_once 'SwiftMailer/lib/swift_required.php';

        /** @var $transportName \Swift_Transport */
        $transportName = sprintf('\Swift_%sTransport', \Framework\String::ucfirst(Framework::app()->getConfig('mail')->transport));

        if(class_exists($transportName))
        {
            // Create the Transport
            $transport = $transportName::newInstance('/usr/sbin/sendmail -t');
        }

        // Create the Mailer using your created Transport
        $this->mailer = \Swift_Mailer::newInstance($transport);
    }

    /**
     * @return \Swift_Mailer
     */
    public function getMailer()
    {
        return $this->mailer;
    }

    /**
     * @param $config_ns
     * @return bool|\Framework\Config\Config
     */
    public function getConfig($config_ns)
    {
        return $this->config->get($config_ns);
    }

    /**
     * @param $config_ns
     * @param array $params
     * @return Framework
     * @throws FrameworkException
     */
    public function setConfig($config_ns, array $params)
    {
        if($this->isRunning())
            throw new FrameworkException('Application already running');

        $this->getConfig($config_ns)->set($params);

        return $this;
    }

    public function getEncoding()
    {
        return $this->_encoding;
    }

    /**
     *
     */
    public function run()
    {
        $this->_running = true;

        \Framework\FrontController::getInstance()->dispatch();
    }

    public function getBaseUrl()
    {
        return $this->_baseUrl;
    }

    /**
     *
     */
    public function end($message = null)
    {
        exit($message);
    }

    /**************************************
     *todo перенести отсюда в другое место!
     **************************************/
    public static function getAllControllers($controllers_path = null)
    {
        $controllers_ns = Framework::getConfig('basic', 'application_dir').'\\'.Framework::getConfig('basic', 'controllers_dir');

        if(($controllers_path === null) or  ($controllers_path === false)) {
            $dir['current']        = __DIR__;
            $dir['back']              = '..';
            $dir['application'] = Framework::getConfig('basic', 'application_dir');
            $dir['controllers'] = Framework::getConfig('basic', 'controllers_dir');

            $controllers_path = implode(DIRECTORY_SEPARATOR,$dir);
        }
        
        $prefix = str_replace(DIRECTORY_SEPARATOR, '',substr($controllers_path, strpos($controllers_path, Framework::getConfig('basic', 'controllers_dir'))+strlen(Framework::getConfig('basic', 'controllers_dir'))));

        $controllers = array();

        foreach(scandir($controllers_path) as $dir) {
            if($dir === '..' or $dir === '.')
            {
                continue;
            } elseif(strpos($dir, 'Controller.php'))
            {
                if(!empty($prefix))
                    $dir = ucfirst($prefix).'\\'.$dir;

                $class = str_replace('.php', '',$dir);

                if(class_exists($controllers_ns.'\\'.$class))
                    $controllers[] = $class;

            } elseif(is_dir($controllers_path. DIRECTORY_SEPARATOR .$dir))
            {
                foreach(self::getAllControllers($controllers_path. DIRECTORY_SEPARATOR .$dir, $dir) as $controller) {
                    $controllers[] = $controller;
                }
            }
        }

        return $controllers;
    }

    /**
     * @static
     * @param bool $postfix
     * @return array
     */
    public static function  getAllActions($postfix = false) {
        $controllers    = self::getAllControllers();
        $controllers_ns = Framework::getConfig('basic', 'application_dir').'\\'.Framework::getConfig('basic', 'controllers_dir');
        $actions = array();
        foreach($controllers as $controller) {
            $methods = get_class_methods($controllers_ns.'\\'.$controller);

            foreach($methods as $method) {
                if(strpos($method, 'Action')) {
                    if($postfix) {
                        $controller = str_replace('Controller', '', $controller);
                        $method = str_replace('Action', '', $method);
                    }
                        $actions[strtolower($controller)][] = $method;
                }
            }
        }
        return $actions;
    }
}
