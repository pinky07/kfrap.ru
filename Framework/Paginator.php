<?php
/**
 * Created by JetBrains PhpStorm.
 * User: pinky
 * Date: 16.03.12
 * Time: 20:12
 * To change this template use File | Settings | File Templates.
 */
namespace Framework;
//use \ActiveRecord\Model as Model;
class Paginator
{

    const TEMPLATE = '';

    public static function paginate(\ActiveRecord\Model $model, array $params = array(), $limit = 20)
    {
        $elemets_count  = $model->count($params);

        if(!empty($_GET['page']) and $_GET['page'] <= ceil($elemets_count/$limit))
        {
            $current_page = (int)$_GET['page'];
        }
        else
        {
            $current_page = 1;
        }

            $offset         = ($current_page-1)*$limit;

        $params['limit'] = $limit;
        $params['offset'] = $offset;

        $elements = $model->all($params);

        if(ceil($elemets_count/$limit) > 1)
        {
            $pages = '<div class="pagination"><ul>';

            parse_str($_SERVER['QUERY_STRING'], $queries);

            $url = FrontController::getInstance()->getRequest()->getRequestUri();

            $showPages = 3;
            for($i=1; $i<= ceil($elemets_count/$limit); $i++)
            {
                $queries['page'] = $i;
                $queryStr = http_build_query($queries);

                if($i+$showPages > $current_page && $i-$showPages < $current_page)
                {
                    if($i == $current_page)
                    {
                        $pages .= "<li class='active'><a href='#'> {$i} </a></li>";
                    }
                    else
                    {
                        $pages .= "<li><a href='{$url}?{$queryStr}'> {$i} </a></li>";
                    }
                }
                else
                {
                    if($i + $showPages == $current_page)
                    {
                        $pages .= "<li><a href='{$url}?{$queryStr}'> << </a></li>";
                    }
                    if($i - $showPages == $current_page)
                    {
                        $pages .= "<li><a href='{$url}?{$queryStr}'> >> </a></li>";
                    }
                }
            }
            $pages .= '</ul></div>';
        }
        else
        {
            $pages = '';
        }


        return compact('elements','pages');
    }
}
