<?php
/**
 * @package Framework
 * @file    FrontController
 * @version 0.1
 * @author  Hamidullin Kamil
 */
namespace Framework;

/**
 * @property Request    $_request
 * @property Router     $_router
 * @property Response   $_response
 * @property Dispatcher $_dispatcher
 */
class FrontController
{
    /**
     * @var Request
     */
    private $_request       = null;
    /**
     * @var Router
     */
    private $_router        = null;
    /**
     * @var Response
     */
    private $_response      = null;
    /**
     * @var Dispatcher
     */
    private $_dispatcher    = null;
    /**
     * @var string
     */
    private $_baseUrl;
    /**
     * @var FrontController
     */
    private static $_instance;

    /**
     * @static
     * @return FrontController
     */
    public static function getInstance()
    {
		if(!isset(self::$_instance))
			self::$_instance = new self();
		return self::$_instance;
	}

    /**
     *
     */
    private function __construct()
    {
        $this->_baseUrl = Framework::app()->baseUrl;
	}

    /**
     * Основной метод отправки запроса - получения ответа
     *
     * @param Request|null $request
     * @param Response|null $response
     * @return null
     */
    public function dispatch(Request $request = null, Response $response = null)
    {
        if($request === null) {
            $request = new Request();
            $this->setRequest($request);
        } else {
            $this->setRequest($request);
        }
        $this->_request->setBaseUrl($this->_baseUrl);
        if($response === null) {
            $response = new Response();
            $this->_response = $response;
        } else {
            $this->_response = $response;
        }

        $router = $this->getRouter();
        $router->route($this->_request);

        $dispatcher = $this->getDispatcher();
        $dispatcher->setResponse($response);
        do {
            $this->_request->setDispatched(true);

            try{
                $dispatcher->dispatch($this->_request, $this->_response);
            } catch(\Exception $e) {
                echo $e->getMessage();
            }

        }  while (!$this->_request->isDispatched());

        $this->_response->sendResponse();
    }

    /**
     * @param Request $request
     */
    public function setRequest(Request $request)
    {
        $this->_request = $request;
    }

    /**
     * @return Request
     */
    public function getRequest()
    {
        if($this->_request == null)
            $this->setRequest(new Request());

            return $this->_request;
    }

    /**
     * @param Router|null $router
     */
    public function setRouter(Router $router = null)
    {
        if($router !== null)
            $this->_router = $router;
        else
            $this->_router = new Router();
    }

    /**
     * @return Router
     */
    public function getRouter()
    {
        if($this->_router == null)
            $this->setRouter();

        return $this->_router;
    }

    /**
     * @return Dispatcher
     */
    public function getDispatcher()
    {
        if($this->_dispatcher === null)
            $this->_dispatcher = new Dispatcher();

        return $this->_dispatcher;
    }

    /**
     * @param Dispatcher $dispatcher
     */
    public function setDispatcher(Dispatcher $dispatcher)
    {
        $this->_dispatcher = $dispatcher;
    }
}
?>