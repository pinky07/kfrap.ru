<?php
/**
 * Created by JetBrains PhpStorm.
 * User: pinky
 * Date: 17.05.12
 * Time: 22:51
 * To change this template use File | Settings | File Templates.
 */
namespace Framework;

interface INotice
{
    public function setMessage($message);

    public function getMessage();

    public function getType();
}
