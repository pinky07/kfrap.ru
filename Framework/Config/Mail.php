<?php
/**
 * Created by JetBrains PhpStorm.
 * User: pinky
 * Date: 22.05.12
 * Time: 23:07
 * To change this template use File | Settings | File Templates.
 */
namespace Framework\Config;

class Mail extends \Framework\Config\Config
{
    protected $transport;
    protected $smtp_host;
    protected $smtp_port;
    protected $send_mail;
}
