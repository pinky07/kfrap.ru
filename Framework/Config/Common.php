<?php
/**
 * Created by JetBrains PhpStorm.
 * User: pinky
 * Date: 06.03.12
 * Time: 23:26
 * To change this template use File | Settings | File Templates.
 */
namespace Framework\Config;

class Common extends \Framework\Config\Config
{
    protected $site_url         = null;
    protected $application_dir  = null;
    protected $controllers_dir  = null;
    protected $models_dir       = null;
    protected $views_dir        = null;
    protected $layout_dir       = null;
    protected $routes_file      = null;
    protected $admin_path       = null;
}
