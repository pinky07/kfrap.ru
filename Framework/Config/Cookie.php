<?php
/**
 * Created by JetBrains PhpStorm.
 * User: pinky
 * Date: 06.03.12
 * Time: 23:26
 * To change this template use File | Settings | File Templates.
 */
namespace Framework\Config;

class Cookie extends \Framework\Config\Config
{
    private $exp        = 3600;
    private $path       = '/';
    private $domain     = NULL;
    private $secure     = FALSE;
    private $httponly	= FALSE;
}
