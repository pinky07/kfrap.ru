<?php
/**
 * Created by JetBrains PhpStorm.
 * User: pinky
 * Date: 07.03.12
 * Time: 0:40
 * To change this template use File | Settings | File Templates.
 */
namespace Framework\Config;

/**
 *
 */
abstract class Config
{
    /**
     * @static
     * @param $config
     * @return null
     */
    public static function create($config)
    {
        $configObj = new $config;

        if(!is_subclass_of($configObj, 'Framework\Config\Config'))
        {
            return null;
        }

        return $configObj;
    }

    /**
     * @param array $params
     */
    function set(array $params)
    {
        foreach($params as $param => $value)
        {

            if(is_array($value))
            {
                if(!isset($value[\Framework\Framework::app()->envoirenment]))
                    break;

                $value = $value[\Framework\Framework::app()->envoirenment];

            }

            if(array_key_exists($param, get_object_vars($this)))
            {
                $this->$param = $value;

                define(strtoupper($param), $value);
            }
        }
    }

    /**
     * @param $name
     * @return bool|mixed
     */
    public function __get($name)
    {
        $methodName  = 'get'.ucfirst($name);

        if(method_exists($this, $methodName))
        {
            return call_user_func(array($this, $methodName));
        }
        elseif (isset($this->$name))
        {
            return $this->$name;
        }

        return false;
    }

    /**
     * @param $name
     * @param $value
     * @return bool|Config
     * @throws \Framework\FrameworkException
     */
    public function __set($name, $value)
    {
        if(\Framework\Framework::app()->isRunning())
            throw new \Framework\FrameworkException('application already running settings read-only');

        if(!isset($this->$name))
            return false;

        $this->$name = $value;

        return $this;
    }
}
