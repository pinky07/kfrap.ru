<?php
/**
 * Created by JetBrains PhpStorm.
 * User: pinky
 * Date: 06.03.12
 * Time: 23:25
 * To change this template use File | Settings | File Templates.
 */
namespace Framework\Config;

class DataBase extends \Framework\Config\Config
{
    protected $host   = 'localhost';
    protected $user   = 'root';
    protected $pass   = NULL;
    protected $name   = NULL;
    protected $enc    = NULL;
    protected $pref   = NULL;

    public function getConnect()
    {
        return 'mysql://'.$this->user.':'.$this->pass.'@'.$this->host.'/'.$this->name.';charset='.$this->enc;
    }
}
