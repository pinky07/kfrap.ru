<?php
/**
 * Created by JetBrains PhpStorm.
 * User: pinky
 * Date: 06.03.12
 * Time: 22:27
 * To change this template use File | Settings | File Templates.
 */
namespace Framework;

abstract class Singleton
{
	/**
	 * @var array
	 */
	private static $instances = array();

	/**
	 * @return object
	 */
	final public static function instance()
	{
		$class_name = get_called_class();

		if (!isset(self::$instances[$class_name]))
			self::$instances[$class_name] = new $class_name;

		return self::$instances[$class_name];
	}

	/**
	 * @return void
	 */
	final private function __clone() {}

	/**
	 * @return string
	 */
	final protected function get_called_class()
	{
		$backtrace = debug_backtrace();
    	return get_class($backtrace[2]['object']);
	}
}
