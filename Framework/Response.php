<?php
/**
 * Created by JetBrains PhpStorm.
 * User: pinky
 * Date: 06.02.12
 * Time: 21:49
 * To change this template use File | Settings | File Templates.
 */
namespace Framework;

class Response
{
    private $_body = null;
    private $_headers = array();
    private $_httpResponseCode = 200;

    public function __construct()
    {
        header('Content-Type: text/html; charset = utf-8');
    }

    public function setHeader($name, $value, $replace = false)
    {
        if ($replace)
        {
            foreach ($this->_headers as $key => $header) {
                if ($name == $header['name']) {
                    unset($this->_headers[$key]);
                }
            }
        }

        $this->_headers[] = array(
                                'name'      => $name,
                                'value'     => $value,
                                'replace'   => $replace);
    }

    public function setBody($content)
    {
        $this->_body = $content;
    }

    public function appendBody($content)
    {
        $this->_body .= $content;
    }

    public function prependBody($content)
    {
        $this->_body = $content.$this->_body;
    }

    public function getBody()
    {
        return $this->_body;
    }

    public function sendHeaders()
    {
        header('HTTP/1.1 ' . $this->_httpResponseCode);

        foreach($this->_headers as $header)
        {
            header($header['name'] . ': ' . $header['value'], $header['replace']);
        }
    }

    public function outputBody()
    {
        echo $this->_body;
    }

    public function sendResponse()
    {
        $this->sendHeaders();
        $this->outputBody();
    }

    /**
     * Отправляет ответ на ajax запрос в виде json объекта
     * @param $data
     */
    public function ajax($data)
    {
        $this->setHeader('Content-Type', 'application/json');

        $this->sendHeaders();
        ob_clean();
        echo exit(json_encode($data));
    }
}
