<?php
function day($sdate) {
	if ($sdate == 0)
		$from['day'] = 0;
	else
		$from['day']=("$sdate[8]$sdate[9]");
	$day=array(0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31);
	$m=count($day);
	$ops ='';
	for($i=1;$i<$m;$i++)
	{ $ops .="<option ";
	($i==$from['day']) ? $ops.=' selected ' : $ops.=' ';
	$ops.="value=\"$i\">$day[$i]</option>"; }
	echo $ops;
}

function month($sdate) {
	$ay = array('', '������', '�������', '����', '������', '����', '����', '����', '������', '��������', '�������', '������', '�������');

	$from['month']=("$sdate[5]$sdate[6]");
	
	$m=count($ay);
	$ops ='';
	for($i=1;$i<$m;$i++)
	{ $ops.="<option ";
	($i==$from['month']) ? $ops.=' selected ' : $ops.=' ';
	 $ops.="value=\"$i\">$ay[$i]</option>"; }
	echo $ops;
}

function year($sdate,$direction=0) {
	$from['year']=("$sdate[0]$sdate[1]$sdate[2]$sdate[3]");
	$thisyear=date("Y");
	//($thisyear!=$from[year] && $sdate) ?  : $syear=$thisyear;
	$syear=$from['year'];
	$ops ='';
	if($direction)
		for($i=2005;$i<=2015;$i++)
		{
			$ops.="<option ";
			($i==$syear) ? $ops.=' selected ' : $ops.=' ';
			$ops.="value=\"$i\">$i</option>";
		}
	else
		for($i=$thisyear+1;$i>2009;$i--)
		{
			$ops.="<option ";
			($i==$syear) ? $ops.=' selected ' : $ops.=' ';
			$ops.="value=\"$i\">$i</option>";
		}
	echo $ops;
}

function hours($pickup) {
	$selected="$pickup[0]$pickup[1]";
	$hour=array('00','01','02','03','04','05','06','07','08','09','10','11','12','13','14','15','16','17','18','19','20','21','22','23');
	$m=count($hour);
	$ops ='';
	for($i=0;$i<$m;$i++) {
	 $ops.='<option';
	 ($hour[$i]==$selected) ? $ops.=' selected ' : $ops.=' ';
	 $ops.='value="'.$hour[$i].'">'.$hour[$i].'</option>'; }
	 echo $ops;
}

function mins($pickup) {
	$selected="$pickup[3]$pickup[4]";
	$ops ='';
	for($i=0;$i<60;$i++) {
	 $ops.='<option';
	 ($i==$selected) ? $ops.=' selected ' : $ops.=' ';
	 $ops.='value="'.$i.'">';
	 ($i<10) ? $ops.='0'.$i : $ops.=$i;
	 $ops.='</option>'; }
	 echo $ops;
}

function __autoload($class) {
	$path = explode(PATH_SEPARATOR, get_include_path());
	if(strrchr($class, '_')) {
		$splits = explode('_', trim($class, '_'));
		$splits['0'] .= 's';
		$classPath = strtolower(implode(DIRECTORY_SEPARATOR, $splits));
		if(file_exists($classPath.'.php')) {
			include($classPath.'.php');
		}
	} else {
		foreach($path as $val) {
			if(file_exists($val.DIRECTORY_SEPARATOR.strtolower($class).'.php')) {
				include(strtolower($class).'.php');
			}
		}
	}
}

function get_date($style = 'Y-m-d H:i:s') {
	
	$date = date($style, strtotime(TIME_ZONE." hour", strtotime(date($style))));
	return $date;
}

function automail($to,$subj,$mesaj,$from='bettips.ru',$frommail='bettips.ru')
{
	//if (!$from) $from=readconf('site_name');
	//if (!$frommail) $frommail=readconf('robot_mail');
	$boundary='--' . md5( uniqid("rhnegatif") );
	$headers="From: $from <$frommail>\n";
	$headers.="Reply-To: $from <$frommail>\n";
	$headers.="X-Priority: 1\n";
	$headers.="X-MSMail-Priority: High";
	$headers.="Mime-Version: 1.0\n";
	$headers.="Content-Type: text/html; charset=utf-8\n"; 
	$headers.="Content-Transfer-Encoding: 8bit\n";
	$headers.="X-Mailer: Php/libMailv1.3\n";
	$result=mail($to,$subj,$mesaj,$headers);
	return $result;
}

function getUserIp() {
	if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
		$ip = $_SERVER['HTTP_CLIENT_IP'];
	}
	elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
		$ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
	} else {
		$ip = $_SERVER['REMOTE_ADDR'];
	}
	return $ip;
}

function redirect($redUrl) {
	header('Location: '.$redUrl);
}

function getCurController() {
	$splits = explode('/', trim($_SERVER['REQUEST_URI'], '/'));
	if($splits[0] == ADMIN_PATH)
		return $splits[1];
	else
		return $splits[0];
}

function genStr($length = 12){
  $chars = 'abcdefghijklmnopqrstuvwxyz123456789';
  $numChars = strlen($chars);
  $string = '';
  for ($i = 0; $i < $length; $i++) {
    $string .= substr($chars, rand(1, $numChars) - 1, 1);
  }
  return $string;
}

function imgUpload($src, $dest, $width, $height, $rgb = 0xFFFFFF, $quality = 100) {
	if (!file_exists($src)) {
		return false;
	}

	$size = getimagesize($src);

	if ($size === false) {
		return false;
	}

	$format = strtolower(substr($size['mime'], strpos($size['mime'], '/') + 1));
	$icfunc = 'imagecreatefrom'.$format;

	if (!function_exists($icfunc)) {
		return false;
	}

	$x_ratio = $width  / $size[0];
	$y_ratio = $height / $size[1];

	if ($height == 0) {

		$y_ratio = $x_ratio;
		$height  = $y_ratio * $size[1];

	} elseif ($width == 0) {

		$x_ratio = $y_ratio;
		$width   = $x_ratio * $size[0];

	}

	$ratio       = min($x_ratio, $y_ratio);
	$use_x_ratio = ($x_ratio == $ratio);

	$new_width   = $use_x_ratio  ? $width  : floor($size[0] * $ratio);
	$new_height  = !$use_x_ratio ? $height : floor($size[1] * $ratio);
	$new_left    = $use_x_ratio  ? 0 : floor(($width - $new_width)   / 2);
	$new_top     = !$use_x_ratio ? 0 : floor(($height - $new_height) / 2);

	$isrc  = $icfunc($src);
	$idest = imagecreatetruecolor($width, $height);

	imagefill($idest, 0, 0, $rgb);
	imagecopyresampled($idest, $isrc, $new_left, $new_top, 0, 0, $new_width, $new_height, $size[0], $size[1]);

	imagejpeg($idest, $dest, $quality);

	imagedestroy($isrc);
	imagedestroy($idest);

	return true;
}

function paging($array, $perPage, $curPage) {
	if(is_array($array)) {
		$cnt 	= count($array);
		if($cnt>0) {
			$stamp	= ($perPage*$curPage)-$perPage;
			if($stamp>=0) {
				for($i=$stamp; $i<$stamp+$perPage and $i<$stamp+($cnt-$stamp); $i++) {
					$arr[] = $array[$i];
				}
				return $arr;
			} else {
				return false;
			} 
		} else {
				return false;
		} 
	} else {
		return false;
	}
}

function checkImg($img) {
	if(isset($img['type']) and isset($img['tmp_name']) and isset($img['size'])) {
		if(!empty($img['type']) and !empty($img['tmp_name']) and !empty($img['size'])) {
			
			$accessTypes = array('jpeg', 'png', 'gif');
			$accessSize  = 300000;
			
			if($img['size'] <= $accessSize) {
				$arr = explode('/', $img['type']);
				$imgType = $arr['1'];
				
				foreach($accessTypes as $value) {
					if($imgType == $value) {
						$check	= true;
						break;
					}
				}

				if(!isset($check))
					$check	= false;
				
				return $check;
			} else {
				return false;
			}
		} else {
			return false;
		}
	} else {
		return false;
	}
}

function checkLogin($login) {
	if(preg_match("/^[a-z0-9]{3,16}$/", $login))
		return true;
	else
		return false;
}

function checkPass($pass) {
	if(preg_match("/^[a-z0-9]{3,16}$/", $pass))
		return true;
	else
		return false;		
}

function checkMail($mail) {
	if(preg_match("/^[a-zA-Z0-9]{1,30}@[a-zA-Z0-9]{3,30}\.[a-zA-Z]{2,3}$/", $mail))
		return true;
	else
		return false;		
}

function clean($input, $sql=false) {
    $input = htmlentities($input, ENT_QUOTES, ENCODING);
    // ��������������� ������.

    if(get_magic_quotes_gpc ())
    {
        $input = stripslashes ($input);
        // ������ ������ ������ �������������.
    }
    if ($sql)
    {
        $input = mysql_real_escape_string ($input);
        // ���� ����� MySQL-������, �� ������ ��������������� �������. 
        // ����������� � ���� ������ ���� ��������!
    }
    $input = strip_tags($input);
    //����� ����.

    $input=str_replace ("\n"," ", $input);
    $input=str_replace ("\r","", $input);
    //������������ �������� ������.

    return $input;
}

function ajaxPath() {
	$path = 'views'.DIRECTORY_SEPARATOR.'backend'.DIRECTORY_SEPARATOR.BACKEND_TEMPLATE.DIRECTORY_SEPARATOR.'ajax'.DIRECTORY_SEPARATOR;
	return $path;
}
?>