<?php
/**
 * Created by JetBrains PhpStorm.
 * User: pinky
 * Date: 05.02.12
 * Time: 16:11
 * To change this template use File | Settings | File Templates.
 */
namespace Framework;

class Foundation {

    protected $_getters = array();
    protected $_setters = array();

    public function __set($name, $value)
    {
        $methodName  = 'set'.ucfirst($name);
        if(method_exists($this, $methodName))
        {
            call_user_func(array($this, $methodName), $value);

        }
        elseif (in_array($name, $this->_setters))
        {
            $this->$name = $value;
        }
        elseif (in_array($name, $this->_getters))
        {
            throw new FrameworkException('property '.$name.' is write-only');
        }
        else
        {
            throw new FrameworkException('property '.$name.' is not accessible.');
        }

        return $this;
    }

    public function __get($name)
    {
        $methodName  = 'get'.ucfirst($name);
        if(method_exists($this, $methodName))
        {
            return call_user_func(array($this, $methodName));
        }
        elseif (in_array($name, $this->_getters))
        {
            return $this->$name;
        }
        elseif (in_array($name, $this->_setters))
        {
            throw new FrameworkException('property '.$name.' is read-only');
        }
        else
        {
            throw new FrameworkException('property '.$name.' is not accessible.');
        }
    }
}
