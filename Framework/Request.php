<?php
/**
 * @package Framework
 * @file    FrontController
 * @version 0.1
 * @author  Hamidullin Kamil
 */
namespace Framework;

/**
 *
 */
class Request
{
    /**
     * @var bool
     */
    protected $_dispatched          = false;
    /**
     * @var string
     */
    protected $_directory;
    /**
     * @var string
     */
    protected $_controller;
    /**
     * @var string
     */
    protected $_action;
    /**
     * @var string
     */
    protected $_defaultController   = 'index';
    /**
     * @var string
     */
    protected $_defaultAction       = 'index';
    /**
     * @var array
     */
    protected $_params              = array();
    /**
     * @var string
     */
    protected $_requestUri;
    /**
     * @var
     */
    protected $_queryString;
    /**
     * @var string
     */
    protected $_baseUrl;
    /**
     * @var Response
     */
    protected $_response;
    /**
     * @var bool
     */
    protected $_xmlhttpRequest      = false;

    /**
     * @param null $uri
     */
    public function __construct($uri = null)
    {
        $this->setBaseUrl();
        $this->setRequestUri($uri);
        $this->setQueryString();
        $this->checkXmlhttpRequest();
    }

    /**
     * @return Request
     */
    public function checkXmlhttpRequest()
    {
        if((isset($_SERVER['HTTP_X_REQUESTED_WITH'])) and ($_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest'))
            $this->_xmlhttpRequest = true;
        else
            $this->_xmlhttpRequest = false;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getRequestMethod()
    {
        return $_SERVER['REQUEST_METHOD'];
    }

    /**
     * @return mixed
     */
    public function getReferrer()
    {
        return isset($_SERVER['HTTP_REFERER'])?$_SERVER['HTTP_REFERER']:null;
    }

    /**
     * @return mixed
     */
    public function getUserIp()
    {
        if (!empty($_SERVER['HTTP_CLIENT_IP']))
        {
            $ip = $_SERVER['HTTP_CLIENT_IP'];
        }
        elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))
        {
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        } else
        {
            $ip = $_SERVER['REMOTE_ADDR'];
        }

        return $ip;
    }

    /**
     * @param null $userAgent
     * @return mixed
     */
    public function getBrowser($userAgent=null)
    {
        return get_browser($userAgent,true);
    }

    /**
     * @return bool
     */
    public function isXmlHttpRequest()
    {
        return $this->_xmlhttpRequest;
    }

    /**
     * @param bool $flag
     * @return Request
     */
    public function setDispatched($flag = true)
    {
        $this->_dispatched = $flag ? true : false;
        return $this;
    }

    /**
     * @return bool
     */
    public function isDispatched()
    {
        return $this->_dispatched;
    }

    /**
     * @return string
     */
    public function getControllerName()
    {
        return $this->_controller;
    }

    /**
     * @param $controllerName
     * @return Request
     */
    public function setControllerName($controllerName)
    {
        $this->_controller = strtolower($controllerName);
        return $this;
    }

    /**
     * @return string
     */
    public function getActionName()
    {
        return $this->_action;
    }

    /**
     * @param $actionName
     * @return Request
     */
    public function setActionName($actionName)
    {
        $this->_action = $actionName;
        return $this;
    }

    /**
     * @return string
     */
    public function getControllerDirectory()
    {
        return $this->_directory;
    }

    /**
     * @param $controller_directory
     * @return Request
     */
    public function setControllerDirectory($controller_directory)
    {
        $this->_directory = $controller_directory;
        return $this;
    }

    /**
     * @return string
     */
    public function getDefaultController()
    {
        return $this->_defaultController;
    }

    /**
     * @return string
     */
    public function getDefaultAction()
    {
        return $this->_defaultAction;
    }

    /**
     * @param array $params
     * @return Request
     */
    public function setParams(array $params)
    {
        $this->_params = $params;
        return $this;
    }

    /**
     * @return array
     */
    public function getParams()
    {
        return $this->_params;
    }

    /**
     * @param null $requestUri
     * @return Request
     */
    public function setRequestUri($requestUri = null)
    {
        if ($requestUri === null)
        {
            if(isset($_SERVER['REQUEST_URI']))
            {
                if($this->_baseUrl == '/')
                    $requestUri = $_SERVER['REQUEST_URI'];
                else
                    $requestUri = str_replace($this->_baseUrl, '', $_SERVER['REQUEST_URI']);
            }


            if($pos = strpos($requestUri, '?'))
                $requestUri = substr($requestUri, 0, $pos);
        }

        $this->_requestUri = $requestUri;

        return $this;
    }

    /**
     * @param null $query
     * @return Request
     */
    public function setQueryString($query = null)
    {
        if($query == null)
        {
            $this->_queryString = $_SERVER['QUERY_STRING'];
        }
        else
        {
            $this->_queryString = $query;
        }

        return $this;
    }

    /**
     * @return mixed
     */
    public function getQueryString()
    {
        return $this->_queryString;
    }

    /**
     * @return string
     */
    public function getRequestUri()
    {
        return $this->_requestUri;
    }

    /**
     * @param null $url
     */
    public function setBaseUrl($url = null)
    {
        if($url === null)
        {

            $url = str_replace('index.php', '', $_SERVER['SCRIPT_NAME']);

            if(empty($url))
                $url = '/'; //todo надо переделать немного по другому
        }

        $this->_baseUrl = $url;
    }

    /**
     * @param $url
     * @param int $statusCode
     */
    public function redirect($url, $statusCode = 302)
    {
        header('Location: '.$url, $statusCode);

        Framework::app()->end();
    }
}
