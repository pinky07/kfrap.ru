<?php
/**
 * Created by JetBrains PhpStorm.
 * User: pinky
 * Date: 05.02.12
 * Time: 19:15
 * To change this template use File | Settings | File Templates.
 */
namespace Framework;
use Framework\Config as Config;

final class Config
{
    private $_common            = null;

    private $_db                = null;

    private $_session           = null;

    private $_cookie            = null;

    private $_mail              = null;

    private $_application       = null;

    private static $_read_only  = false;

    public function __construct()
    {
        $this->_common      = Config\Config::create('Framework\Config\Common');
        $this->_db          = Config\Config::create('Framework\Config\DataBase');
        $this->_session     = Config\Config::create('Framework\Config\Session');
        $this->_cookie      = Config\Config::create('Framework\Config\Cookie');
        $this->_mail        = Config\Config::create('Framework\Config\Mail');
        $this->_application = Config\Config::create('Application\Config\Config');
    }

    /**
     * @param $config_ns
     * @return \Framework\Config bool
     */
    public function get($config_ns)
    {
        if(isset($this->{'_'.$config_ns}))
        {
            return $this->{'_'.$config_ns};
        }

        return false;
    }
}