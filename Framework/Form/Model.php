<?php
/**
 * Created by JetBrains PhpStorm.
 * User: pinky
 * Date: 18.02.12
 * Time: 12:41
 * To change this template use File | Settings | File Templates.
 */
namespace Framework\Form;
use ActiveRecord\Validations as Validations;
use \Framework\Inflector as Inflector;

class Model extends \Framework\Model
{
    /**
     * @var \ActiveRecord\Errors
     */
    protected $error           = null;

    protected $formName         = null;

    /**
     * Инициализирует форм модель и устанавливает данные из пост запроса
     */
    public function __construct()
    {
        $this->formName = Inflector::underscore(end(explode('\\', get_class($this))));

        $this->setAttributesFromPost();
    }

    /**
     * Устанавливает данные из Post запроса в форм модель
     */
    public function setAttributesFromPost()
    {
        if(isset($_POST[$this->formName]) and is_array($_POST[$this->formName]))
        {
            foreach($_POST[$this->formName] as $element => $value)
            {
                if(property_exists($this, $element))
                {
                    $this->$element = ($value === 'on')?1:$value;
                }
            }
        }
    }

    /**
     * Метод валидации форм модели.
     *
     * @return boolean
     */
    protected function _validate()
    {
        require_once _FWK_ROOT_DIR_.'/ActiveRecord/Validations.php';

        $validator = new Validations($this);
        // need to store reference b4 validating so that custom validators have access to add errors
        $this->error = $validator->get_record();

        $validator->validate();

        if (!$this -> error -> is_empty())
            return false;

        return true;
    }

    /**
     * Возвращает массив ошибок
     * @return mixed
     */
    public function getErrors()
    {
        return $this -> error -> get_raw_errors();
    }

    /**
     * Проверяет наличие ошибок валидации
     * @return bool
     */
    public function hasErrors()
    {
        return !$this -> error -> is_empty();
    }

    /**
     * @return array
     */
    public function getAttributes()
    {
        $class = new \ReflectionClass(get_class($this));

        $attributes = array();

        foreach($class->getProperties() as $property)
        {
            if($property->isPublic() && !$property->isStatic())
            {
                $attributes[$property->getName()] = $this->{$property->getName()};
            }
        }

        return $attributes;
    }

    /**
     * @param $name
     * @return mixed
     * @throws \Exception
     */
    public function __get($name)
    {
        if(property_exists($this, $name))
        {
            return $this->$name;
        }
        {
            throw new \Exception('property '.$name.' is not accessible.');
        }
    }

    /**
     * @param $name
     * @param $value
     * @return mixed
     * @throws \Exception
     */
    public function __set($name, $value)
    {
        if(property_exists($this, $name))
        {
            return $this->$name = $value;
        }
        {
            throw new \Exception('property '.$name.' is not accessible.');
        }
    }
}
