<?php
/**
 * Created by JetBrains PhpStorm.
 * User: pinky
 * Date: 05.02.12
 * Time: 16:09
 * To change this template use File | Settings | File Templates.
 */
namespace Framework;

/**
 *
 */
class Router
{
    /**
     * @var array
     */
    private $_routes        = array();
    /**
     * @var Request
     */
    private $_request       = null;


    /**
     * @param Request $request
     * @param array $params
     */
    private function _setRequestParams(Request $request, array $params)
    {
        if(!empty($params['controller_directory']))
            $request->setControllerDirectory($params['controller_directory']);
        if(!empty($params['controller']))
            $request->setControllerName($params['controller']);
        if(!empty($params['action']))
            $request->setActionName($params['action']);
        if(!empty($params['params']))
            $request->setParams($params['params']);
    }

    /**
     *
     */
    public function __construct()
    {
        $this->_loadRoutes();
    }

    //todo обязательно переделать!
    /**
     * @throws RouterException
     */
    private function _loadRoutes()
    {
        $routes_file = Framework::app()->getConfig('common')->routes_file;

        if(file_exists($routes_file))
            include_once $routes_file;
        else
            throw new RouterException('routes file not found');

        if(!isset($routes))
            throw new RouterException('routes array must be named "$routes"');
        else
            $this->_routes = $routes;
    }

    /**
     * @param Request $request
     */
    public function setRequest(Request $request)
    {
        $this->_request = $request;
    }

    /**
     * @param $requestUri
     */
    public function makeRoute(&$requestUri)
    {

        if(is_array($this->_routes))
        {
            foreach($this->_routes as $pattern => $route)
            {
                if(!preg_match("~$pattern~", $requestUri))
                    continue;

                $requestUri = preg_replace("~$pattern~", $route, $requestUri);
                break;
            }
        }
    }

    /**
     * @param $url
     * @return bool|mixed
     */
    public function findRoute($url)
    {
        if(array_search($url, $this->_routes) !== false)
        {
            return array_search($url, $this->_routes);
        }

        return false;
    }

    /**
     * @param Request $request
     */
    public function route(Request $request)
    {
        $this->setRequest($request);

        $requestUri = $request->getRequestUri();
        if($requestUri  != null) {
            $this->makeRoute($requestUri);
            $params =  $this->_parseUri($requestUri);
            $this->_setRequestParams($request, $params);
        } else {
            $this->setDefaultRoute($request);
        }
    }

    /**
     * @param $uri
     * @return array
     */
    private function _parseUri($uri)
    {
        $splits		= explode('/', trim($uri, '/'));

        $directory = 'Application/Controllers';
        while(!empty($splits[0]) and is_dir(_ROOT_DIR_.DIRECTORY_SEPARATOR.$directory.DIRECTORY_SEPARATOR.ucfirst($splits[0])))
        {
            $controller_directory[] = ucfirst(array_shift($splits));
            $directory .= DIRECTORY_SEPARATOR.end($controller_directory);
        }

        $params['controller_directory'] = (!empty($controller_directory))?implode(DIRECTORY_SEPARATOR, $controller_directory):null;
        $params['controller'] = (!empty($splits['0']))?$splits['0']:$this->_request->getDefaultController();
		$params['action'] = (!empty($splits['1']))?$splits['1']:$this->_request->getDefaultAction();

        if(!empty($controller_directory))
            $params['controller'] = strtolower(implode('\\', $controller_directory)).'\\'.$params['controller'];

		if(isset($splits['2']) and isset($splits['3']))
        {
			for($i=2, $cnt = count($splits); $i < $cnt; $i++) {
				if($i%2 == 0)
					$keys[] = $splits[$i];
				else
					$values[] = $splits[$i];
			}
			if(count($keys) > count($values))
					array_pop($keys);
			$params['params'] = array_combine($keys, $values);
		}

        return $params;
    }

    /**
     * @param Request $request
     */
    public function setDefaultRoute(Request $request)
    {
        $params = array(
                                        'controller'  => $request->getDefaultController(),
                                        'action'      => $request->getDefaultAction(),
                                        'params'      => array()
        );
        $this->_setRequestParams($request, $params);
    }

    /**
     * @return array
     */
    public function getRoutes()
    {
        return $this->_routes;
    }
}
