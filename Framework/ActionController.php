<?php
/**
 * Created by JetBrains PhpStorm.
 * User: pinky
 * Date: 05.02.12
 * Time: 15:23
 * To change this template use File | Settings | File Templates.
 */
namespace Framework;
use Framework\Helpers\FormHelper as FormHelper; //todo

class ActionController extends Foundation
{
    /**
     * Request object
     *
     * @var Request
     */
    protected $_request;

    /**
     * Response object
     *
     * @var Response
     */
    protected $_response;

    /**
     * @var array
     */
    private $_params            = array();

    /**
     * @var ActionView
     */
    protected $_actionView;

    /**
     * @var string
     */
    protected $_view;

    /**
     * Action layout file name
     *
     * @var string
     */
    protected $_layout;

    /**
     * @var bool
     */
    protected $_rendered        = false;

    /**
     * @var bool
     */
    protected $_autoRender      = true;

    /**
     * Base views directory
     *
     * @var string
     */
    protected $_viewsDir;

    /**
     * Base layouts directory
     *
     * @var string
     */
    protected $_layoutsDir;

    /**
     * @var null
     */
    protected $_sessionData     = null;

    /**
     * @var array
     */
    public  $notices = array();


    public function __construct(Request $request, Response $response, array $params = array()) {

        $this   ->setRequest($request)
                ->setResponse($response)
                ->setParams($params)
                ->setActionView();

        $this -> _initSession();

        $this->__onLoad();
    }

    /**
     * @param Request $request
     * @return ActionController
     */
    public function setRequest(Request $request)
    {
        $this->_request = $request;
        return $this;
    }

    /**
     * @param Response $response
     * @return ActionController
     */
    public function setResponse(Response $response)
    {
        $this->_response = $response;
        return $this;
    }

    /**
     * @param array $params
     * @return ActionController
     */
    public function setParams(array $params = array())
    {
        $this->_params = $params;
        return $this;
    }

    /**
     * @param ActionView|null $view
     * @return ActionController
     */
    public function setActionView(ActionView $view = null)
    {
        if($view === null)
            $this->_actionView = new ActionView($this->_view, $this->_layout);
        else
            $this->_actionView = $view;

        return $this;
    }

    /**
     * @return Request
     */
    public function getRequest()
    {
        return $this->_request;
    }

    /**
     * @return Response
     */
    public function getResponse()
    {
        return $this->_response;
    }

    /**
     * @return array
     */
    public function getParams()
    {
        return $this->_params;
    }

    /**
     * @return ActionView
     */
    public function getActionView()
    {
        return $this->_actionView;
    }

    /**
     * @param $dir
     */
    public function setViewsDir($dir)
    {
        $this->_viewsDir = $dir;
    }

    /**
     * @param $controller_name
     * @return ActionController
     */
    public function setControllerName($controller_name)
    {
        $this->_request->getControllerName($controller_name);

        return $this;
    }

    /**
     * @param $action_name
     * @return ActionController
     */
    public function setActionName($action_name)
    {
        $this->_request->setActionName($action_name);

        return $this;
    }

    /**
     * @param $key
     * @param null $value
     * @return ActionController
     */
    public function set($key, $value = null)
    {
        $this->_actionView->set($key, $value);

        return $this;
    }

    /**
     * @param $action
     */
    final public function process($action)
    {
        $this->before();

        ob_start();

        $this->set($this->{$action}());

        $this->_response->appendBody(ob_get_clean());

        if($this->_autoRender)
        {
            $this->beforeRender();

            $this->render();

            $this->afterRender();
        }

        $this->after();

    }

    /**
     *
     */
    final public function render()
    {
        $this->exportControllerVarsToView();

        if($this->_actionView->getView() === null)
            $this->_actionView->setView($this->getViewFile());

        if($this->_actionView->getLayout() === null)
            $this->_actionView->setLayout($this->getLayoutFile());

        if($this->_request->isXmlHttpRequest())
        {
            $this->_actionView->withoutLayout = true;
        }

        $content = $this->_actionView->render();

        $this->_response->appendBody($content);

        $this->_rendered = true;
    }

    /**
     * @param $action
     * @param null $controller
     * @param array|null $params
     */
    final protected function forward($action, $controller = null, array $params = null)
    {
        $request = $this->_request;

        if (null !== $controller) {
            $request->setControllerName($controller);
        }
        if (null !== $params) {
            $request->setParams($params);
        }

        $request->setActionName($action)
                ->setDispatched(false);
    }

    /**
     * @return ActionController
     */
    private function _initSession()
    {
        $this->setSession('post', Security::clean($_POST));
        $this->setSession('get', Security::clean($_GET));

        return $this;
    }

    /**
     * @param null $view
     * @return ActionController
     */
    public function setView($view = null)
    {
        if($view === null)
            $this->_view = strtolower(str_replace('Action','',$this->_request->getActionName()));
        else
            $this->_view = $view;

        return $this;
    }

    /**
     * @param $layout
     */
    public function setLayout($layout)
    {
        $this->_layout = $layout;
    }

    /**
     * @return string
     */
    public function getViewFile()
    {
        if($this->_view === null)
            $this->setView();

        if($this->_viewsDir !== null)
            $path['views_base_dir']   = $this->_viewsDir;

        $path['views_controller_dir'] = Inflector::namespace_to_path($this->_request->getControllerName());

        $path['view_file']            = $this->_view.'.inc';

        $viewFile = implode(DIRECTORY_SEPARATOR, $path);

        return $viewFile;
    }

    /**
     * @return string
     */
    public function getLayoutFile()
    {
        if(empty($this->_layout))
        {
            return 'empty.inc';
        }

        return $this->_layout.'.inc';
    }

    /**
     * @param ActionView|null $view
     */
    public function exportControllerVarsToView(ActionView $view = null)
    {
        if($view === null)
            $view = $this->_actionView;

        $reflect    = new \ReflectionClass($this);
        $properties = $reflect->getProperties(\ReflectionProperty::IS_PUBLIC | \ReflectionProperty::IS_PROTECTED);

        foreach($properties as $property)
        {
            if(property_exists($view, $property->getName()))
            {
                continue;
            }

            if($property->isPublic())
            {
                $view->{$property->getName()} = $this->{$property->getName()};
            }
                else
            {
                $getter = 'get'.ucfirst(str_replace('_','',$property->getName()));

                if(is_callable(array($this, $getter)))
                    $view->{str_replace('_','',$property->getName())} = $this->$getter();
            }

        }
    }

    /**
     * @param $key
     * @return bool
     */
    public function params($key)
    {
        if(!isset($this->_params[$key]))
            return false;

        return $this->_params[$key];
    }

    /**
     * @param $key
     * @param bool $cleaned
     * @return bool
     */
    public function post($key, $cleaned = false)
    {
        if(!isset($_POST[$key]))
        {
            return false;
        }

        if($cleaned)
            Security::clean($_POST[$key]);

        return $_POST[$key];
    }

    /**
     * @return bool
     */
    public function isPost()
    {
        return ($_SERVER['REQUEST_METHOD'] == 'POST');
    }

    /**
     * @param $key
     * @param bool $cleaned
     * @return bool
     */
    public function get($key, $cleaned = true)
    {
        if(!isset($_GET[$key]))
        {
            return false;
        }

        if($cleaned)
            $_GET[$key] = Security::clean($_GET[$key]);

        return $_GET[$key];
    }

    /**
     * @param $key
     * @param null $value
     */
    public function setSession($key, $value = null)
    {
        Session::getInstance()->set($key, $value, $this->_request->getControllerName());
    }

    /**
     * @param $key
     */
    public function getSession($key)
    {
        Session::getInstance()->get($key, $this->getRequest()->getControllerName());
    }

    /**
     * @return bool
     */
    public function isRendered()
    {
        return $this->_rendered;
    }

    /**
     * @param $type
     * @param $message
     */
    public function addNotice($type, $message)
    {
        Notice::create($type, $message);
    }

    /**
     * @return bool
     */
    public function hasError()
    {
        return Notice::hasNotice('error');
    }

    /**
     * @param array $params
     */
    public function redirectTo(array $params = array())
    {
        if(!isset($params['controller']))
        {
            $params['controller'] = $this
                                        -> getRequest()
                                        -> getControllerName();
            if(!isset($params['action']))
            {
                $params['action'] = $this
                                        -> getRequest()
                                        -> getActionName();

                if(!isset($params['params']))
                {
                    $params['params'] = $this
                                            -> getRequest()
                                            -> getParams();
                    if(!isset($params['query']))
                    {
                        parse_str($this-> getRequest()-> getQueryString(), $params['query']);
                    }
                }
            }
            else
            {
                if(!isset($params['params']))
                {
                    $params['query']  = array();
                }
            }

        }
        elseif(!isset($params['action']))
        {
            $params['action'] = 'index';
        }

        $url = sprintf('%s/%s/%s%s%s',
            Framework::app()->getConfig('common')->site_url,
            \Framework\Inflector::namespace_to_path($params['controller']),
            $params['action'],
            (isset($params['params'])
                && !empty($params['params']))?'/'.str_replace(
                                                        '=',
                                                        '/',
                                                        http_build_query(
                                                            $params['params'], null, '/')):'',
            (isset($params['query'])
                && !empty($params['query']))?'?'.http_build_query(
                                                    $params['query']):''
        );

        $this->_request->redirect($url);
    }

    /**
     * @param $notice
     * @param array $params
     * @return bool
     */
    public function noticeTo($notice, array $params = array())
    {
        if(is_array($notice) && !isset($notice['message']) && !is_string($notice))
        {
            return false;
        }

        $this->addNotice(
            (isset($notice['type']))?$notice['type']:'info',
            (isset($notice['message']))?$notice['message']:$notice);

        $this->redirectTo($params);

        exit;
    }

    /**
     *
     */
    protected function __onLoad()
    {

    }

    /**
     * Called before the controller action.
     *
     * @return void
     */
    public function before()
    {
    }

    /**
     * Called after the controller action is run and rendered.
     *
     * @return void
     */
    public function after()
    {
    }

    /**
     * Called before render action.
     *
     * @return void
     */
    public function beforeRender()
    {
        $this -> notices = Notice::flush();

        if(!is_array($this -> notices))
        {
            $this -> notices = array();
        }
    }

    /**
     * Called after render action.
     *
     * @return void
     */
    public function afterRender()
    {
    }

    /*
     *
     */
    public function isAjax()
    {
        return $this->_request->isXmlHttpRequest();
    }
}