<?php
/**
 * Created by JetBrains PhpStorm.
 * User: pinky
 * Date: 06.03.12
 * Time: 21:26
 * To change this template use File | Settings | File Templates.
 */
namespace Framework\Helpers;

class HtmlHelper extends Helper
{
    public function tag($name, $options = array(), $open = false)
    {
        if (!$name)
        {
            return '';
        }

        return '<'.$name.$this->formatOptions($options).(($open) ? '>' : ' />');
    }

    protected function formatOptions($options = array())
    {
        $html = '';
        foreach ($options as $key => $value)
        {
            $html .= ' '.$key.'="'.escape_once($value).'"';
        }

        return $html;
    }

    function escape_once($html)
    {
        return $this->fix_double_escape(htmlspecialchars($html, ENT_COMPAT, \Framework\Framework::app()->getConfig('common')->charset));
    }

    function fix_double_escape($escaped)
    {
        return preg_replace('/&amp;([a-z]+|(#\d+)|(#x[\da-f]+));/i', '&$1;', $escaped);
    }
}
