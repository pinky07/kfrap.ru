<?php
/**
 * Created by JetBrains PhpStorm.
 * User: PC
 * Date: 11.02.12
 * Time: 12:35
 * To change this template use File | Settings | File Templates.
 */
namespace Framework\Helpers;
use Framework\Form\Model as FormModel;
class FormHelper extends Helper
{
    protected $formModel = null;
    protected $attribute;
    private static  $_instance = null;
    
    protected $attributes = array();

    public function __construct(FormModel $model = null) 
    {
        $this->formModel = $model;
    }

    private function __clone() {

    }

    protected function getValue() 
    {
        
    }

    public static function getInstance() {
        if(!self::$_instance instanceof FormHelper)
            self::$_instance = new self();

        return self::$_instance;
    }

    public function openForm($action = null, $method = 'POST', array $options = array()) {

        if($action === null or $action === false)
            $action = $_SERVER['REQUEST_URI'];

        $form = "<form action = '$action' method = '$method'";

        if(!empty($options)) {
            foreach ($options as $name => $value)
                $form .= " $name = '$value'";
        }

        $form .= '>';

        echo $form;
    }

    public function closeForm() {
        $form = '</form>';

        echo $form;
    }

    protected function formatAttributes(array $attributes) 
    {
        $attrStr = '';
        
        foreach ($attributes as $attr => $value) 
        {
            if($attr == 'value')
                $attrStr .= " $attr = '$this->value'";

            $attrStr .= " $attr = '$value'";
        
        }
    }

    public function field($type, array $attributes = array())
    {
        $field = "<input type ='$type'";

        if(!empty($attributes)) {
            foreach ($attributes as $attr => $value)
                $field .= " $attr = '$value'";
        }

        $field .= '>';

        echo $field;
    }

    public function radio(array $attributes = array())
    {
        $radio = $this->field('radio', $attributes);

        echo $radio;
    }

    public function text(array $attributes = array()) {

        $text = $this->field('text', $attributes);

        echo $text;
    }

    public function hidden(array $attributes = array()) {

        $hidden = $this->field('hidden', $attributes);

        echo $hidden;
    }

    public function checkbox(array $attributes = array(), $checked = false) {

        if($checked)
            $attributes['checked'] = 'checked';
        
        $checkbox = $this->field('checkbox', $attributes);

        echo $checkbox;
    }

    public function submit(array $attributes = array()) {

        $submit = $this->field('submit', $attributes);

        echo $submit;
    }

    public function reset(array $attributes = array()) {

        $reset = $this->field('reset', $attributes);

        echo $reset;
    }

    public function textarea(array $attributes = array(), $value = '') {

        $textarea = "<textarea";

        if(!empty($attributes)) {
            foreach($attributes as $name => $val)
                $textarea .= " $name = '$val'";
        }

        $textarea .= ">$value</textarea>";

        echo $textarea;
    }

    public function password(array $attributes = array()) {

        $password = $this->field('password', $attributes);

        echo $password;
    }

    public function file(array $attributes = array()) {

        $file = $this->field('file', $attributes);

        echo $file;
    }

    public function label(array $attributes = array(), $text = '') {
        $label = '<label';
        if(!empty($attributes)) {
            foreach($attributes as $name => $val)
                $label .= " $name = '$val'";
        }
        
        $label .= ">$text</label>";

        echo $label;
    }

    public function select(array $attributes = array(), array $options = array(), $selected = null) {

        $select = "<select";

        if(!empty($attributes)) {
            foreach($attributes as $name => $val)
                $select .= " $name = '$val'";
        }

        $value = '';

        if(!empty($options)) {
            foreach($options as $val => $name) {
                $sel = ($val == $selected)?' selected':'';

                $value .= "<option value = '$val' $sel>$name</option>";
            }
                
        }

        $select .= ">$value</select>";

        echo $select;
    }

    public function CKEditor(array $attributes = array(), $value = '') {
        if(isset($attributes['name']))
            $name = $attributes['name'];
        else
            $name = '';

        $this->textarea($attributes, $value);

        echo <<<HTML
<script type="text/javascript">
        CKEDITOR.replace( '{$name}', {height:310,
        //filebrowserBrowseUrl : '/public/js/ckfinder/ckfinder.html',
        //filebrowserImageBrowseUrl : '/public/js/ckfinder/ckfinder.html?Type=Images',
        //filebrowserFlashBrowseUrl : '/public/js/ckfinder/ckfinder.html?Type=Flash',
        filebrowserUploadUrl : '/js/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
        filebrowserImageUploadUrl : '/js/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',
        filebrowserFlashUploadUrl : '/js/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash'
        } );
</script>
HTML;
    }
}
