<?php
/**
 * Created by JetBrains PhpStorm.
 * User: pinky
 * Date: 16.03.12
 * Time: 22:38
 * To change this template use File | Settings | File Templates.
 */
namespace Framework;

class ActionView
{
    private $_view;
    private $_layout;
    private $_data          = array();

    public $withoutLayout   = false;

    const VIEW_EXT      = '.inc';

    public function __construct($view = null, $layout = null, $data = array())
    {
        if($view !== null)
            $this->setView($view);

        if($layout !== null)
            $this->setLayout($layout);
    }

    public function setView($view)
    {
        if(strpos( $view, self::VIEW_EXT ) === false)
            $view = $view.self::VIEW_EXT;

        if(strpos($view, Framework::app()->getConfig('common')->views_dir) === false)
        {
            $view_full_path =   Framework::app()->getConfig('common')->views_dir
                                .DIRECTORY_SEPARATOR
                                .$view;
        }
        else
            $view_full_path = $view;

        if(file_exists($view_full_path))
        {
            $this->_view = $view_full_path;
        }
            else
        {
            throw new FrameworkException('not found view file '.$view_full_path);
        }


        return $this;
    }

    public function setLayout($layout)
    {
        if(strpos( $layout, self::VIEW_EXT ) === false)
            $layout = $layout.self::VIEW_EXT;

        if(strpos($layout, Framework::app()->getConfig('common')->layout_dir) === false)
        {
            $layout_full_path = Framework::app()->getConfig('common')->layout_dir
                                    .DIRECTORY_SEPARATOR
                                    .$layout;
        }
        else
            $layout_full_path = $layout;

        if(file_exists($layout_full_path))
        {
            $this->_layout = $layout_full_path;
        }
            else
        {
            throw new FrameworkException('not found layout file '.$layout_full_path);
        }

        return $this;
    }

    public function getView()
    {
        return $this->_view;
    }

    public function getLayout()
    {
        return $this->_layout;
    }

    public function set($key, $value = null)
    {
        if (is_array($key))
        {
            foreach ($key as $name => $value)
            {
                $this->_data[$name] = $value;
            }
        }
        else
        {
            $this->_data[$key] = $value;
        }

        return $this;
    }

    public function render()
    {
        $view_content = $this->_capture($this->_view);

        if($this->withoutLayout)
        {
            return $view_content;
        }

        $this->set('content', $view_content);

        return $this->_capture($this->_layout);
    }

    private function _capture($file)
    {
        extract($this->_data, EXTR_SKIP);

        ob_start();

        try
        {
            include $file;
        }
        catch (\Exception $e)
        {
            ob_end_clean();

            throw $e;
        }

        return ob_get_clean();
    }
}
