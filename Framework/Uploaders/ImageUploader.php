<?php
/**
 * Created by JetBrains PhpStorm.
 * User: PC
 * Date: 04.03.12
 * Time: 17:02
 * To change this template use File | Settings | File Templates.
 */
namespace Framework\Uploaders;

class ImageUploader extends Uploader
{
    public $height              = null;
    public $width               = null;
    public $rgb                 = 0xFFFFFF;
    public $quality             = 100;
    public $to                  = '../public/upload/images';
    protected $allowedTypes     = array('jpeg', 'jpg', 'png', 'gif');
    protected $prefix           = 'picture_';

    protected function process($tmp_name, $upload_dir)
    {
        $size = getimagesize($tmp_name);

        if ($size === false) {
            return false;
        }

        if($this->width == null)
            $this->width = $size[0];

        if($this->height == null)
            $this->height = $size[1];

        $format = strtolower(substr($size['mime'], strpos($size['mime'], '/') + 1));
        $icfunc = 'imagecreatefrom'.$format;

        if (!function_exists($icfunc)) {
            return false;
        }

        $x_ratio = $this->width  / $size[0];
        $y_ratio = $this->height / $size[1];

        if ($this->width == 0)
        {
            $y_ratio = $x_ratio;
            $this->height  = $y_ratio * $size[1];

        } elseif ($this->height == 0)
        {
            $x_ratio = $y_ratio;
            $this->width   = $x_ratio * $size[0];

        }

        $ratio       = min($x_ratio, $y_ratio);
        $use_x_ratio = ($x_ratio == $ratio);

        $new_width   = $use_x_ratio  ? $this->width  : floor($size[0] * $ratio);
        $new_height  = !$use_x_ratio ? $this->height : floor($size[1] * $ratio);
        $new_left    = $use_x_ratio  ? 0 : floor(($this->width - $new_width)   / 2);
        $new_top     = !$use_x_ratio ? 0 : floor(($this->height - $new_height) / 2);

        $isrc  = $icfunc($tmp_name);
        $idest = imagecreatetruecolor($this->width, $this->height);

        imagefill($idest, 0, 0, $this->rgb);
        imagecopyresampled($idest, $isrc, $new_left, $new_top, 0, 0, $new_width, $new_height, $size[0], $size[1]);

        imagejpeg($idest, $upload_dir, $this->quality);

        imagedestroy($isrc);

        imagedestroy($idest);

        return true;
    }
}
