<?php
/**
 * Created by JetBrains PhpStorm.
 * User: PC
 * Date: 04.03.12
 * Time: 16:57
 * To change this template use File | Settings | File Templates.
 */
namespace Framework\Uploaders;

class FileUploader extends Uploader
{
    protected $to                = '../public/upload/files';
    protected $allowedTypes         = array('doc', 'msword', 'pdf', 'PDF', 'rtf', 'djvu', 'vnd.ms-office', 'zip', 'jpg', 'JPG', 'png');
    protected $maxSize              = 9000000000000000;
}