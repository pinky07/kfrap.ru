<?php
/**
 * Created by JetBrains PhpStorm.
 * User: PC
 * Date: 04.03.12
 * Time: 17:01
 * To change this template use File | Settings | File Templates.
 */
namespace Framework\Uploaders;

abstract class Uploader extends \Framework\Foundation
{
    protected $files                = array();
    protected $type                 = null;
    protected $prefix               = 'file_';
    protected $useDefaultName       = false;
    protected $to                   = '../public/upload';
    protected $allowedTypes         = array();
    protected $maxSize              = 90000000000;
    protected $errors               = array();

    protected $errorsMessage = array(
                                    1 => "Размер загружаемого файла превышает допустимый размер.",
                                    2 => "Размер загружаемого файла превышает допустимый размер.",
                                    3 => "Файл был загружен лишь частично.",
                                    4 => "Файл не был загружен.",
                                    6 => "Файл не был загружен.",
                                    7 => "Файл не был загружен.",
                                    8 => "Файл не был загружен.");

    final protected function __construct()
    {

    }

    final protected function validate($extension, $size)
    {
        if(in_array($extension, $this->allowedTypes) and $size <= $this->maxSize)
        {
            return true;
        }
        else {
            if(!in_array($extension, $this->allowedTypes))
                $this->errors[] = "Файлы с расширением .{$extension} не разрешенны к загрузке.";
            if($size > $this->maxSize)
                $this->errors[] = "Превышен допустимый размер файла.";
        }

       return false;
    }


    final private function upload(array $file)
    {
        $splits = explode('/', $file['type']);

        $extension = $splits['1'];

        if (!$this->validate($extension, $file['size']))
            return false;

        if($this->useDefaultName)
        {
            $name = $this->prefix.$file['name'];
        } else {

            $name = uniqid($this->prefix).'.'.$extension;
        }

        if ($this->process($file['tmp_name'], $this->to . DIRECTORY_SEPARATOR . $name))
        {
            return $name;

        } else
        {
            return false;
        }
    }

    protected function process($tmp_name, $upload_dir)
    {
        return move_uploaded_file($tmp_name, $upload_dir);
    }

    /**
     * Основной метод для загрузки файла или файлов.
     * В качестве аргумента принимает суперглобальный массив $_FILES['file_name'],
     * либо название файла из ассоциативного массива $_FILES
     * @param $files
     * @return bool
     */
    public function uploads($files)
    {
        if(!is_dir($this->to))
        {
            $this->errors[] = 'dir not found';
            return false;
        }

        if(!is_array($files))
        {
            if(!$this->exists($files))
                return false;

            $files = $_FILES[$files];
        }

        $this->files = $files;

        if (!is_array($this->files['name']))
        {
            return $this->uploadsOneFile();
        } else
        {
            return $this->uploadsManyFiles();
        }
    }

    /**
     * загрузка одного файла
     */
    public function uploadsOneFile()
    {
        if(!$this->_isValid($this->files))
            return false;

        if ($this->files['error'] != 0)
        {
            $this->errors[] = $this->errorsMessage[$this->files['error']];

            return false;
        }

        $result = $this->upload($this->files);

        if (!$result)
            return false;

        $this->files['name'] = $result;

        return true;
    }

    /**
     * загрузка нескольких файлов
     */
    function uploadsManyFiles()
    {
        $coutFiles = count($this->files['name']);

        for ($i = 0; $i < $coutFiles; $i++)
        {

            if($this->_isValid(array(
                                    'error'     => $this->files['error'][$i],
                                    'name'      => $this->files['name'][$i],
                                    'type'      => $this->files['type'][$i],
                                    'tmp_name'  => $this->files['tmp_name'][$i],
                                    'size'      => $this->files['size'][$i],
                                    )))
            {
                $this->errors[] = 'file '.$this->files['name'][$i].' is not valid';
                break;
            }

            if ($this->files['error'][$i] == 0)
            {
                $result = $this->upload(
                                        array(
                                                'name'      => $this->files['name'][$i],
                                                'type'      => $this->files['type'][$i],
                                                'tmp_name'  => $this->files['tmp_name'][$i],
                                                'size'      => $this->files['size'][$i]));

                if ($result != false)
                {
                    $this->files['name'][$i] = $result;
                } else
                {
                    $this->errors[] = $this->files['name'];
                }
            } else
            {
                $this->errors[] = $this->errorsMessage[$this->files['error']];
            }
        }

        return true;
    }

    /**
     * @static
     * @param $uploaderName
     * @return \Framework\Uploaders\Uploader
     * @throws UploaderException
     */
    public static function factory($uploaderName)
    {
        $uploaderClass = 'Framework\Uploaders\\'.ucfirst($uploaderName).'Uploader';

        if(class_exists($uploaderClass))
        {
            $uploader = new $uploaderClass;

            if(!is_subclass_of($uploader, 'Framework\Uploaders\Uploader'))
                throw new UploaderException('Uploader ' .$uploaderName. ' must be extends Uploader class');

        } else {
            throw new UploaderException('Uploader ' .$uploaderName. ' not found');
        }

        return $uploader;
    }

    public function getErrors()
    {
        return $this->errors;
    }

    protected function _isValid(array $file) {
        return (isset($file['error'])
        			AND isset($file['name'])
        			AND isset($file['type'])
        			AND isset($file['tmp_name'])
        			AND isset($file['size']));
    }

    public function exists($file)
    {
        return isset($_FILES[$file]);
    }

    public function isEmpty($file)
    {
        return empty($_FILES[$file]['name']);
    }

    public function getTo()
    {
        return $this->to;
    }

    public function __get($name)
    {
        if(isset($this->files[$name]))
            return $this->files[$name];
        else
            return parent::__get($name);
    }
}
