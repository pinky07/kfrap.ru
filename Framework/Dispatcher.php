<?php
/**
 * Created by JetBrains PhpStorm.
 * User: pinky
 * Date: 06.02.12
 * Time: 22:19
 * To change this template use File | Settings | File Templates.
 */
namespace Framework;

/**
 *
 */
class Dispatcher
{
    /**
     * @var ActionController
     */
    private $_controller;

    /**
     * @var string
     */
    private $_defaultAction = 'index';
    /**
     * @var string
     */
    protected $_defaultController = 'index';
    /**
     * @var FrontController
     */
    protected $_frontController;
    /**
     * @var array
     */
    protected $_invokeParams = array();
    /**
     * @var Response
     */
    protected $_response = null;

    /**
     * @param Response $response
     * @return Dispatcher
     */
    public function setResponse(Response $response)
    {
        $this->_response = $response;
        return $this;
    }

    /**
     * @return Response
     */
    public function getResponse()
    {
        return $this->_response;
    }

    /**
     * @param Request $request
     * @param Response $response
     * @throws DispatcherException
     */
    public function dispatch(Request $request, Response $response)
    {
        $this->setResponse($response);

        $className  = $this->getControllerClass($request);

        $params = $request->getParams();

        $this->loadClass($className);

        $rc = new \ReflectionClass($className);

        if(!$rc->isSubclassOf('Framework\ActionController'))
        {
            throw new DispatcherException('Controller must be extend ActionController');
        }

        $this->_controller = new $className($request, $response, $params);

        $actionName = $this->getActionMethod($request);

        if(!$rc->hasMethod($actionName))
        {
            throw new DispatcherException('Action method <strong>' .$actionName. '</strong> is not found in <strong>' .$className. '</strong> controller');
        }

        try {

            $this->_controller->process($actionName);

        } catch(\Exception $e) {

            echo $e->getMessage();
        }
    }

    /**
     * @param $className
     * @return mixed
     * @throws DispatcherException
     */
    public function loadClass($className)
    {
        if(class_exists($className))
        {
            return $className;
        }
        else
        {
            throw new DispatcherException('class ' .$className. ' not found');
        }
    }

    /**
     * @param Request $request
     * @return string
     */
    public function getControllerClass(Request $request)
    {
        $controllerName = $request->getControllerName();

        if(empty($controllerName))
        {
            $controllerName = $this->getDefaultControllerName();
            $request->setControllerName($controllerName);
        }
        $namespace = $this->getControllerClassNamespace();

        $className = $namespace.'\\'.$this->formatControllerName($controllerName);

        return $className;
    }

    /**
     * @return string
     */
    public function getControllerClassNamespace()
    {
        return str_replace(DIRECTORY_SEPARATOR, '\\', Framework::app()->getConfig('common')->controllers_dir);
    }


    /**
     * @param Request $request
     * @return string
     */
    public function getActionMethod(Request $request)
    {
        $actionName = $request->getActionName();

        if(empty($actionName)) {
             $actionName = $this->getDefaultActionName();
            $request->setActionName($actionName);
        }

        $actionMethod = $this->formatActionName($actionName);
        return $actionMethod;
    }

    /**
     * @return string
     */
    public function getDefaultControllerName()
    {
        return $this->_defaultController;
    }

    /**
     * @return string
     */
    public function getDefaultActionName()
    {
        return $this->_defaultAction;
    }

    /**
     * @param $unformatted
     * @return string
     */
    public function formatControllerName($unformatted)
    {
        if($pos = strpos($unformatted, '_'))
        {
            $unformatted[$pos+1] = ucfirst($unformatted[$pos+1]);
        }
        
        if($pos = strpos($unformatted, '\\'))
        {
            $unformatted[$pos+1] = ucfirst($unformatted[$pos+1]);
        }

        $controllerName = ucfirst($unformatted).'Controller';

        return $controllerName;
    }

    /**
     * @param $unformatted
     * @return string
     */
    public function formatActionName($unformatted)
    {
        $unformatted = strtolower($unformatted);

        if($pos = strpos($unformatted, '_')) {
            $unformatted[$pos+1] = ucfirst($unformatted[$pos+1]);
            $unformatted  = str_replace('_', '', $unformatted);
        }

        $actionName = $unformatted.'Action';

        return $actionName;
    }

    /**
     * @param $class
     * @return string
     */
    public function classToFileName($class)
    {
        if(strpos($class, '_'))
        {
            $class = str_replace($class[0], strtolower($class[0]), $class);
        }
        return str_replace('_', DIRECTORY_SEPARATOR, $class) . '.php';
    }
}
