<?php
/**
 * Created by JetBrains PhpStorm.
 * User: pinky
 * Date: 18.03.12
 * Time: 22:18
 * To change this template use File | Settings | File Templates.
 */
namespace Framework;
class Security
{

    /**
     * @static
     * @param $uri
     */
    public static function clean_uri($uri)
    {
        //todo
    }

    /**
     * Cleans the global $_GET, $_POST and $_COOKIE arrays
     */
    public static function clean_input()
    {
        $_GET		= static::clean($_GET);
        $_POST		= static::clean($_POST);
        $_COOKIE	= static::clean($_COOKIE);
    }

    /**
     * Generic variable clean method
     *
     * @param $data
     * @return array|mixed|string
     */
    public static function clean($data)
    {
        if(empty($data))
            return $data;
        if(is_array($data) && count($data)) {
            foreach($data as $k => $v) {
                $data[$k] = self::clean($v);
            }
            return $data;
        }
            if(trim($data) === '') {
                 return $data;
            }
                // xss_clean function from Kohana framework 2.3.1

            $data = str_replace(array('&amp;','&lt;','&gt;'), array('&amp;amp;','&amp;lt;','&amp;gt;'), $data);

            $data = preg_replace('/(&#*w+)[x00-x20]+;/u', '$1;', $data);

            $data = preg_replace('/(&#x*[0-9A-F]+);*/iu', '$1;', $data);

            $data = html_entity_decode($data, ENT_COMPAT, 'UTF-8');

                // Remove any attribute starting with "on" or xmlns

            $data = preg_replace('#(<[^>]+?[x00-x20"\'])(?:on|xmlns)[^>]*+>#iu', '$1>', $data);

                // Remove javascript: and vbscript: protocols

            $data = preg_replace('#([a-z]*)[x00-x20]*=[x00-x20]*([`\'"]*)[x00-x20]*j[x00-x20]*a[x00-x20]*v[x00-x20]*a[x00-x20]*s[x00-x20]*c[x00-x20]*r[x00-x20]*i[x00-x20]*p[x00-x20]*t[x00-x20]*:#iu', '$1=$2nojavascript...', $data);

            $data = preg_replace('#([a-z]*)[x00-x20]*=([\'"]*)[x00-x20]*v[x00-x20]*b[x00-x20]*s[x00-x20]*c[x00-x20]*r[x00-x20]*i[x00-x20]*p[x00-x20]*t[x00-x20]*:#iu', '$1=$2novbscript...', $data);

            $data = preg_replace('#([a-z]*)[x00-x20]*=([\'"]*)[x00-x20]*-moz-​binding[x00-x20]*:#u', '$1=$2nomozbinding...', $data);

            // Only works in IE: <span style="width: exp​ression(alert('Ping!'));"></span>

            $data = preg_replace('#(<[^>]+?)style[\x00-\x20]*=[\x00-\x20]*[`\'"]*.*?expression[\x00-\x20]*\([^>]*+>#is', '$1>', $data);

            $data = preg_replace('#(<[^>]+?)style[\x00-\x20]*=[\x00-\x20]*[`\'"]*.*?behaviour[\x00-\x20]*\([^>]*+>#is', '$1>', $data);

            $data = preg_replace('#(<[^>]+?)style[x00-x20]*=[x00-x20]*[`\'"]*.*?s[x00-x20]*c[x00-x20]*r[x00-x20]*i[x00-x20]*p[x00-x20]*t[x00-x20]*:*[^>]*+>#iu', '$1>', $data);

            // Remove namespaced elements (we do not need them)

            $data = preg_replace('#</*w+:w[^>]*+>#i', '', $data);

            do {
                // Remove really unwanted tags
                $old_data = $data;
                $data = preg_replace('#</*(?:applet|b(?:ase|gsound|link)|embed|frame(?:set)?|i(?:frame|layer)|l(?:ayer|ink)|meta|object|s(?:cript|tyle)|title|xml)[^>]*+>#i', '', $data);
            }
            while ($old_data !== $data);

            return $data;
    }

    public static function strip_tags($value)
    {
        if ( ! is_array($value))
        {
            $value = filter_var($value, FILTER_SANITIZE_STRING);
        }
        else
        {
            foreach ($value as $k => $v)
            {
                $value[$k] = static::strip_tags($v);
            }
        }

        return $value;
    }

}
