<?php
/**
 * Created by JetBrains PhpStorm.
 * User: PC
 * Date: 11.02.12
 * Time: 18:48
 * To change this template use File | Settings | File Templates.
 */
namespace Framework;

class Session
{
    private $_name;
    private $_lifetime;
    private $_data;
    private static $_instance;

    private function __construct() {

        $session_name     = Framework::app()->getConfig('session')->name;
        $session_lifetime = Framework::app()->getConfig('session')->lifetime;
        $cookie_path      = Framework::app()->getConfig('cookie')->cookie_path;
        $cookie_domain    = Framework::app()->getConfig('cookie')->cookie_domain;
        $cookie_secure    = Framework::app()->getConfig('cookie')->cookie_secure;
        $cookie_httponly  = Framework::app()->getConfig('cookie')->cookie_httponly;

        //session_set_cookie_params($session_lifetime, $cookie_path, $cookie_domain, $cookie_secure, $cookie_httponly);

        //session_cache_limiter(FALSE);

        //session_name($session_name);

        session_start();

        $this->_data =& $_SESSION;

        $this->_name = session_name();
    }

    /**
     * @static
     * @return Session
     */
    public static function getInstance()
    {
        if(!isset(self::$_instance))
        {
            self::$_instance = new self();
        }

        return self::$_instance;
    }

    public function set($key, $value = null, $namespace = 'common')
    {
        if (is_array($key))
        {
            foreach ($key as $name => $value)
            {
                $this->_data[$namespace][$name] = $value;
            }
        }
        else
        {
            $this->_data[$namespace][$key] = $value;
        }

        return $this;
    }

    public function get($key, $namespace = 'common')
    {
        if(isset($this->_data[$namespace][$key]))
        {
            return $this->_data[$namespace][$key];
        }

        return false;
    }

    public function rm($key, $namespace = 'common')
    {
        if(!isset($this->_data[$namespace][$key]))
        {
            return false;
        }

        unset($this->_data[$namespace][$key]);

        return true;
    }

    public function flush($key, $namespace = 'common')
    {
        if(!isset($this->_data[$namespace][$key]))
        {
            return false;
        }

        $data = $this->get($key, $namespace);

        $this->rm($key, $namespace);

        return $data;
    }

    public function flushNamespace($namespace = 'common')
    {
        if(!isset($this->_data[$namespace]))
        {
            return false;
        }

        $data = $this->_data[$namespace];

        unset($this->_data[$namespace]);

        return $data;
    }

    public function append($key, $value = null, $namespace = 'common')
    {
        if(!isset($this->_data[$namespace][$key]))
        {
            $this->_data[$namespace][$key] = array($value);
        }
        else
        {
            array_push($this->_data[$namespace][$key], $value);
        }

        return $this;
    }

    public function getSessionName()
    {
        return $this->_name;
    }

    public function regenerate()
    {
        session_regenerate_id();
    }

    public function exist($key, $namespace = 'common')
    {
        return isset($this->_data[$namespace][$key]);
    }
}
