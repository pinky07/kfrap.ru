<?php
/**
 * Created by JetBrains PhpStorm.
 * User: pinky
 * Date: 14.02.12
 * Time: 20:47
 * To change this template use File | Settings | File Templates.
 */
namespace Framework;

class Notice implements \Framework\INotice
{
    private $_message       = null;

    private $_noticeType    = null;
    /*private static  $_notices = array(
                                    'warning'    => array(),
                                    'error'         => array(),
                                    'success'   => array(),
                                    'info'             => array());*/

    const SESSION_NS = 'notices';

    public function __construct($message = null)
    {
        if($message !== null)
            $this->setMessage($message);

        return $this;
    }

    public function setMessage($message)
    {
        $this->_message = $message;

        return $this;
    }

    public function getMessage()
    {
        return $this->_message;
    }

    public function getType()
    {
        if($this->_noticeType == null)
        {
            $this->_noticeType = strtolower(Inflector::denamespace(get_class($this)));
        }

        return $this->_noticeType;
    }

    public static function create($notice, $message, $save_in_session = true)
    {
        $noticeClass = sprintf('Framework\Notice\%s', ucfirst($notice));

        if(!class_exists($noticeClass))
        {
            throw new FrameworkException('Класс уведомления не найден');
        }

        if(is_array($message))
        {
            $notices = array();

            foreach($message as $mess)
            {
                if(is_array($mess))
                {
                    self::create($notice, $mess);

                    continue;
                }

                $notice = new $noticeClass();
                $notice -> setMessage($mess);

                if($save_in_session)
                    $notice -> pushSession();

                $notices[]  = $notice;
            }

            return $notices;
        }

        $notice = new $noticeClass();
        $notice -> setMessage($message);

        if($save_in_session)
            $notice -> pushSession();

        return $notice;
    }

    public function pushSession()
    {
        Session::getInstance()->append(
            $this->getType(),
            $this->_message,
            self::SESSION_NS
        );
    }

    public static function flush($noticeType = 'all')
    {
        if($noticeType === 'all')
        {
            $noticesArray = Session::getInstance()->flushNamespace(self::SESSION_NS);

            $notices = array();

            if(is_array($noticesArray))
            {
                foreach($noticesArray as $type => $notice)
                {
                    if(!empty($notice) && is_array($notice))
                    {
                        foreach($notice as $nt)
                        {
                            $notices[] = self::create($type, $nt, false);
                        }
                    }
                }
            }

            return $notices;
        }

        if(Session::getInstance()->exist($noticeType, self::SESSION_NS))
        {
            $notices = Session::getInstance()->flush($noticeType, self::SESSION_NS);

            return self::create($noticeType, $notices, false);
        }

        return false;
    }

    public static function hasNotice($type)
    {
        return Session::getInstance()->exist($type, self::SESSION_NS);
    }

    public function show()
    {
return  <<<HTML
<div class="alert alert-{$this->getType()}">
<ul style='margin: 0 0 0 25px'>
        <li>{$this->getMessage()}</li>
    </ul>
</div>
HTML;
    }
}
