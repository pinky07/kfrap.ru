<?php
/**
 * Created by JetBrains PhpStorm.
 * User: PC
 * Date: 10.02.12
 * Time: 21:44
 * To change this template use File | Settings | File Templates.
 */
namespace Framework;

class Autoloader
{
    private $_extension         = '.php';
    private $_includePath       = null;
    private $_namespaces        = array();
    private $_prefixes          = array();
    private $_useIncludePath    = false;
    private static $_instance   = null;

    private function __construct()
    {

    }

    public static function getInstance()
    {
        if(!self::$_instance instanceof Loader)
            self::$_instance = new self();

        return self::$_instance;
    }

    public function setNamespaces(array $namespaces)
    {
        foreach($namespaces as $namespace => $locations) {
            $this->_namespaces[$namespace] = (array) $locations;
        }

        return $this;
    }

    public function setPrefixes(array $classes)
    {
        foreach ($classes as $prefix => $locations) {
            $this->_prefixes[$prefix] = (array) $locations;
        }
    }

    public function setExtension($extension) {
        $this->_extension = $extension;
    }

    public function register()
    {
        spl_autoload_register(array($this, 'loadClass'), true);
    }

    public function loadClass($class)
    {
        if($file = $this->findFile($class)) {
            require $file;
        }
    }

    public function findFile($class)
    {
        if ('\\' == $class[0]) {
            $class = substr($class, 1);
        }

        if (false !== $pos = strrpos($class, '\\')) {
            $namespace = substr($class, 0, $pos);
            $className = substr($class, $pos + 1);
            $normalizedClass = str_replace('\\', DIRECTORY_SEPARATOR, $namespace).DIRECTORY_SEPARATOR.str_replace('_', DIRECTORY_SEPARATOR, $className).$this->_extension;
            foreach ($this->_namespaces as $ns => $dirs) {
                if (0 !== strpos($namespace, $ns)) {
                    continue;
                }
                foreach ($dirs as $dir) {
                    $file = $dir.DIRECTORY_SEPARATOR.$normalizedClass;
                   //echo '<br/>';
                    // print_r($file); echo '<br/>';;
                    if (is_file($file)) {
                        return $file;
                    }
                }
            }
        } else {
            $normalizedClass = str_replace('_', DIRECTORY_SEPARATOR, $class).'.php';
            foreach ($this->_prefixes as $prefix => $dirs) {
                if (0 !== strpos($class, $prefix)) {
                    continue;
                }

                foreach ($dirs as $dir) {
                    $file = $dir.DIRECTORY_SEPARATOR.$normalizedClass;
                    if (is_file($file)) {
                        return $file;
                    }
                }
            }
        }
        if ($this->_useIncludePath && $file = stream_resolve_include_path($normalizedClass)) {
            return $file;
        }
    }

    public function setIncludePath()
    {
        foreach(func_get_args() as $path) {
            if(!file_exists($path) or (file_exists($path) and !is_dir($path))) {

                throw new \Exception ("Include path '{$path}' not exist!");

            } else {
                $paths = explode(PATH_SEPARATOR, get_include_path());

                if (array_search($path, $paths) === false)
                    array_push($paths, $path);

                set_include_path(implode(PATH_SEPARATOR, $paths));
            }
        }
    }
}
