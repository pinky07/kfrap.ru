<?php
namespace Framework;

abstract class Model extends Foundation
{
    /**
     * Factory method for ActiveRecord models
     *
     * @static
     * @param $class
     * @return \ActiveRecord\Model
     * @throws FrameworkException
     */
	public static function ActiveRecord( $class )
    {
        $class  = 'Application\Models\\'.$class;

        if(!class_exists($class))
            throw new FrameworkException('Класс '.$class.' не найден');

        $rc = new \ReflectionClass($class);
        if(!$rc->isSubclassOf('\ActiveRecord\Model'))
            throw new FrameworkException('Неправильный класс модели '.$class);


        return new $class;
	}

    /**
     * Factory method for Form models
     *
     * @static
     * @param $class
     * @return \Framework\Form\Model
     * @throws FrameworkException
     */
    public static function Form($class)
    {
        $class  = 'Application\Models\Forms\\'.$class;

        if(!class_exists($class))
            throw new FrameworkException('Класс '.$class.' не найден');

        $rc = new \ReflectionClass($class);
        if(!$rc->isSubclassOf('\Framework\Form\Model'))
            throw new FrameworkException('Неправильный класс модели '.$class);

        return new $class;
    }

	public function __call($arg1, $arg2)
    {
		echo 'Вызван не существующий метод <b>'.$arg1.'</b>';
	}

    /**
     * Run validations on model and returns whether or not model passed validation.
     *
     * @see is_invalid
     * @return boolean
     */
    public function is_valid()
    {
        return $this->_validate();
    }

    /**
     * Runs validations and returns true if invalid.
     *
     * @see is_valid
     * @return boolean
     */
    public function is_invalid()
    {
        return !$this->_validate();
    }

    /**
     * Validates the model.
     *
     * @return boolean True if passed validators otherwise false
     */
    abstract protected function _validate();

    /**
     * Set models attributes from array
     * TODO доделать с учетом валидации атрибутов
     * @param $array array
     */
    public function setAttributes($array = array())
    {
        foreach($array as $attribute => $value)
        {
            if(isset($this->$attribute))
            {
                $this->$attribute = $value;
            }
        }
    }
}
?>